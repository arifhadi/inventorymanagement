<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Incoming_item extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_incoming_item');
        $this->load->helper('url');
        date_default_timezone_set("Asia/Jakarta");
    }

    //Index
    public function index()
    {
        check_permission_page(ID_GROUP,'read','incoming');
        $status = $this->m_incoming_item->get_total_stock_in();
        // log_r($status);
        $lates_update   = $this->m_incoming_item->get_lates_update();
        $tes    = $lates_update[0]->last_update;
        $this->data['date_last_update'] = date('d F Y h:i', strtotime($tes));

        $user_id        = $lates_update[0]->updateby;
        $get_users      = $this->m_incoming_item->get_user($user_id);
        $this->data['name_user']    = $get_users->first_name;
        
        $this->data['status'] = $status;
        $this->admintemp->view("Incomingitem/List_incoming", $this->data);
    }

    //Form
    public function Form($id_item)
    {
        check_permission_page(ID_GROUP,'create','incoming');
        $stock_in = $this->m_incoming_item->get_stock_in($id_item);

        $this->data['total_list'] = $this->m_incoming_item->count_stock($id_item);

        $stock_list = $this->m_incoming_item->get_stock_list($id_item);
        $this->data['stock_list'] = $stock_list;

        $this->data['stock_in'] = $stock_in;
        $this->data['id_item'] = $id_item;
        $this->data['opening'] = $stock_in->opening;
        $this->admintemp->view('Incomingitem/Form_add_stock', $this->data);
    }

    //Save Add Stock
    public function save_add_stock()
    {
        $pic        = $this->input->post('pic');
        $id_item    = $this->input->post('id_item');
        $stock_in   = $this->input->post('stock_in');
        $date_in    = date('Y-m-d');
        $opening    = $this->input->post('opening');

        $cek_stock_in = $this->m_incoming_item->cek_stock_in($id_item);
        $call_balance = $this->m_incoming_item->call_balance($id_item);

        if (empty($cek_stock_in->total_stock_in)) {
            $total = $stock_in + $opening;
            $this->data['total'] = $total;
        } else {
            $total = $stock_in + $call_balance->balance_stock;
            $this->data['total'] = $total;
        }

        $data = array(
            'pic'        => $pic,
            'id_item'    => $id_item,
            'stock_in'   => $stock_in,
            'date_in'    => $date_in,

        );

        $this->m_incoming_item->insert_add_stock($data);
        $count = $this->m_incoming_item->count_stock($id_item);

        $total_balance = array(
            'total_stock_in'    => $count->stock_in,
            'balance_stock'     => $total,
            'updateby'          => USER_ID,
            'last_update'       => date("Y-m-d h:i:s")
        );

        $total_balance = $this->m_incoming_item->total($id_item, $total_balance);
        $this->session->set_flashdata('info', "Data was successfully Updated!");
        redirect('Incoming_item/form/' . $id_item);
    }

    //Detail
    public function detail($id_item)
    {
        check_permission_page(ID_GROUP,'read','incoming');
        $data_details = $this->m_incoming_item->detail($id_item);
        $total_list = $this->m_incoming_item->count_stock($id_item);
        $this->data['data_details'] = $data_details;
        $this->data['total_list']   = $total_list;
        $this->admintemp->view('Incomingitem/detail_incoming', $this->data);
    }

    //Masuk Ke List Edit
    public function edit($id_item)
    {
        check_permission_page(ID_GROUP,'update','incoming');
        $stock_list = $this->m_incoming_item->get_stock_list($id_item);
        $total_list = $this->m_incoming_item->count_stock($id_item);
        $this->data['stock_list'] = $stock_list;
        $this->data['total_list'] = $total_list;
        $this->admintemp->view('Incomingitem/edit_incoming', $this->data);
    }

    // Masuk Ke Form Edit
    public function edit_form($id)
    {
        check_permission_page(ID_GROUP,'update','incoming');
        // $id = 2;
        $s_edit = $this->m_incoming_item->get_stock_edit($id);
        $p_edit = $this->m_incoming_item->get_pic($id);
        // log_r($s_edit);
        $this->data['s_edit'] = $s_edit;
        // $this->data['id_item'] = $id_item;
        $this->data['p_edit'] = $p_edit;
        $this->data['id'] = $id;
        $this->data['total_stock_in'] = $s_edit->total_stock_in;

        $this->admintemp->view('Incomingitem/form_edit', $this->data);
    }

    // Update Stock in
    public function update_edit()
    {
        check_permission_page(ID_GROUP,'update','incoming');
        $id         = $this->input->post('id');
        $pic        = $this->input->post('pic');
        $id_item    = $this->input->post('id_item');
        $stock_in   = $this->input->post('stock_in');
        $date_in    = date('Y-m-d');
        
        $call_opening = $this->m_incoming_item->call_opening($id_item);

        $data = array(
            'id'        => $id,
            'pic'        => $pic,
            'id_item'    => $id_item,
            'stock_in'   => $stock_in,
            'date_in'    => $date_in,

        );
        // log_r($data);
        $this->m_incoming_item->update_edit($id, $data);
        $count = $this->m_incoming_item->count_stock($id_item);

        $total = $count->stock_in + $call_opening->opening - $call_opening->total_stock_out;

        $total_balance = array(
            'total_stock_in'   => $count->stock_in,
            'balance_stock'    => $total,
        );

        $total_balance = $this->m_incoming_item->total($id_item, $total_balance);
        $this->session->set_flashdata('info', "Data was successfully Updated!");
        redirect('Incoming_item/edit/' . $id_item);
    }

    //Update Data
    public function update_data()
    {
        $pic         = $this->input->post('pic');
        $id_item     = $this->input->post('id_item');
        $date_in     = date('Y-m-d');
        $stock_in    = $this->input->post('stock_in');

        $data = array(
            'pic'       => $pic,
            'id_item'   => $id_item,
            'date_in'   => $date_in,
            'stock_in'  => $stock_in,
        );

        $cek_stock_in = $this->m_incoming_item->cek_stock_in($id_item);

        $count = $this->m_incoming_item->count_sum($id_item);

        $total_sum = array(
            'total_stock_in'   => $count->stock_in,
        );
        $total_sum = $this->m_incoming_item->total_sum($id_item, $total_sum);
        $this->m_incoming_item->update_add_item($data, $id_item);
        $this->session->set_flashdata('info', "Data was successfully Updated!");
        // log_r($data);      
        redirect('Incoming_item/edit/' . $id_item);
    }

    //Hapus
    public function hapus($id_item)
    {
        check_permission_page(ID_GROUP,'delete','incoming');
        $where = array('id_item' => $id_item);
        $this->m_incoming_item->hapus_data($where, 'tb_item');
        $this->m_incoming_item->hapus_data($where, 'tb_stock');
        $this->m_incoming_item->hapus_data($where, 'tb_item_stock_in');
        // log_r($where);
        redirect('Incoming_item');
    }
}
