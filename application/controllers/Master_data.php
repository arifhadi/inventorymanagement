<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_data extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_master_data');
        $this->load->helper('url');
        $this->load->helper('download');
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index()
    {
        check_permission_page(ID_GROUP,'read','master_data');
        $master_data = $this->m_master_data->get_master_data();
        // log_r($master_data);
        $lates_update   = $this->m_master_data->get_lates_update();
        $tes    = $lates_update[0]->last_update;
        $this->data['date_last_update'] = date('d F Y h:i', strtotime($tes));

        $user_id        = $lates_update[0]->updateby;
        $get_users      = $this->m_master_data->get_user($user_id);
        $this->data['name_user']    = $get_users->first_name;

        $this->data['master_data'] = $master_data;
        $this->admintemp->view('Masterdata/List_master', $this->data);
    }

    public function add_item()
    {
        check_permission_page(ID_GROUP,'create','master_data');
        $category = $this->m_master_data->get_category();
        // log_r($category);
        $this->data['customers'] = $this->m_master_data->get_customers();
        $this->data['supplier']  = $this->m_master_data->get_supplier();
        $this->data['category']  = $category;
        $this->admintemp->view('Masterdata/Form_add_item', $this->data);
    }

    public function save_add_item()
    {
        $name        = $this->input->post('name');
        $id_category = $this->input->post('id_category');
        $id_customer = $this->input->post('id_customer');
        $id_supplier = $this->input->post('id_supplier');
        $inventory_category = $this->input->post('inventory_category');
        $unit        = $this->input->post('unit');
        $opening     = $this->input->post('opening');
        $remarks     = $this->input->post('remarks');
        $date        = date ('Y-m-d');

        $get_id_item =  $this->m_master_data->get_id_item($id_category);//cek data category
        $code_category = preg_replace("/[^a-zA-Z]/","",$id_category);//cek ID category
        if (!empty($get_id_item)) {
            foreach ($get_id_item as $key => $value) {
                $tes = substr($value->id_item, -4);    
            }
            $tes++;
            $angka_item  = $code_category."-".sprintf("%04s", $tes);
        }else{
            $angka_item  = $code_category."-0001";
        }
        // log_r($angka_item);
        $cek_id_item =  $this->m_master_data->cek_id_item($angka_item);
        if ($cek_id_item == 1) {
            $this->session->set_flashdata('danger', "$id_item is Already !");
            redirect('Master_data/add_item');
        }else {
            $config['upload_path']    = './src/assets/images/foto';
            $config['allowed_types']  = 'gif|jpg|png|jpeg';
            $config['max_size']       = 10000;
            $config['max_width']      = 10024;
            $config['max_height']     = 7688;
            $image      = $_FILES['image']['name'];
            // $contoh = "tes ini";
            $trim_image = str_replace(" ", "_", $image);
            // log_r($trim_image);
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('image')) {
                $data = array(
                    'id_item'     => $angka_item,
                    'id_category' => $id_category,
                    'id_customer' => $id_customer,
                    'id_supplier' => $id_supplier,
                    'inventory_category' => $inventory_category,
                    'unit'        => $unit,
                    'name'        => $name,
                    'opening'     => $opening,
                    'remarks'     => $remarks,
                    'status'      => 2,
                    'date'        => $date,
                    'updateby'    => USER_ID,
                    'last_update' => date("Y-m-d h:i:s")
                );
            } else {
                $data = array(
                    'id_item'     => $angka_item,
                    'id_category' => $id_category,
                    'id_customer' => $id_customer,
                    'id_supplier' => $id_supplier,
                    'inventory_category' => $inventory_category,
                    'unit'        => $unit,
                    'name'        => $name,
                    'opening'     => $opening,
                    'status'      => 2,
                    'remarks'     => $remarks,
                    'image'       => $trim_image,
                    'date'        => $date,
                    'updateby'    => USER_ID,
                    'last_update' => date("Y-m-d h:i:s")
                );
            }
            
            $data_stock = array(
                'id_item'  => $angka_item,
                'opening'  => $opening,
    
            );
        }

        $this->m_master_data->insert_add_item($data);
        $this->m_master_data->insert_tb_stock($data_stock);
        $this->session->set_flashdata('info', "Data has Been Saved!");
        redirect('Master_data');
    }


    public function detail($id_item)
    {
        check_permission_page(ID_GROUP,'read','master_data');
        $data_details = $this->m_master_data->detail($id_item);
        // log_r($data_details); 
        $this->data['customers'] = $this->m_master_data->get_customers();
        $this->data['supplier']  = $this->m_master_data->get_supplier(); 
        $this->data['data_details'] = $data_details;
        $this->admintemp->view('Masterdata/detail', $this->data);
    }

    // download 
    public function download($image=''){
        // log_r($image);
        $file = './src/assets/images/foto/'.$image;
        force_download($file, NULL);
        die();
        // force_download('/src/assets/images/foto/jpun_1.jpg');
    }    

    public function edit($id_item)
    {
        check_permission_page(ID_GROUP,'update','master_data');
        $category = $this->m_master_data->get_category();
        $edit_data = $this->m_master_data->edit($id_item);
        // log_r( $edit_data);
        $this->data['customers'] = $this->m_master_data->get_customers();
        $this->data['supplier']  = $this->m_master_data->get_supplier();
        $this->data['category'] = $category;
        $this->data['data_edit'] = $edit_data;
        $this->admintemp->view('Masterdata/data_edit', $this->data);
    }

    // update
    public function update_data()
    {
        $id_item     = $this->input->post('id_item');
        $id_category = $this->input->post('id_category');
        $id_customer = $this->input->post('id_customer');
        $id_supplier = $this->input->post('id_supplier');
        $inventory_category = $this->input->post('inventory_category');
        $unit        = $this->input->post('unit');
        $opening     = $this->input->post('opening');
        $name        = $this->input->post('name');
        $remarks     = $this->input->post('remarks');
        $status      = $this->input->post('status');

        $config['upload_path']          = './src/assets/images/foto';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 10000;
        $config['max_width']            = 10024;
        $config['max_height']           = 7688;
        $image       = $_FILES['image']['name'];
        $trim_image = str_replace(" ", "_", $image); 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        $call_stock = $this->m_master_data->call_stock($id_item);
        if($call_stock->opening == $opening)
        {
            $total = $opening;
        }else
        {
            $total_balance =  $call_stock->balance_stock - $call_stock->opening;
            $total         =  $total_balance + $opening ;
        }

        if (!$this->upload->do_upload('image')) {
            $data = array(
                'id_item'     => $id_item,
                'id_category' => $id_category,
                'id_customer' => $id_customer,
                'id_supplier' => $id_supplier,
                'inventory_category' => $inventory_category,
                'unit'        => $unit,
                'opening'     => $opening,
                'name'        => $name,
                'remarks'     => $remarks,
                'status'      => $status,
                'updateby'    => USER_ID,
                'last_update' => date("Y-m-d h:i:s")
            );
            $stock = array(
                'opening'       => $opening,
                'balance_stock' => $total,
            );
        } else {
            $data = array('image' => $this->upload->data());
            $data = array(
                'id_item'    => $id_item,
                'id_category' => $id_category,
                'id_customer' => $id_customer,
                'id_supplier' => $id_supplier,
                'inventory_category' => $inventory_category,
                'unit'        => $unit,
                'opening'     => $opening,
                'name'        => $name,
                'remarks'     => $remarks,
                'status'      => $status,
                'image'       => $trim_image,
                'updateby'    => USER_ID,
                'last_update' => date("Y-m-d h:i:s")
            );
            
            $stock = array(
            'opening'       => $opening,
            'balance_stock' => $total,
            );
        }
        
        $this->m_master_data->update_add_item($data, $id_item);
        $this->m_master_data->update_stock($id_item, $stock);
        $this->session->set_flashdata('info', "Data was successfully Updated!");   
        redirect('Master_data/edit/'. $id_item);
    }

    //button hapus
    public function hapus($id_item)
    {   
        check_permission_page(ID_GROUP,'delete','master_data');
        $where = array('id_item' => $id_item);
        $this->m_master_data->hapus_data($where, 'tb_item');
        $this->m_master_data->hapus_data($where, 'tb_stock');
        $this->m_master_data->hapus_data($where, 'tb_item_stock_in');
        $this->m_master_data->hapus_data($where, 'tb_item_stock_out');
        $this->m_master_data->hapus_data($where, 'tb_request');
        // log_r($where);
        redirect('Master_data');
    }

    //add category 
    public function add_category()
    {
        $category = $this->m_master_data->get_category();
        // log_r($category);
        $this->data['category'] = $category;
        // log_r($category);
        $this->admintemp->view('Masterdata/Form_category', $this->data);
    }

    public function save_category()
    {
        $nama_category  = $this->input->post('nama_category');
        $id_category    = $this->input->post('id_category');

        $data = array(
            'id_category'   => $id_category,
            'nama_category' => $nama_category,
        );
        $this->m_master_data->insert_category($data);
        $this->session->set_flashdata('info', "Data has Been Saved!");
        redirect('Master_data/add_category');
    }

    public function edit_category($id_category)
    {
        $edit_category = $this->m_master_data->edit_category($id_category);
        // log_r( $edit_category->nama_category);
        $this->data['edit_category'] = $edit_category;
        $this->admintemp->view('Masterdata/form_edit_category', $this->data);
    }

    public function update_category()
    {
        $nama_category  = $this->input->post('nama_category');
        $id_category    = $this->input->post('id_category');

        $data = array(
            'id_category'   => $id_category,
            'nama_category' => $nama_category,
        );
        $this->m_master_data->update_category($data, $id_category);
        $this->session->set_flashdata('info', "Data was successfully updated!");
        redirect('Master_data/edit_category/'. $id_category);
    }



    public function hapus_category($id_category)
    {
        // $where = array ('id_category' => $id_category);
        // $this->m_master_data->hapus_data($where, 'tb_category');
        $this->db->where('id_category', $id_category);
        $this->db->delete('tb_category');
        // log_r($id_item);
        redirect('Master_data/add_category');
    }

    public function add_customer()
    {
        if ($this->input->post()) {
            $name_cust  = $this->input->post('name_cust');
            $short_name = $this->input->post('short_name');
            $email      = $this->input->post('email');
            $phone      = $this->input->post('phone');

            $cek_data_pertama = $this->m_master_data->data_customer();
            if (empty($cek_data_pertama)) {//cek pertama kali data
                $id_cust    = $short_name."-001";
            }else{
                $cek_data_pertama = $this->m_master_data->data_customer();
                foreach ($cek_data_pertama as $key => $value) {
                    $cut_id = substr($value->id_customer, -3);    
                }
                $cut_id++;
                $id_cust  = $short_name."-".sprintf("%03s", $cut_id);
            }

            $cek_id_customer =  $this->m_master_data->cek_id_customer($id_cust);
            if ($cek_id_customer == 1) {
                $this->session->set_flashdata('error', "data is readily available!");
                redirect('Master_data/add_customer');
            }else{
                $data = array(
                    'id_customer' => $id_cust,
                    'name_cust'   => $name_cust,
                    'short_name'  => $short_name,
                    'email'       => $email,
                    'phone'       => $phone,
                );
                $this->m_master_data->insert_customer($data);
                $this->session->set_flashdata('success', "Data was successfully added!");
                redirect('Master_data/add_customer');
            }
        }else{
            $this->data['data_customer'] = $this->m_master_data->data_customer();
            $this->admintemp->view('Masterdata/add_customer', $this->data);
        }
    }

    public function detail_customer($id='')
    {
        $this->data['detail_customer'] = $this->m_master_data->data_customer_row($id);
        // log_r($this->data['detail_customer']);
        $this->admintemp->view('Masterdata/detail_customer', $this->data);
    }

    public function edit_customer($id='')
    {
        if ($this->input->post()) {
            $id         = $this->input->post('id');
            $name_cust  = $this->input->post('name_cust');
            $short_name = $this->input->post('short_name');
            $email      = $this->input->post('email');
            $phone      = $this->input->post('phone');

            $data = array(
                'name_cust'   => $name_cust,
                'short_name'  => $short_name,
                'email'       => $email,
                'phone'       => $phone,
            );
            
            $data_cust = $this->m_master_data->data_customer_row($id);
            $this->m_master_data->update_customer($data, $id);
            $this->session->set_flashdata('success', "Data was successfully updated!");
            redirect('Master_data/edit_customer/'.$data_cust->id_customer);
        }else{
            $this->data['detail_customer'] = $this->m_master_data->data_customer_row($id);
            $this->admintemp->view('Masterdata/edit_customer', $this->data);
        }
    }

    public function add_supplier()
    {
        if ($this->input->post()) {
            $name_sup   = $this->input->post('name_sup');
            $short_name = $this->input->post('short_name');
            $email      = $this->input->post('email');
            $phone      = $this->input->post('phone');

            $cek_data_pertama = $this->m_master_data->data_supplier();
            if (empty($cek_data_pertama)) {//cek pertama kali data
                $id_supp    = $short_name."-001";
            }else{
                $cek_data_pertama = $this->m_master_data->data_supplier();
                foreach ($cek_data_pertama as $key => $value) {
                    $cut_id = substr($value->id_supplier, -3);    
                }
                $cut_id++;
                $id_supp  = $short_name."-".sprintf("%03s", $cut_id);
            }

            $cek_id_supplier =  $this->m_master_data->cek_id_supplier($id_supp);
            if ($cek_id_supplier == 1) {
                $this->session->set_flashdata('error', "data is readily available!");
                redirect('Master_data/add_supplier');
            }else{
                $data = array(
                    'id_supplier' => $id_supp,
                    'name_sup'    => $name_sup,
                    'short_name'  => $short_name,
                    'email'       => $email,
                    'phone'       => $phone,
                );
                // log_r($data);
                $this->m_master_data->insert_supplier($data);
                $this->session->set_flashdata('success', "Data was successfully added!");
                redirect('Master_data/add_supplier');
            }
        }else{
            $this->data['data_supplier'] = $this->m_master_data->data_supplier();
            $this->admintemp->view('Masterdata/add_supplier', $this->data);
        }
    }

    public function detail_supplier($id='')
    {
        $this->data['detail_supplier'] = $this->m_master_data->detail_supplier_row($id);
        $this->admintemp->view('Masterdata/detail_supplier', $this->data);
    }

    public function edit_supplier($id='')
    {
        if ($this->input->post()) {
            $id_supp    = $this->input->post('id_supp');
            $name_sup   = $this->input->post('name_sup');
            $short_name = $this->input->post('short_name');
            $email      = $this->input->post('email');
            $phone      = $this->input->post('phone');

            $data = array(
                'name_sup'    => $name_sup,
                'short_name'  => $short_name,
                'email'       => $email,
                'phone'       => $phone,
            );
            // log_r($data);
            $this->m_master_data->update_supplier($data, $id_supp);
            $this->session->set_flashdata('success', "Data was successfully updated!");
            redirect('Master_data/edit_supplier/'.$id_supp);
        }else{
            $this->data['detail_supplier'] = $this->m_master_data->detail_supplier_row($id);
            $this->admintemp->view('Masterdata/edit_supplier', $this->data);
        }
    }

    //non active
    public function update_non_active($id_item)
    {
        $this->m_master_data->non_active($id_item);
        redirect('Master_data');
    }

    //active
    public function update_active($id_item)
    {
        $this->m_master_data->active($id_item);
        redirect('Master_data');
    }

}