<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	 function __construct()
    {
        parent::__construct();
        $this->load->model('m_report');
        $this->load->helper('url');
    }

	public function index()
	{
		// $dateawal = "2021-04-05";
		// $dateakhir = "2021-04-06";

		// $begin = new DateTime('2010-05-01');
		// $end = new DateTime('2010-05-10');

		// $interval = DateInterval::createFromDateString('1 day');
		// $period = new DatePeriod($begin, $interval, $end);

		// foreach ($period as $dt) {
		//     $tes = $dt->format("l Y-m-d H:i:s\n");
		// }
	 //    log_r( $tes);


		$tes = $this->m_report->get_data();
		
		
		$this->admintemp->view('report/list_report');
	}

	function dateRange( $first='', $last='', $step = '+1 day', $format = 'Y-m-d' ) {
		$first = "2021-04-05";
		$last = "2021-04-06";
	    $dates = [];
	    $current = strtotime( $first );
	    $last = strtotime( $last );

	    while( $current <= $last ) {

	        $dates = date( $format, $current );
	        $current = strtotime( $step, $current );
	        
	        $coba[] = "SUM(IF(date_out = '".$dates."', stock_out,0)) AS 'tes',";
	    }
	    // print_r($coba);
	    // die();
	    // $this->db->select('id_item, SUM(IF(date_out = "2021-04-05", stock_out,0)) AS "tes", SUM(IF(date_out = "2021-04-06", stock_out,0)) AS "tes"');
	    $tes = $this->m_report->get_data($coba);
	    log_r($tes);
		
		$this->admintemp->view('report/list_report');
	}

	// public function get_report()
	// {
	// 	$tes = $this->m_report->get_repot();
	// }

	
}
