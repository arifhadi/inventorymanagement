<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_request');
        $this->load->helper('url');
    }

    //Index
    public function index()
    {
        check_permission_page(ID_GROUP,'read','approval');
        $request_list = $this->m_request->get_request_list();
        $this->data['request_list'] = $request_list;
        $this->admintemp->view('request/request_home', $this->data);
    }

    public function form($id)
    {
        check_permission_page(ID_GROUP,'update','approval');
        $req_in = $this->m_request->get_req_in($id);
        $this->data['req_in'] = $req_in;
        $this->data['id'] = $id;
        $this->data['balance_stock'] = $req_in->balance_stock;
        $this->admintemp->view('request/form_request', $this->data);
    }

    public function save_request()
    {
        $id              = $this->input->post('id');
        $requestor       = $this->input->post('requestor');
        $id_item         = $this->input->post('id_item');
        $pic             = $this->input->post('pic');
        $total_request   = $this->input->post('total_request');
        $date_request    = $this->input->post('date_request');
        $date_responded  = date('Y-m-d');
        $status          = $this->input->post('status');
        $reason          = $this->input->post('reason');

        $call_balance = $this->m_request->call_balance($id_item);
        if ($status == 3) {
            $total_balance  = $call_balance->balance_stock - 0;
            $this->data['total_balance'] = $total_balance;
        } elseif ($status == 2) {
            $total_stock_out  = $total_request + $call_balance->total_stock_out;
            $this->data['total_stock_out'] = $total_stock_out;
            $total_balance  = $call_balance->balance_stock - $total_request;
            $this->data['total_balance'] = $total_balance;
            $this->session->set_flashdata('info', "Request Has Acceptance!");
        }

        $data = array(
            'id'            => $id,
            'id_item'       => $id_item,
            'requestor'     => $requestor,
            'pic'           => $pic,
            'total_request' => $total_request,
            'date_request'  => $date_request,
            'date_responded' => $date_responded,
            'status'        => $status,
            'reason'        => $reason,
        );

        $this->m_request->update_request($id, $data);
        $total = array(
            'balance_stock' => $total_balance,
            'total_stock_out' => $total_stock_out,

        );
        $this->m_request->update_balance($id_item, $total);

        $tb_stock_out = array(
            'id_item'   => $id_item,
            'pic'       => $pic,
            'stock_out' => $total_request,
            'date_out'  => $date_request,
            'requestor' => $requestor,
        );
        $this->m_request->insert_stock_out($tb_stock_out);

        redirect('request');
    }

    // //Detail
    public function detail($id)
    {
        check_permission_page(ID_GROUP,'read','approval');
        $req_in = $this->m_request->get_req_in($id);
        $this->data['req_in'] = $req_in;
        $this->data['id'] = $id;
        $this->data['balance_stock'] = $req_in->balance_stock;
        $this->admintemp->view('request/detail_request', $this->data);
    }

    //Hapus
    public function hapus($id)
    {
        check_permission_page(ID_GROUP,'delete','approval');
        $where = array('id' => $id);
        $this->m_request->hapus_data($where, 'tb_request');
        redirect('request');
    }
}
