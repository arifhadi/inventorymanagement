    <?php
    defined('BASEPATH') or exit('No direct script access allowed');

    class Requestor extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            $this->load->model('m_requestor');
            $this->load->helper('url');
        }

        //Index
        public function index()
        {
            check_permission_page(ID_GROUP,'read','creating');
            $request = $this->m_requestor->get_request();
            
            $this->data['request'] = $request;
            $this->admintemp->view('requestor/requestor_list', $this->data);
        }

        //Form
        public function form($id_item)
        {
            check_permission_page(ID_GROUP,'create','creating');
            $req_in = $this->m_requestor->get_req_in($id_item);
            $request_list = $this->m_requestor->get_request_list($id_item);
            $this->data['request_list'] = $request_list;

            $this->data['total_request'] = $this->m_requestor->total_request($id_item);

            $this->data['req_in'] = $req_in;
            $this->data['id_item'] = $id_item;
            $this->data['balance_stock'] = $req_in->balance_stock;
            $this->admintemp->view('requestor/form_request', $this->data);
        }

        //Save Request 
        public function save_request()
        {
            $id_item       = $this->input->post('id_item');
            $requestor     = $this->input->post('requestor');
            $total_request = $this->input->post('total_request');
            $date_request  = date('Y-m-d');

            $call_balance = $this->m_requestor->call_balance($id_item);

            if ($call_balance->balance_stock < $total_request) {
                $call_balance->balance_stock;
                $this->session->set_flashdata('danger', "Data tidak berhasil di Simpan!");
                redirect('requestor/form/' . $id_item);
            } else {
                $call_balance->balance_stock;
                $this->session->set_flashdata('info', "Data berhasil di Simpan!");
            }


            $data = array(
                'id_item'       => $id_item,
                'requestor'     => $requestor,
                'date_request'  => $date_request,
                'total_request' => $total_request,
                'status'        => 1,
            );
            // log_r($data );
            $this->m_requestor->insert_request($data);
            redirect('requestor/form/' . $id_item);
        }

        //Detail
        public function detail($id_item)
        {
            check_permission_page(ID_GROUP,'read','creating');
            $data_details = $this->m_requestor->detail($id_item);
            $total_list = $this->m_requestor->total_request($id_item);

            $this->data['total_list'] = $total_list;
            $this->data['data_details'] = $data_details;
            $this->admintemp->view('requestor/detail_request', $this->data);
        }

        public function more_dtl($id)
        {
            check_permission_page(ID_GROUP,'read','creating');
            $data_details = $this->m_requestor->more_dtl($id);
            $this->data['data_details'] = $data_details;
            $this->data['balance_stock'] = $data_details->balance_stock;
            $this->admintemp->view('requestor/more_dtl', $this->data);
        }


        public function tb_edit($id_item)
        {
            check_permission_page(ID_GROUP,'update','creating');
            $tb_list = $this->m_requestor->get_request_list($id_item);
            $total_list = $this->m_requestor->total_request($id_item);
            $this->data['tb_list'] = $tb_list;
            $this->data['total_list'] = $total_list;

            $this->admintemp->view('requestor/tb_edit', $this->data);
        }

        public function edit($id)
        {
            $tb_req = $this->m_requestor->get_tb_req($id);
            $s_edit = $this->m_requestor->get_stock_edit($id);

            $this->data['tb_req'] = $tb_req;
            $this->data['s_edit'] = $s_edit;
            $this->data['id'] = $id;
            $this->data['balance_stock'] = $s_edit->balance_stock;

            $this->admintemp->view('requestor/edit_request', $this->data);
        }

        public function save_edit()
        {
            $id              = $this->input->post('id');
            $requestor       = $this->input->post('requestor');
            $id_item         = $this->input->post('id_item');
            $total_request   = $this->input->post('total_request');
            $date_request    = date('Y-m-d');

            $data = array(
                'id'            => $id,
                'id_item'       => $id_item,
                'requestor'     => $requestor,
                'date_request'  => $date_request,
                'total_request' => $total_request,
                'status'        => 1,

            );
            // log_r($data,);
            $this->m_requestor->update_request($id, $data);

            $this->session->set_flashdata('info', "Data berhasil di Update!");
            redirect('requestor/edit/' . $id);
        }
    }
