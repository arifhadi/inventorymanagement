<?php

defined('BASEPATH') OR exit('No direct script access allowed');

    class Itemout extends CI_Controller{

        function __construct(){
            parent::__construct();		
            $this->load->model('m_itemout');
            $this->load->helper('url');
     
        }

        //index
        public function index()
        {
            check_permission_page(ID_GROUP,'read','outgoing');
            $total_stock_out = $this->m_itemout->get_total_stock_out();
            $this->data['total_stock_out'] = $total_stock_out;
            
            $this->admintemp->view("Itemout/listitemdata", $this->data);
        }

        //form
        public function Form($id_item)
        {
            check_permission_page(ID_GROUP,'update','outgoing');
            $stock_out = $this->m_itemout->get_stock_out($id_item);
            $this->data['stock_out'] = $stock_out;

            $stock_list = $this->m_itemout->get_stock_list($id_item);
            $this->data['stock_list'] = $stock_list;
            // log_r($stock_list);

            $this->data['id_item'] = $id_item;
            $this->data['balance_stock'] = $stock_out->balance_stock;
            $this->data['total_list'] = $this->m_itemout->count_stock($id_item);
            $this->admintemp->view('Itemout/formoutgoing', $this->data);
        }

        //save form
        public function save_form()
        {
            $id             = $this->input->post('id');
            $pic            = $this->input->post('pic');
            $id_item        = $this->input->post('id_item');
            $balance_stock  = $this->input->post('balance_stock');
            $stock_out      = $this->input->post('stock_out');
            $date_out       = $this->input->post('date_out');
            // log_r($stock_out);

            $cek_stock_out = $this->m_itemout->cek_stock_out($id_item);
            // log_r($cek_stock_out);
            $call_balance = $this->m_itemout->call_balance($id_item);
            // log_r($call_balance);
            if ($call_balance->balance_stock < $stock_out) {
                // log_r("ulang");
                $call_balance->balance_stock;
                $this->session->set_flashdata('danger', "Data tidak berhasil di Simpan!");
                redirect('itemout/form/' . $id_item);
            } else {
                // log_r("berhasil");
                $this->session->set_flashdata('info', "Data berhasil di Simpan!");
                $total_balance  = $call_balance->balance_stock - $stock_out;
                $this->data['total_balance'] = $total_balance;
            }

            $data = array(  
                'pic'        => $pic,
                'id_item'    => $id_item, 
                'stock_out'  => $stock_out,
                'date_out'   => $date_out,
            );
            // log_r($data);
            $this->m_itemout->insert_form($data);  
            $count = $this->m_itemout->count_stock($id_item);

            $total = array (
                'total_stock_out'     => $count->stock_out,
                'balance_stock'       => $total_balance,

            );
            // log_r($total);
            // $total = $count->stock_out;
            $this->m_itemout->count_item_stock($id_item, $total);
            redirect('Itemout/form/' . $id_item);  
        }

        //detail
        public function detail($id_item='')
        {
            check_permission_page(ID_GROUP,'read','outgoing');
            $data_details = $this->m_itemout->detail($id_item);
            $this->data['per_mount'] = $this->m_itemout->per_mount($id_item);
            $this->data['data_details'] = $data_details;
            $this->data['total_list'] = $this->m_itemout->count_stock($id_item);
            // log_r($this->data['per_mount']);
            $this->admintemp->view('Itemout/detail', $this->data);
        }

        //edit
        public function edit($id_item) 
        {
            check_permission_page(ID_GROUP,'update','outgoing');
            $stock_out = $this->m_itemout->get_stock_out($id_item);
            $this->data['stock_out'] = $stock_out;
    
            $stock_list = $this->m_itemout->get_stock_list($id_item);
            $this->data['stock_list'] = $stock_list;

            $stock_form = $this->m_itemout->get_stock_form($id_item);
            $this->data['stock_form'] = $stock_form;

            $this->data['id_item'] = $id_item;
            $this->data['total_list'] = $this->m_itemout->count_stock($id_item);
            $this->admintemp->view('Itemout/edit', $this->data);
        }

        // form edit
        public function form_edit($id)
        {
            check_permission_page(ID_GROUP,'update','outgoing');
            $stock_form = $this->m_itemout->get_stock_form($id);
            $this->data['stock_form'] = $stock_form;
            // log_r($stock_form);

            $this->data['id'] = $id;
            $this->admintemp->view('Itemout/form_edit', $this->data);
        }
        
        // Update
        public function save_form_edit()
        {
            $id          = $this->input->post('id');
            $pic         = $this->input->post('pic');
            $id_item     = $this->input->post('id_item');
            $stock_out   = $this->input->post('stock_out');
            $date_out    = $this->input->post('date_out');

            $data = array(
                'pic'         => $pic,
                'id_item'     => $id_item,
                'stock_out'   => $stock_out,
                'date_out'    => $date_out,

            );
            // log_r($data,);
            $update_out = $this->m_itemout->update_itemout($id, $data);
            // log_r($update_out);
            
            $count = $this->m_itemout->count_stock($id_item);

            $data_out = array(
                'total_stock_out'   => $count->stock_out,
            );
            $this->m_itemout->update_edit($id_item, $data_out);

            $call_balance = $this->m_itemout->call_balance($id_item);
            // log_r($call_balance); 

            if ($call_balance->balance_stock < $count->stock_out) {
                // log_r("ulang");
                $call_balance->balance_stock;
                $this->session->set_flashdata('danger', "Data tidak berhasil di Update!");
                redirect('Itemout/form_edit/' . $id);
            } else {
                // log_r("berhasil");
                $this->session->set_flashdata('info', "Data berhasil di Update!");
                $total_balance = $call_balance->opening + $call_balance->total_stock_in - $call_balance->total_stock_out;
                $this->data['total_balance'] = $total_balance;
            }

            $total = array(
                'total_stock_out'   => $count->stock_out,
                'balance_stock'    => $total_balance,
            );
            $total = $this->m_itemout->update_balance_out($id_item, $total);

            $this->session->set_flashdata('info', "Data berhasil di Update!");
            redirect('Itemout/edit/' . $id_item); 
        }

        //Hapus
        public function hapus($id_item)
        {
            check_permission_page(ID_GROUP,'delete','outgoing');
            $where = array('id_item' => $id_item);
            $this->m_itemout->hapus_data($where, 'tb_item');
            $this->m_itemout->hapus_data($where, 'tb_stock');
            $this->m_itemout->hapus_data($where, 'tb_item_stock_out');
            // log_r($where);
            redirect('Itemout');
        }
    }

?>