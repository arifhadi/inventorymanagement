<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
  
<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 5px;
        margin-bottom: 0;
        font-size: 23px;
    }

    #button1 {
        margin-top: 30px;
        left: 48%;
    }


    .control-label:after {
        content: "*";
        color: red;
    }
</style>

<div class="page">
    <div class="page-header" style="padding: 0px;"><br>
        <div class="col-md-12">
            <!-- button-->
            <a href="<?php echo base_url("Dashboard/indirect") ?>" type="button" class="btn btn-danger btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
                <span class="ladda-label"><i class="icon md-home mr-10" aria-hidden="true"></i>Home</span>
            </a>
            <a href="<?php echo base_url("itemout") ?>" type="button" class="btn btn-info btn-round" data-style="slide-left" data-plugin="ladda" data-type="progress" id="back">
                <span class="ladda-label"><i class="icon md-undo" aria-hidden="true"></i>Back</span>
            </a>
        </div>
    </div>

    <h3 align="center">Out Stock</h3>

    <div class="page-content container-fluid" style="padding: 0px;">
        <div class="row">
            <div class="col-lg-6">
                <!-- <h1 align="center">Form Add Stock</h1> -->
                <!-- Panel Basic -->
                <div class="panel">
                    <div class="panel-body">
                    <?php if($this->session->flashdata('info')){?>
                            <div class="alert dark alert-success alert-dismissible" role="alert" id="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            <?= $this->session->flashdata('info');?>
                            </div>
                    <?php }  elseif ($this->session->flashdata('danger')){?>
                        <div class="alert dark alert-danger alert-dismissible" role="alert" id="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            <?= $this->session->flashdata('danger');?>
                            </div>
                    <?php }?>
                    <?php echo form_open_multipart('itemout/save_form'); ?>
                    <h3 Align="center">Form Out Stock</h3>
                        <div class="form-group form-material row">
                            <label class="col-md-3 col-form-label control-label"><b> PIC </b></label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="pic" placeholder="PIC" autocomplete="off" required value="<?= USER_NAME ?>" readonly>
                            </div>
                        </div>

                        <div class="form-group form-material row">
                            <label class="col-md-3 col-form-label control-label"><b>ID Item </b></label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="id_item" placeholder="ID Item" autocomplete="off" required readonly value="<?= $id_item ?>">
                            </div>
                        </div>

                        <div class="form-group form-material row">
                            <label class="col-md-3 col-form-label control-label"><b>Total Stock In</b></label>
                            <div class="col-md-6">
                                <input type="text" readonly class="form-control" name="total_stock_in" placeholder="total_stock_in" autocomplete="off" required value="<?= $balance_stock ?>" />
                            </div>
                        </div>

                        <div class="form-group form-material row">
                            <label class="col-md-3 col-form-label control-label"><b> Stock Out </b></label>
                            <div class="col-md-6">
                                <input type="number" min="1" max="<?= $balance_stock ?>" step="1" class="form-control" name="stock_out" placeholder="Stock out" autocomplete="off" required>
                            </div>
                        </div>

                        <div class="form-group form-material row">
                            <label class="col-md-3 col-form-label control-label"><b>Date Out </b></label>
                            <div class="col-md-6">
                                <input type="date" class="form-control" name="date_out" placeholder="Date" autocomplete="off" readonly required value="<?= date('Y-m-d'); ?>">
                            </div>
                        </div>

                        <div class="form-group form-material row">
                            <button type="submit" class="btn btn-sm btn-success btn-round mt--10" id="button1">
                                <span class="text">&nbsp;&nbsp;&nbsp; SAVE &nbsp;&nbsp;&nbsp;</span>
                            </button>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- End Panel Form -->
            </div>

            <div class="col-lg-6">
                <!-- Panel Extended -->
                <div class="panel">
                    <div class="panel-body">
                        <h3 align="center">List Stock Out</h3>
                        <div class="row">
                            <div class="col-md-12">
                            </div>
                        </div>
                        <?php echo form_open_multipart('Itemout/update_stock_out'); ?>
                            <table class="table table-bordered table-hover table-striped" cellspacing="0" id="exampleTableSearch" data-mobile-responsive="true">
                                <thead>
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th class="text-center">PIC</th>
                                        <th class="text-center">ID Item</th>
                                        <th class="text-center">Date Out</th>
                                        <th class="text-center">Stock Out</th>
                                    </tr>
                                </thead>
                                <tbody> 
                                <?php $no = 1; foreach($stock_list as $value){ ?>
                                    <tr>
                                        <td class="text-center"><?= $no++ ?></td>
                                        <td class="text-center"><?= $value->pic ?></td>
                                        <td class="text-center"><?= $value->id_item ?></td>
                                        <td class="text-center"><?= $value->date_out ?></td>
                                        <td class="text-center"><?= $value->stock_out ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                                <tr>
                                    <td class="text-center" colspan="4"><b>Total<b></td>
                                    <td class="text-center" style="color:red"><?= $total_list->stock_out ?></td>
                                </tr>
                            </table>
                            <?php echo form_close(); ?>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>