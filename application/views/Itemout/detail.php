<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 5px;
        margin-bottom: 0;
        font-size: 30px;
    }

    .button {
        display: flex;
        color: white;
        justify-content: space-evenly;
    }

    .container {
        top: 50%;
    }
</style>
<!-- button-->
<div class="page">
    <div class="page-header" style="padding: 0px;"><br>
        <a href="<?php echo base_url("itemout/index") ?>" type="button" class="btn btn-info btn-round" data-style="slide-left" data-plugin="ladda" data-type="progress" id="home">
            <span class="ladda-label"><i class="icon md-undo" aria-hidden="true"></i>Back</span>
        </a>
    </div>

    <h3 Align="center">Detail Out Stock</h3>

    <div class="page-content" style="padding: 0px;">
        <!-- button-->
        <div class=" col-md-">
            <div class="panel">
                <!-- Tabel List -->
                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                            </div>
                        </div>
                        <table class="table table-bordered table-hover table-striped" cellspacing="0" id="exampleTableSearch">
                            <thead>
                                <tr>
                                    <th class="text-center" width="20">No.</th>
                                    <th class="text-center" width="50">PIC</th>
                                    <th class="text-center" width="50">ID Item</th>
                                    <th class="text-center" width="50">Date Out</th>
                                    <th class="text-center" width="50">Stock Out</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; foreach ($data_details as $value) { ?>
                                    <tr>
                                        <td class="text-center"><?= $no++ ?></td>
                                        <td class="text-center"><?= $value->pic ?></td>
                                        <td class="text-center"><?= $value->id_item ?></td>
                                        <td class="text-center"><?= $value->date_out ?></td>
                                        <td class="text-center"><?= $value->stock_out ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                                <tr>
                                    <td class="text-center" style="background-color: white;" colspan="4"><b>Total<b></td>
                                    <td class="text-center" style="color:red; background-color: white;"><b><?= $total_list->stock_out ?></b></td>
                                </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>