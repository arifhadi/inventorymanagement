<style>
    #table{
        position: absolute;
        display: block;
        width: 1225px;
        height: auto;
        top: 50%;
        left: 50%;
        transform: translate(-50%, 29%);
    }

    .button{
        display: flex;
        color: white;
        justify-content:space-evenly;
    }

    #jan{
        color: white;
        background-color: blueviolet;
        margin: 0 -20px 0 -20px;
    }

    #button2{
        margin-left: -1px;
        margin-right: -1px;
    }
</style>

<!-- button-->
<div class="page">
    <div class="page-header" style="padding: 0px;"><br>
        <div class="col-md-12">
        <a href="<?php echo base_url("Dashboard/indirect") ?>" type="button" class="btn btn-round btn-danger" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
            <span class="ladda-label"><i class="icon md-home mr-10" aria-hidden="true"></i>Home</span>
            <!-- style="background-color: hsl(227, 46%, 50%);border:hsl(227, 46%, 50%)" -->
        </a>
        </div>
    </div>

    <h3 Align="center">Tabel Outgoing Item</h3>
                        
    <div class="page-content" style="padding: 0px;">
        <!-- button-->
        <div class=" col-md-12">
            <div class="panel">
                <!-- alert -->
                <?php if ($this->session->flashdata('info')) { ?>
                    <div class="alert dark alert-success alert-dismissible" role="alert" id="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?= $this->session->flashdata('info'); ?>
                    </div>
                <?php } ?>
                <!-- Tabel List -->
                <div class="panel-body">
                    <div class="panel">
                        <div class="panel-body">
                            <table class="table table-hover dataTable table-striped w-full no-footer dtr-inline" cellspacing="0" id="exampleTableSearch">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="20">No.</th>
                                        <th class="text-center">Id Item</th>
                                        <th class="text-center">Name</th>
                                        <th class="text-center">Unit</th>
                                        <th class="text-center">Total Stock Out</th>
                                        <th class="text-center">Balance Stock</th>
                                        <th class="text-center">Out Stock</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; foreach($total_stock_out as $value){ ?>
                                        <tr>
                                            <td><?=$no++?></td>
                                            <td><?=$value->id_item?></td>
                                            <td><?=$value->name?></td>
                                            <td><?=$value->unit?></td>
                                            <td class="text-center"><?=$value->total_stock_out?></td>
                                            <td class="text-center"><?=$value->balance_stock?></td>
                                            <td class="text-center">
                                                <?php if(check_permission_view(ID_GROUP,'create','outgoing')) { ?>
                                                    <button type='button' data-bind='<?= $value->id_item ?>'  class='btn out_stock btn-floating btn-xs btn-warning' data-placement="top"><i class="icon md-minus-circle-outline" aria-hidden="true" disable="disable"></i></button>
                                                <?php } ?>
                                            </td>
                                            <td class="button">
                                                <?php if(check_permission_view(ID_GROUP,'read','outgoing')) { ?>
                                                    <button type='submit' data-bind='<?= $value->id_item ?>' class='btn detail btn-floating btn-info btn-xs'><i class="icon md-assignment-check" aria-hidden="true"></i></button>
                                                <?php } ?>

                                                <?php if(check_permission_view(ID_GROUP,'update','outgoing')) { ?>
                                                    <button type='submit' data-bind='<?= $value->id_item ?>' class='btn edit btn-floating btn-success btn-xs'><i class="icon md-icon md-edit" aria-hidden="true"></i></button>
                                                <?php } ?>

                                                <?php if(check_permission_view(ID_GROUP,'delete','outgoing')) { ?>
                                                    <button type='button' data-bind='<?= $value->id_item ?>' class='btn nonactive btn-floating btn-danger btn-xs'><i class="icon md-icon md-delete" aria-hidden="true"></i></button>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Alert Btn Delete -->
<script type="text/javascript">
    $(".nonactive").click(function() {
        var id = $(this).attr("data-bind");
        console.log(id);
        swal({
                title: "Are you sure want to delete?",
                // text: "you will change this data to Non-Active !",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Delete !",
                cancelButtonText: "Cancel !",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: '<?= base_url("Itemout/hapus/") ?>' + id,
                        type: 'DELETE',
                        error: function() {
                            alert('Something is wrong');
                        },
                        success: function(data) {
                            $("#" + id).remove();
                            swal("Non-Active !", "Your Data is Delete", "success");
                            window.location.reload();
                        }
                    });
                } else {
                    swal("Cancelled", "You Canceled To Delete :)", "error");
                }
            });
    });
</script>
<!-- End Alert Button Delete -->
 
<!-- Alert Button Edit -->
<script type="text/javascript">
    //Change Data
    $(".edit").click(function() {
        var id = $(this).attr("data-bind");

        swal({
                title: "Are you sure you want to change the data?",
                text: "",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Yes",
                cancelButtonText: "Cancel",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        // url: '<?= base_url("itemout/edit/") ?>'+id,
                        type: 'DELETE',
                        error: function() {
                            alert('Something is wrong');
                        },
                        success: function(data) {
                            $("#" + id).remove();
                            // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            window.location.href = '<?= base_url("itemout/edit/") ?>' + id;
                        }
                    });
                } else {
                    swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });
    });
</script>
<!-- End Alert Button Edit -->

<!-- Alert Button Detail -->
<script type="text/javascript">
    //Change Data
    $(".detail").click(function() {
        var id = $(this).attr("data-bind");

        swal({
                title: "Are you sure you want to see detail?",
                text: "",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: "btn-info",
                confirmButtonText: "Yes",
                cancelButtonText: "Cancel",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        // url: '<?= base_url("itemout/detail/") ?>'+id,
                        type: 'DELETE',
                        error: function() {
                            alert('Something is wrong');
                        },
                        success: function(data) {
                            $("#" + id).remove();
                            // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            window.location.href = '<?= base_url("itemout/detail/") ?>' + id;
                        }
                    });
                } else {
                    swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });
    });
</script>
<!-- End Alert Button Detail -->

<!-- Alert Button Outstock -->
<script type="text/javascript">
    //Change Data
    $(".out_stock").click(function() {
        var id = $(this).attr("data-bind");

        swal({
                title: "Are you sure you want to out stock?",
                text: "",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: "btn-warning",
                confirmButtonText: "Yes",
                cancelButtonText: "Cancel",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        // url: '<?= base_url("itemout/form/") ?>'+id,
                        type: 'DELETE',
                        error: function() {
                            alert('Something is wrong');
                        },
                        success: function(data) {
                            $("#" + id).remove();
                            // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            window.location.href = '<?= base_url("itemout/form/") ?>' + id;
                        }
                    });
                } else {
                    swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });
    });
</script>
<!-- End Alert Button Outstock-->