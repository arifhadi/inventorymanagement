<?php defined('BASEPATH') or exit('No direct script access allowed');?>

<style>
    #buttone{
        margin: 0 -100px 0 75px;
        padding-right: -100px;
    }
    .title{
        margin-top: -7px;
    }
</style>
 
<div class="page">
    <div class="page-header" style="padding: 0px;"><br>
        <a href="<?php echo base_url("Dashboard/indirect") ?>" type="button" class="btn btn-danger btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
            <span class="ladda-label"><i class="icon md-home mr-10" aria-hidden="true"></i>Home</span>
        </a>
        <a href="<?php echo base_url("Itemout/index") ?>" type="button" class="btn btn-info btn-round" data-style="slide-left" data-plugin="ladda" data-type="progress" id="home">
            <span class="ladda-label"><i class="icon md-undo" aria-hidden="true"></i>Back</span>
        </a>
    </div>

    <h3 class="title" Align="center">Edit Stock Out</h3>

    <div class="page-content" style="padding: 0px;">
        <!-- Button -->
        <div class=" col-md-">
            <div class="panel">
            <!-- Alert -->
            <?php if ($this->session->flashdata('info')) { ?>
                <div class="alert dark alert-success alert-dismissible" role="alert" id="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button><?= $this->session->flashdata('info'); ?>
                </div>
            <?php } ?>
            <!-- Tabel List -->
                <div class="panel-body">
                    <table class="table table-bordered table-hover table-striped" cellspacing="0" id="exampleTableSearch">
                        <thead>
                            <tr>
                                <th class="text-center">No.</th>
                                <th class="text-center">PIC</th>
                                <th class="text-center">ID Item</th>
                                <th class="text-center">Date Out</th>
                                <th class="text-center">Stock Out</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no = 1; foreach($stock_list as $value){ ?>
                            <tr>
                                <td class="text-center"><?= $no++ ?></td>
                                <td class="text-center"><?= $value->pic ?></td>
                                <td class="text-center"><?= $value->id_item ?></td>
                                <td class="text-center"><?= $value->date_out ?></td>
                                <td class="text-center"><?= $value->stock_out ?></td>
                                <td>
                                    <?php if(check_permission_view(ID_GROUP,'update','outgoing')) { ?>
                                        <button type='submit' data-bind='<?= $value->id ?>' id="buttone" class='btn btn-success edit_stock btn-floating btn-success btn-xs'><i class="icon md-icon md-edit" aria-hidden="true"></i></button>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                            <tr>
                                <td class="text-center" colspan="4"><b>Total<b></td>
                                <td class="text-center" style="color:red"><?= $total_list->stock_out ?></td>
                            </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Tabel List -->
        
<script type="text/javascript">
    //Change Data
    $(".edit_stock").click(function() {
        var id = $(this).attr("data-bind");

        swal({
            title: "Are you sure you want to edit stock?",
                text: "",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Yes !",
                cancelButtonText: "Cancel !",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: 'DELETE',
                        error: function() {
                            alert('Something is wrong');
                        },
                        success: function(data) {
                            $("#" + id).remove();
                            // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            window.location.href = '<?= base_url("itemout/form_edit/") ?>' + id;
                        }
                    });
                } else {
                    swal("Cancelled", "You Canceled To Delete :)", "error");
                }
            });
    });
</script>