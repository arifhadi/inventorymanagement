<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
    <style>
        .card-header{
            margin: 0 0 -5px 0 ;
            background-color: #36459b;
        }
        #quest{
            margin-top: -60px;
        }
        #card{
            margin: -10px 350px 0 350px;
            height: 460px;
        }
        #card1{
            margin: -10px 350px 0 350px;
            height: auto;
            width: auto;
        }
        #button1{
            margin-top: 10px;
            left: 48%;
        }
        .form-material{
            display: flex;
        }
        .control-label:after {
                content: "*";
                color: red;
            }
        </style>

<div class="page">
    <div class="page-header">
        <!-- <a href="<?php echo base_url("Dashboard") ?>" type="button" class="btn btn-info btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
            <span class="ladda-label"><i class="icon md-home mr-10" aria-hidden="true"></i>Home</span>
        </a> -->
        <a href="<?php echo base_url("itemout/edit/" . $stock_form->id_item) ?>" type="button" class="btn btn-info btn-round" data-style="slide-left" data-plugin="ladda" data-type="progress">
            <span class="ladda-label"><i class="icon md-undo" aria-hidden="true"></i>Back</span>
        </a>
    </div>
        <!-- Example Panel With All -->
        <h1 Align="center">Form Edit Outstock</h1>
        <br>
                <div class="card" id="card">
                    <?php if($this->session->flashdata('info')){?>
                            <div class="alert dark alert-success alert-dismissible" role="alert" id="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            <?= $this->session->flashdata('info');?>
                            </div>
                    <?php }  elseif ($this->session->flashdata('danger')){?>
                        <div class="alert dark alert-danger alert-dismissible" role="alert" id="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            <?= $this->session->flashdata('danger');?>
                            </div>
                    <?php }?>

                    <div class="card-body">
                        <?php echo form_open_multipart('itemout/save_form_edit/'); ?>

                        <br><br>

                        <div class="form-group form-material row" hidden>
                            <label class="col-md-3 col-form-label control-label"><b>Id </b></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="id" placeholder="id" autocomplete="off" required value="<?= $stock_form->id ?>" />
                            </div>
                        </div>
                        <br>


                        <div class="form-group form-material row" id="quest">
                            <label class="col-md-3 col-form-label control-label"><b>PIC </b></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="pic" placeholder="PIC" autocomplete="off" required readonly value="<?= $stock_form->pic ?>" />
                            </div>
                        </div>
                        <br>

                        <div class="form-group form-material row">
                            <label class="col-md-3 col-form-label control-label"><b>ID Item </b></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="id_item" placeholder="ID Item" autocomplete="off" required readonly value="<?= $stock_form->id_item ?>" />
                            </div>
                        </div>
                        <br>

                        <div class="form-group form-material row">
                            <label class="col-md-3 col-form-label control-label"><b>Total Stock In</b></label>
                            <div class="col-md-9">
                                <input type="text" readonly class="form-control" name="balance_stock" placeholder="Total Stock In" autocomplete="off" required value="<?= $stock_form->balance_stock ?>" />
                            </div>
                        </div>
                        <br>
 
                        <div class="form-group form-material row">
                            <label class="col-md-3 col-form-label control-label"><b>Stock Out </b></label>
                            <div class="col-md-9">
                                <input type="number" min="1" max="<?= $stock_form->balance_stock ?>" step="1" class="form-control" name="stock_out" placeholder="Stock Out" autocomplete="off" required value="<?= $stock_form->stock_out ?>" />
                            </div>
                        </div>
                        <br>

                        <div class="form-group form-material row">
                            <label class="col-md-3 col-form-label control-label"><b>Date Out </b></label>
                            <div class="col-md-9">
                                <input type="date" class="form-control" name="date_out" placeholder="Date Out" autocomplete="off" required value="<?= $stock_form->date_out;?>">
                            </div>
                        </div>

                        <button type="submit" class="btn btn-sm btn-success btn-round" id="button1">
                            <span class=" text">SAVE</span>
                            </a>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>