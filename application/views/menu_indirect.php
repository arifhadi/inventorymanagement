<style>
	body {
		font-family: "Libre Baskerville", serif;
		font-weight: 400;
		font-size: 16px;
		line-height: 30px;
		background-color: #0c0f15;
		color: #ababab;
	}

	.heading-page {
		text-transform: uppercase;
		font-size: 43px;
		font-weight: bolder;
		letter-spacing: 3px;
		color: white;
	}

	a {
		color: inherit;
		-webkit-transition: all 0.3s ease 0s;
		-moz-transition: all 0.3s ease 0s;
		-o-transition: all 0.3s ease 0s;
		transition: all 0.3s ease 0s;
	}

	a:hover,
	a:focus {
		color: #ababab;
		text-decoration: none;
		outline: 0 none;
	}

	h1,
	h2,
	h3,
	h4,
	h5,
	h6 {
		color: #1e2530;
		font-family: "Open Sans", sans-serif;
		margin: 0;
		line-height: 1.3;
	}

	p {
		margin-bottom: 20px;
	}

	p:last-child {
		margin-bottom: 0;
	}

	/*
 * Selection color
 */
	::-moz-selection {
		background-color: #FA6862;
		color: #fff;
	}

	::selection {
		background-color: #FA6862;
		color: #fff;
	}

	/*
 *  Reset bootstrap's default style
 */
	.form-control::-webkit-input-placeholder,
	::-webkit-input-placeholder {
		opacity: 1;
		color: inherit;
	}

	.form-control:-moz-placeholder,
	:-moz-placeholder {
		/* Firefox 18- */
		opacity: 1;
		color: inherit;
	}

	.form-control::-moz-placeholder,
	::-moz-placeholder {
		/* Firefox 19+ */
		opacity: 1;
		color: inherit;
	}

	.form-control:-ms-input-placeholder,
	:-ms-input-placeholder {
		opacity: 1;
		color: inherit;
	}
	
	.fil{
		fill: #007399;
	}

	button,
	input,
	select,
	textarea,
	label {
		font-weight: 00;
	}

	.btn {
		-webkit-transition: all 0.3s ease 0s;
		-moz-transition: all 0.3s ease 0s;
		-o-transition: all 0.3s ease 0s;
		transition: all 0.3s ease 0s;
	}

	.btn:hover,
	.btn:focus,
	.btn:active:focus {
		outline: 0 none;
	}

	.btn-primary {
		background-color: #FA6862;
		border: 0;
		font-family: "Open Sans", sans-serif;
		font-weight: 700;
		height: 48px;
		line-height: 50px;
		padding: 0 42px;
		text-transform: uppercase;
	}

	.btn-primary:hover,
	.btn-primary:focus,
	.btn-primary:active,
	.btn-primary:active:focus {
		background-color: #f9423a;
	}

	.btn-border {
		border: 1px solid #d7d8db;
		display: inline-block;
		padding: 7px;
	}

	/*
 *  CSS Helper Class
 */
	.clear:before,
	.clear:after {
		content: " ";
		display: table;
	}

	.clear:after {
		clear: both;
	}

	.pt-table {
		display: table;
		width: 100%;
		height: -webkit-calc(100vh - 4px);
		height: -moz-calc(100vh - 4px);
		height: calc(100vh - 4px);
	}

	.pt-tablecell {
		display: table-cell;
		vertical-align: middle;
	}

	.overlay {
		position: absolute;
		left: 0;
		top: 0;
		width: 100%;
		height: 100%;
	}

	.relative {
		position: relative;
	}

	.primary,
	.link:hover {
		color: #FA6862;
	}

	.no-gutter {
		margin-left: 0;
		margin-right: 0;
	}

	.no-gutter>[class^="col-"] {
		padding-left: 0;
		padding-right: 0;
	}

	.flex {
		display: -webkit-box;
		display: -webkit-flex;
		display: -moz-flex;
		display: -ms-flexbox;
		display: flex;
	}

	.flex-middle {
		-webkit-box-align: center;
		-ms-flex-align: center;
		-webkit-align-items: center;
		-moz-align-items: center;
		align-items: center;
	}

	.space-between {
		-webkit-box-pack: justify;
		-ms-flex-pack: justify;
		-webkit-justify-content: space-between;
		-moz-justify-content: space-between;
		justify-content: space-between;
	}

	.nicescroll-cursors {
		background: #FA6862 !important;
	}

	.preloader {
		bottom: 0;
		left: 0;
		position: fixed;
		right: 0;
		top: 0;
		z-index: 1000;
		display: -webkit-box;
		display: -webkit-flex;
		display: -moz-flex;
		display: -ms-flexbox;
		display: flex;
	}

	.preloader.active.hidden {
		display: none;
	}

	.loading-mask {
		background-color: #FA6862;
		height: 100%;
		left: 0;
		position: absolute;
		top: 0;
		width: 20%;
		-webkit-transition: all 0.6s cubic-bezier(0.61, 0, 0.6, 1) 0s;
		-moz-transition: all 0.6s cubic-bezier(0.61, 0, 0.6, 1) 0s;
		-o-transition: all 0.6s cubic-bezier(0.61, 0, 0.6, 1) 0s;
		transition: all 0.6s cubic-bezier(0.61, 0, 0.6, 1) 0s;
	}

	.loading-mask:nth-child(2) {
		left: 20%;
		-webkit-transition-delay: 0.1s;
		-moz-transition-delay: 0.1s;
		-o-transition-delay: 0.1s;
		transition-delay: 0.1s;
	}

	.loading-mask:nth-child(3) {
		left: 40%;
		-webkit-transition-delay: 0.2s;
		-moz-transition-delay: 0.2s;
		-o-transition-delay: 0.2s;
		transition-delay: 0.2s;
	}

	.loading-mask:nth-child(4) {
		left: 60%;
		-webkit-transition-delay: 0.3s;
		-moz-transition-delay: 0.3s;
		-o-transition-delay: 0.3s;
		transition-delay: 0.3s;
	}

	.loading-mask:nth-child(5) {
		left: 80%;
		-webkit-transition-delay: 0.4s;
		-moz-transition-delay: 0.4s;
		-o-transition-delay: 0.4s;
		transition-delay: 0.4s;
	}

	.preloader.active.done {
		z-index: 0;
	}

	.preloader.active .loading-mask {
		width: 0;
	}

	/*------------------------------------------------
	Start Styling
-------------------------------------------------*/
	.mt20 {
		margin-top: 20px;
	}

	.page-close {
		font-size: 30px;
		position: absolute;
		right: 30px;
		top: 30px;
		z-index: 100;
	}

	.page-title {
		margin-bottom: 75px;
	}

	.page-title img {
		margin-bottom: 20px;
	}

	.page-title h2 {
		font-size: 68px;
		margin-bottom: 25px;
		position: relative;
		z-index: 0;
		font-weight: 900;
		text-transform: uppercase;
	}

	.page-title p {
		font-size: 16px;
	}

	.page-title .title-bg {
		color: rgba(30, 37, 48, 0.07);
		font-size: 158px;
		left: 0;
		letter-spacing: 10px;
		line-height: 0.7;
		position: absolute;
		right: 0;
		top: 50%;
		z-index: -1;
		-webkit-transform: translateY(-50%);
		-moz-transform: translateY(-50%);
		-ms-transform: translateY(-50%);
		-o-transform: translateY(-50%);
		transform: translateY(-50%);
	}

	.section-title {
		margin-bottom: 20px;
	}

	.section-title h3 {
		display: inline-block;
		position: relative;
	}

	.section-title h3::before,
	.section-title h3::after {
		content: "";
		height: 2px;
		position: absolute;
		bottom: 8px;
		left: -webkit-calc(100% + 14px);
		left: -moz-calc(100% + 14px);
		left: calc(100% + 14px);
	}

	.section-title h3::before {
		background-color: #1e2530;
		width: 96px;
		bottom: 14px;
	}

	.section-title h3::after {
		background-color: #FA6862;
		width: 73px;
	}

	.section-title.light h3 {
		color: #fff;
	}

	.section-title.light h3::before {
		background-color: #fff;
	}

	.page-nav {
		bottom: 40px;
		left: 0;
		position: absolute;
		right: 0;
	}

	.page-nav span {
		font-family: "Open Sans", sans-serif;
		font-size: 14px;
		font-weight: 500;
		line-height: 0.9;
		text-transform: uppercase;
	}

	/*------------------------------------------------
    Home Page
-------------------------------------------------*/

	.hexagon-item:first-child {
		margin-left: 0;
	}

	.page-home {
		background-position: center center;
		background-repeat: no-repeat;
		background-size: cover;
		vertical-align: middle;
	}

	.page-home .overlay {
		background-color: rgba(14, 17, 24, 0.78);
	}

	/* End of container */
	.hexagon-item {
		cursor: pointer;
		width: 200px;
		height: 173.20508px;
		float: left;
		margin-left: -29px;
		z-index: 0;
		position: relative;
		-webkit-transform: rotate(30deg);
		-moz-transform: rotate(30deg);
		-ms-transform: rotate(30deg);
		-o-transform: rotate(30deg);
		transform: rotate(30deg);
	}

	.hexagon-item:first-child {
		margin-left: 0;
	}

	.hexagon-item:hover {
		z-index: 1;
	}

	.hexagon-item:hover .hex-item:last-child {
		opacity: 1;
		-webkit-transform: scale(1.3);
		-moz-transform: scale(1.3);
		-ms-transform: scale(1.3);
		-o-transform: scale(1.3);
		transform: scale(1.3);
	}

	.hexagon-item:hover .hex-item:first-child {
		opacity: 1;
		-webkit-transform: scale(1.2);
		-moz-transform: scale(1.2);
		-ms-transform: scale(1.2);
		-o-transform: scale(1.2);
		transform: scale(1.2);
	}

	.hexagon-item:hover .hex-item:first-child div:before,
	.hexagon-item:hover .hex-item:first-child div:after {
		height: 5px;
	}

	.hexagon-item:hover .hex-item div::before,
	.hexagon-item:hover .hex-item div::after {
		background-color: #ff0037;
	}

	.hexagon-item:hover .hex-content svg {
		-webkit-transform: scale(0.97);
		-moz-transform: scale(0.97);
		-ms-transform: scale(0.97);
		-o-transform: scale(0.97);
		transform: scale(0.97);
	}

	.page-home .hexagon-item:nth-last-child(1),
	.page-home .hexagon-item:nth-last-child(2),
	.page-home .hexagon-item:nth-last-child(3) {
		-webkit-transform: rotate(30deg) translate(87px, -80px);
		-moz-transform: rotate(30deg) translate(87px, -80px);
		-ms-transform: rotate(30deg) translate(87px, -80px);
		-o-transform: rotate(30deg) translate(87px, -80px);
		transform: rotate(30deg) translate(87px, -80px);
	}

	.hex-item {
		position: absolute;
		top: 0;
		left: 50px;
		width: 100px;
		height: 173.20508px;
	}

	.hex-item:first-child {
		z-index: 0;
		-webkit-transform: scale(0.9);
		-moz-transform: scale(0.9);
		-ms-transform: scale(0.9);
		-o-transform: scale(0.9);
		transform: scale(0.9);
		-webkit-transition: all 0.3s cubic-bezier(0.165, 0.84, 0.44, 1);
		-moz-transition: all 0.3s cubic-bezier(0.165, 0.84, 0.44, 1);
		-o-transition: all 0.3s cubic-bezier(0.165, 0.84, 0.44, 1);
		transition: all 0.3s cubic-bezier(0.165, 0.84, 0.44, 1);
	}

	.hex-item:last-child {
		transition: all 0.3s cubic-bezier(0.19, 1, 0.22, 1);
		z-index: 1;
	}

	.hex-item div {
		box-sizing: border-box;
		position: absolute;
		top: 0;
		width: 100px;
		height: 173.20508px;
		-webkit-transform-origin: center center;
		-moz-transform-origin: center center;
		-ms-transform-origin: center center;
		-o-transform-origin: center center;
		transform-origin: center center;
	}

	.hex-item div::before,
	.hex-item div::after {
		background-color: #007399;
		content: "";
		position: absolute;
		width: 100%;
		height: 3px;
		-webkit-transition: all 0.3s cubic-bezier(0.165, 0.84, 0.44, 1) 0s;
		-moz-transition: all 0.3s cubic-bezier(0.165, 0.84, 0.44, 1) 0s;
		-o-transition: all 0.3s cubic-bezier(0.165, 0.84, 0.44, 1) 0s;
		transition: all 0.3s cubic-bezier(0.165, 0.84, 0.44, 1) 0s;
	}

	.hex-item div:before {
		top: 0;
	}

	.hex-item div:after {
		bottom: 0;
	}

	.hex-item div:nth-child(1) {
		-webkit-transform: rotate(0deg);
		-moz-transform: rotate(0deg);
		-ms-transform: rotate(0deg);
		-o-transform: rotate(0deg);
		transform: rotate(0deg);
	}

	.hex-item div:nth-child(2) {
		-webkit-transform: rotate(60deg);
		-moz-transform: rotate(60deg);
		-ms-transform: rotate(60deg);
		-o-transform: rotate(60deg);
		transform: rotate(60deg);
	}

	.hex-item div:nth-child(3) {
		-webkit-transform: rotate(120deg);
		-moz-transform: rotate(120deg);
		-ms-transform: rotate(120deg);
		-o-transform: rotate(120deg);
		transform: rotate(120deg);
	}

	.hex-content {
	color: #fff;
	display: block;
	height: 180px;
	margin: 0 auto;
	position: relative;
	text-align: center;
	transform: rotate(-30deg);
		width: 156px;
	}

	.hex-content .hex-content-inner {
		left: 50%;
		margin: -3px 0 0 2px;
		position: absolute;
		top: 50%;
		-webkit-transform: translate(-50%, -50%);
		-moz-transform: translate(-50%, -50%);
		-ms-transform: translate(-50%, -50%);
		-o-transform: translate(-50%, -50%);
		transform: translate(-50%, -50%);
	}

	.hex-content .icon {
		display: block;
		font-size: 36px;
		line-height: 30px;
		margin-bottom: 11px;
	}

	.hex-content .title {
		display: block;
		font-family: "Open Sans", sans-serif;
		font-size: 14px;
		letter-spacing: 1px;
		line-height: 24px;
		text-transform: uppercase;
	}

	.hex-content svg {
		left: -7px;
		position: absolute;
		top: -13px;
		transform: scale(0.87);
		z-index: -1;
		-webkit-transition: all 0.3s cubic-bezier(0.165, 0.84, 0.44, 1) 0s;
		-moz-transition: all 0.3s cubic-bezier(0.165, 0.84, 0.44, 1) 0s;
		-o-transition: all 0.3s cubic-bezier(0.165, 0.84, 0.44, 1) 0s;
		transition: all 0.3s cubic-bezier(0.165, 0.84, 0.44, 1) 0s;
	}

	.hex-content:hover {
		color: #fff;
	}

	.page-home .hexagon-item:nth-last-child(1),
	.page-home .hexagon-item:nth-last-child(2),
	.page-home .hexagon-item:nth-last-child(3) {
		-webkit-transform: rotate(30deg) translate(87px, -80px);
		-moz-transform: rotate(30deg) translate(87px, -80px);
		-ms-transform: rotate(30deg) translate(87px, -80px);
		-o-transform: rotate(30deg) translate(87px, -80px);
		transform: rotate(30deg) translate(87px, -80px);
	}

	/* ------------------------------------------------
    Welcome Page
	------------------------------------------------- */
	.author-image-large {
		position: absolute;
		right: 0;
		top: 0;
	}

	.author-image-large img {
		height: -webkit-calc(100vh - 4px);
		height: -moz-calc(100vh - 4px);
		height: calc(100vh - 4px);
	}


	@media (min-width: 1200px) {
		.col-lg-offset-2 {
			margin-left: 16.66666667%;
		}
	}

	@media (min-width: 1200px) {
		.col-lg-8 {
			width: 66.66666667%;
		}
	}

	.hexagon-item:first-child {
		margin-left: 0;
	}

	.pt-table.desktop-768 .pt-tablecell {
		padding-bottom: 110px;
		padding-top: 60px;
	}



	.hexagon-item:hover .icon i {
		color: #ff0037;
		transition: 0.6s;

	}


	.hexagon-item:hover .title {
		-webkit-animation: focus-in-contract 0.5s cubic-bezier(0.250, 0.460, 0.450, 0.940) both;
		animation: focus-in-contract 0.5s cubic-bezier(0.250, 0.460, 0.450, 0.940) both;
	}

	/***************************/

	@-webkit-keyframes focus-in-contract {
		0% {
			letter-spacing: 1em;
			-webkit-filter: blur(12px);
			filter: blur(12px);
			opacity: 0;
		}

		100% {
			-webkit-filter: blur(0px);
			filter: blur(0px);
			opacity: 1;
		}
	}

	@keyframes focus-in-contract {
		0% {
			letter-spacing: 1em;
			-webkit-filter: blur(12px);
			filter: blur(12px);
			opacity: 0;
		}

		100% {
			-webkit-filter: blur(0px);
			filter: blur(0px);
			opacity: 1;
		}
	}


	@media only screen and (max-width: 767px) {
		.hexagon-item {
			float: none;
			margin: 0 auto 50px;
		}

		.hexagon-item:first-child {
			margin-left: auto;
		}

		.page-home .hexagon-item:nth-last-child(1),
		.page-home .hexagon-item:nth-last-child(2),
		.page-home .hexagon-item:nth-last-child(3) {
			-webkit-transform: rotate(30deg) translate(0px, 0px);
			-moz-transform: rotate(30deg) translate(0px, 0px);
			-ms-transform: rotate(30deg) translate(0px, 0px);
			-o-transform: rotate(30deg) translate(0px, 0px);
			transform: rotate(30deg) translate(0px, 0px);
		}

	}
</style>

<div class="pt-table desktop-600">
    <div class="pt-tablecell page-home relative" style="background-image: url(https://cdn2.hubspot.net/hubfs/2528720/Imported_Blog_Media/BoxesBackgroundImage-2.jpg); background-position: center; background-size: cover;">
        <div class="overlay"><br>
            &nbsp;&nbsp;&nbsp;<a href="<?php echo base_url("dashboard") ?>" type="button" class="btn btn-danger btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
		    <span class="ladda-label"><i class="icon md-home mr-15" aria-hidden="true"></i>Home</span></a>
                </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="page-title  home text-center">
                                  <span class="heading-page" style="font-size: 32px;"> Indirect Material Inventory
                                  </span>
                                </div>
								<div class="page-content container-fluid">
									<div class="row" data-plugin="matchHeight" data-by-row="true">

									<?php if(check_permission_view(ID_GROUP,'read','incoming')) { ?>
										<div class="col-xl-4 col-md-3">
											<div class="hexagon-item">
												<div class="hex-item">
													<div></div>
													<div></div>
													<div></div>
												</div>
												<div class="hex-item">
													<div></div>
													<div></div>
													<div></div>
												</div>
												<a href="<?php echo base_url('Incoming_item') ?>" class="hex-content">
													<span class="hex-content-inner">
														<span class="icon">
															<i class="fa fa-bullseye"></i>
														</span>
														<br><br><span class="title">IM Incoming</span>
													</span>
													<svg class="fil" viewBox="0 0 173.20508075688772 200" height="200" width="174" version="1.1" xmlns=""><path d="M86.60254037844386 0L173.20508075688772 50L173.20508075688772 150L86.60254037844386 200L0 150L0 50Z" ></path></svg>
													<svg class="" viewBox="-200 -100 800 800">
													<path d="M447.739,250.776l-58.514-125.91c-2.014-3.521-5.884-5.557-9.927-5.224h-50.155c-5.771,0-10.449,4.678-10.449,10.449
														c0,5.771,4.678,10.449,10.449,10.449h43.363l48.588,104.49h-59.559c-27.004-0.133-51.563,15.625-62.694,40.229
														c-8.062,16.923-25.141,27.698-43.886,27.69h-60.604c-18.745,0.008-35.823-10.767-43.886-27.69
														c-11.131-24.604-35.69-40.362-62.694-40.229H29.257l57.469-104.49h33.437c5.771,0,10.449-4.678,10.449-10.449
														c0-5.771-4.678-10.449-10.449-10.449H80.457c-3.776-0.358-7.425,1.467-9.404,4.702L2.09,250.776
														c-1.209,1.072-1.958,2.569-2.09,4.18v130.09c0.832,29.282,24.524,52.744,53.812,53.29h341.682
														c29.289-0.546,52.98-24.008,53.812-53.29v-130.09C449.108,253.461,448.572,252.032,447.739,250.776z"/>
													<path d="M217.339,252.865c3.706,4.04,9.986,4.31,14.025,0.603c0.21-0.192,0.411-0.394,0.603-0.603l71.053-71.576
														c3.462-4.617,2.527-11.166-2.09-14.629c-3.715-2.786-8.824-2.786-12.539,0l-53.29,53.29V21.42
														c0-5.771-4.678-10.449-10.449-10.449s-10.449,4.678-10.449,10.449v198.531l-53.29-53.29c-4.617-3.462-11.166-2.527-14.629,2.09
														c-2.786,3.715-2.786,8.824,0,12.539L217.339,252.865z"/>
													</svg>
												</a>
                                    		</div>
										</div>
									<?php } ?>

									<?php if(check_permission_view(ID_GROUP,'read','master_data')) { ?>
										<div class="col-xl-4 col-md-6">
											<div class="hexagon-item">
												<div class="hex-item">
													<div></div>
													<div></div>
													<div></div>
												</div>
												<div class="hex-item">
													<div></div>
													<div></div>
													<div></div>
												</div>
												<a href="<?php echo base_url('Master_data') ?>" class="hex-content">
													<span class="hex-content-inner">
														<span class="icon">
															<i class="fa fa-braille"></i>
														</span>
														<br><br><span class="title">IM Master Data</span>
													</span>
													<svg class="fil" viewBox="0 0 173.20508075688772 200" height="200" width="174" version="1.1" xmlns="http://www.w3.org/2000/svg"><path d="M86.60254037844386 0L173.20508075688772 50L173.20508075688772 150L86.60254037844386 200L0 150L0 50Z" ></path></svg>
														<svg class="" viewBox="-41 -17 120 120" >
														<g id="database_server" data-name="database server">
														<path d="m43.82 20.01a2.986 2.986 0 0 0 -2.82-2.01h-1v-2h1a3.009 3.009 0 0 0 3-3v-8a3.009 3.009 0 0 0 -3-3h-36a3.009 3.009 0 0 0 -3 3v8a3.009 3.009 0 0 0 3 3h1v2h-1a3.009 3.009 0 0 0 -3 3v8a3.009 3.009 0 0 0 3 3h1v2h-1a3.009 3.009 0 0 0 -3 3v8a3.009 3.009 0 0 0 3 3h19v8c0 3.94 9.56 6 19 6s19-2.06 19-6v-30c0-3.83-9.01-5.87-18.18-5.99zm-38.82-6.01a1 1 0 0 1 -1-1v-8a1 1 0 0 1 1-1h36a1 1 0 0 1 1 1v8a1 1 0 0 1 -1 1zm33 2v2h-4v-2zm-6 0v2h-18v-2zm-20 0v2h-4v-2zm12 30h-19a1 1 0 0 1 -1-1v-8a1 1 0 0 1 1-1h19zm-16-12v-2h4v2zm16 0h-10v-2h10zm0-8v4h-19a1 1 0 0 1 -1-1v-8a1 1 0 0 1 1-1h36a.7.7 0 0 1 .14.03c-8.8.25-17.14 2.29-17.14 5.97zm36 30c0 1.36-6 4-17 4s-17-2.64-17-4v-7.18c3.29 2.09 10.16 3.18 17 3.18s13.71-1.09 17-3.18zm0-10c0 1.36-6 4-17 4s-17-2.64-17-4v-7.18c3.29 2.09 10.16 3.18 17 3.18s13.71-1.09 17-3.18zm0-10c0 1.36-6 4-17 4s-17-2.64-17-4v-7.18c3.29 2.09 10.16 3.18 17 3.18s13.71-1.09 17-3.18zm-17-6c-11 0-17-2.64-17-4s6-4 17-4 17 2.64 17 4-6 4-17 4z"/>
														<path d="m42 35h2v2h-2z"/>
														<path d="m42 46h2v2h-2z"/>
														<path d="m42 55h2v2h-2z"/>
														<path d="m11 22h-4a1 1 0 0 0 -1 1v4a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1v-4a1 1 0 0 0 -1-1zm-1 4h-2v-2h2z"/>
														<path d="m19 22h-4a1 1 0 0 0 -1 1v4a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1v-4a1 1 0 0 0 -1-1zm-1 4h-2v-2h2z"/>
														<path d="m11 6h-4a1 1 0 0 0 -1 1v4a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1v-4a1 1 0 0 0 -1-1zm-1 4h-2v-2h2z"/>
														<path d="m19 6h-4a1 1 0 0 0 -1 1v4a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1v-4a1 1 0 0 0 -1-1zm-1 4h-2v-2h2z"/>
														<path d="m22 6h14v2h-14z"/><path d="m38 6h2v2h-2z"/><path d="m22 10h14v2h-14z"/><path d="m38 10h2v2h-2z"/>
														<path d="m11 38h-4a1 1 0 0 0 -1 1v4a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1v-4a1 1 0 0 0 -1-1zm-1 4h-2v-2h2z"/>
														<path d="m19 38h-4a1 1 0 0 0 -1 1v4a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1v-4a1 1 0 0 0 -1-1zm-1 4h-2v-2h2z"/></g>
													</svg>
												</a>    
											</div>
										</div>
									<?php } ?>
									
									<?php if(check_permission_view(ID_GROUP,'read','outgoing')) { ?>
										<div class="col-xl-4 col-md-6">
											<div class="hexagon-item">
												<div class="hex-item">
													<div></div>
													<div></div>
													<div></div>
												</div>
												<div class="hex-item">
													<div></div>
													<div></div>
													<div></div>
												</div>
												<a href="<?php echo base_url("itemout") ?>" class="hex-content">
													<span class="hex-content-inner">
														<span class="icon">
															<i class="fa fa-id-badge"></i>
														</span><br><br>
														<span class="title">IM Outgoing</span>
													</span>
													<svg class="fil" viewBox="0 0 173.20508075688772 200" height="200" width="174" version="1.1" xmlns="http://www.w3.org/2000/svg"><path d="M86.60254037844386 0L173.20508075688772 50L173.20508075688772 150L86.60254037844386 200L0 150L0 50Z" ></path></svg>
													<svg class="" viewBox="-185 -100 750 750">
													<path d="M395.154,161.689l-0.607,0.279v-1.77l-0.407,0.155l0.263-0.631l-105.62-43.023c-1.275-0.252-2.526-0.6-3.741-1.087
														l-22.673-8.94v12.544l116.146,47.308l-148.703,58.649v-12.37h-28.115c-12.289,0-22.284-9.992-22.284-22.28v-78.895L50.943,160.423
														l-0.562-0.225v0.106L4.254,228.16l46.126-4.071v151.979l165.933,65.458v0.256h12.287v-0.256l165.938-65.458V271.422l42.988-18.479
														L395.154,161.689z M216.319,428.304L62.676,367.712V178.264l153.643,60.604V428.304z M382.238,367.707l-153.632,60.597V238.868
														l33.762,89.366l119.87-51.527V367.707z M142.365,100.315c-1.041-1.385-1.041-3.278,0.025-4.657l72.384-94.156
														C215.506,0.558,216.621,0,217.826,0c1.185,0,2.31,0.558,3.043,1.502l72.386,94.156c0.536,0.69,0.799,1.514,0.799,2.337
														c0,0.818-0.254,1.625-0.779,2.32c-1.028,1.384-2.867,1.888-4.465,1.25l-47.935-18.923v110.956c0,2.123-1.722,3.844-3.847,3.844
														h-38.41c-2.124,0-3.845-1.715-3.845-3.844V82.643l-47.924,18.923C145.247,102.208,143.408,101.7,142.365,100.315z"/>
													</svg>
												</a>
											</div>
										</div>
									<?php } ?>
									
									</div>
								</div>
								
								<div class="page-content container-fluid">
									<div class="row" data-plugin="matchHeight" data-by-row="true">
										<div class="col-xl-2 col-md-4">
										</div>

									<?php if(check_permission_view(ID_GROUP,'read','approval')) { ?>
										<div class="col-xl-4 col-md-3">
											<div class="hexagon-item">
												<div class="hex-item">
													<div></div>
													<div></div>
													<div></div>
												</div>
												<div class="hex-item">
													<div></div>
													<div></div>
													<div></div>
												</div>
												<a href="<?php echo base_url('request') ?>" class="hex-content">
													<span class="hex-content-inner">
														<span class="icon">
															<i class="fa fa-clipboard"></i>
														</span>
														<br><br><span class="title">IM Approval Status</span>
													</span><svg class="fil" viewBox="0 0 173.20508075688772 200" height="200" width="174" version="1.1" xmlns="http://www.w3.org/2000/svg"><path d="M86.60254037844386 0L173.20508075688772 50L173.20508075688772 150L86.60254037844386 200L0 150L0 50Z"></path></svg>
													<svg class="" viewBox="-12 -8 40 40">
														<path d="M18.303,4.742l-1.454-1.455c-0.171-0.171-0.475-0.171-0.646,0l-3.061,3.064H2.019c-0.251,0-0.457,0.205-0.457,0.456v9.578c0,0.251,0.206,0.456,0.457,0.456h13.683c0.252,0,0.457-0.205,0.457-0.456V7.533l2.144-2.146C18.481,5.208,18.483,4.917,18.303,
														4.742 M15.258,15.929H2.476V7.263h9.754L9.695,9.792c-0.057,0.057-0.101,0.13-0.119,0.212L9.18,11.36h-3.98c-0.251,0-0.457,0.205-0.457,0.456c0,0.253,0.205,0.456,0.457,0.456h4.336c0.023,0,0.899,0.02,1.498-0.127c0.312-0.077,0.55-0.137,0.55-0.137c0.08-0.018,
														0.155-0.059,0.212-0.118l3.463-3.443V15.929z M11.241,11.156l-1.078,0.267l0.267-1.076l6.097-6.091l0.808,0.808L11.241,11.156z"></path>
													</svg>
												</a>
											</div>
										</div>
									<?php } ?>
									
									<?php if(check_permission_view(ID_GROUP,'read','creating')) { ?>
										<div class="col-xl-4 col-md-6">
											<div class="hexagon-item">
												<div class="hex-item">
													<div></div>
													<div></div>
													<div></div>
												</div>
												<div class="hex-item">
													<div></div>
													<div></div>
													<div></div>
												</div>
												<a href="<?php echo base_url('Requestor') ?>" class="hex-content">
													<span class="hex-content-inner">
														<span class="icon">
															<i class="fa fa-map-signs"></i>
														</span><br><br>
														<span class="title">IM Creating</span>
													</span>
													<svg class="fil" viewBox="0 0 173.20508075688772 200" height="200" width="174" version="1.1"><path d="M86.60254037844386 0L173.20508075688772 50L173.20508075688772 150L86.60254037844386 200L0 150L0 50Z" ></path></svg>
													<svg class="" viewBox="-11 -9 40 40">
													<path d="M16.588,3.411h-4.466c0.042-0.116,0.074-0.236,0.074-0.366c0-0.606-0.492-1.098-1.099-1.098H8.901c-0.607,0-1.098,0.492-1.098,1.098c0,0.13,0.033,0.25,0.074,0.366H3.41c-0.606,0-1.098,0.492-1.098,1.098c0,0.607,0.492,
													1.098,1.098,1.098h0.366V16.59c0,0.808,0.655,1.464,1.464,1.464h9.517c0.809,0,1.466-0.656,1.466-1.464V5.607h0.364c0.607,0,1.1-0.491,1.1-1.098C17.688,3.903,17.195,3.411,16.588,3.411z M8.901,2.679h2.196c0.202,0,0.366,0.164,
													0.366,0.366S11.3,3.411,11.098,3.411H8.901c-0.203,0-0.366-0.164-0.366-0.366S8.699,2.679,8.901,2.679z M15.491,16.59c0,0.405-0.329,0.731-0.733,0.731H5.241c-0.404,0-0.732-0.326-0.732-0.731V5.607h10.983V16.59z M16.588,
													4.875H3.41c-0.203,0-0.366-0.164-0.366-0.366S3.208,4.143,3.41,4.143h13.178c0.202,0,0.367,0.164,0.367,0.366S16.79,4.875,16.588,4.875zM6.705,14.027h6.589c0.202,0,0.366-0.164,0.366-0.366s-0.164-0.367-0.366-0.367H6.705c-0.203,
													0-0.366,0.165-0.366,0.367S6.502,14.027,6.705,14.027z M6.705,11.83h6.589c0.202,0,0.366-0.164,0.366-0.365c0-0.203-0.164-0.367-0.366-0.367H6.705c-0.203,0-0.366,0.164-0.366,0.367C6.339,11.666,6.502,11.83,6.705,11.83z M6.705,
													9.634h6.589c0.202,0,0.366-0.164,0.366-0.366c0-0.202-0.164-0.366-0.366-0.366H6.705c-0.203,0-0.366,0.164-0.366,0.366C6.339,9.47,6.502,9.634,6.705,9.634z"></path>
													</svg>
												</a>
											</div>
										</div>
									<?php } ?>
										
										<!-- <div class="col-xl-4 col-md-6">
											<div class="hexagon-item">
												<div class="hex-item">
													<div></div>
													<div></div>
													<div></div>
												</div>
												<div class="hex-item">
													<div></div>
													<div></div>
													<div></div>
												</div>
												<a href="<?php echo base_url('Report') ?>" class="hex-content">
													<span class="hex-content-inner">
														<span class="icon">
															<i class="fa fa-map-signs"></i>
														</span><br><br>
														<span class="title">IM Report</span>
													</span>
													<svg class="fil" viewBox="0 0 173.20508075688772 200" height="200" width="174" version="1.1"><path d="M86.60254037844386 0L173.20508075688772 50L173.20508075688772 150L86.60254037844386 200L0 150L0 50Z" ></path></svg>
													<svg class="" viewBox="-15 -11 100 100"><g>
														<path d="m61.71 13.29-11-11a1.033 1.033 0 0 0 -.71-.29h-25a3.009 3.009 0 0 0 -3 3v6.05a20.973 20.973 0 0 0 0 41.9v6.05a3.009 3.009 0 0 0 3 3h34a3.009 3.009 0 0 0 3-3v-45a1.033 1.033 0 0 0 -.29-.71zm-10.71-7.88 7.59 7.59h-6.59a1 1 0 0 1 -1-1zm-47 26.59a19.023 19.023 0 0 1 18-18.97v18.56l-13.1 13.1a18.879 18.879 0 0 1 -4.9-12.69zm6.31 14.1 13.1-13.1h18.56a18.963 18.963 0 0 1 -31.66 13.1zm49.69 12.9a1 1 0 0 1 -1 1h-34a1 1 0 0 1 -1-1v-6.05a21.014 21.014 0 0 0 20-20.95 1 1 0 0 0 -1-1h-19v-26a1 1 0 0 1 1-1h24v8a3.009 3.009 0 0 0 3 3h8z"/><path d="m27 7a1 1 0 0 0 -1 1v20a1 1 0 0 0 1 1h20a1 1 0 0 0 1-1 21.023 21.023 0 0 0 -21-21zm1 20v-17.97a19.017 19.017 0 0 1 17.97 17.97z"/><path d="m49 40a9 9 0 1 0 9 9 9.014 9.014 0 0 0 -9-9zm0 16a7 7 0 1 1 7-7 7.008 7.008 0 0 1 -7 7z"/><path d="m47 50.586-2.293-2.293-1.414 1.414 3 3a1 1 0 0 0 1.414 0l6-6-1.414-1.414z"/><path d="m50 19h8v2h-8z"/><path d="m50 25h8v2h-8z"/><path d="m50 31h8v2h-8z"/></g>
													</svg>
												</a>
											</div>
										</div> -->
                                    </div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	</div>
</div>			
