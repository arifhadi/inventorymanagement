<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 5px;
        margin-bottom: 0;
        font-size: 30px;
    }

    #button1 {
        margin-top: 30px;
        left: 48%;
    }

    .container {
        top: 50%;
    }

    .action {
        width: 200;
    }
</style>

<div class="page">
    <div class="page-header">
        <div class="col-md-12">
            <!-- button-->
            <a href="<?php echo base_url("Dashboard") ?>" type="button" class="btn btn-danger  btn btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
                <span class="ladda-label"><i class="icon md-home mr-10" aria-hidden="true"></i>Home</span>
            </a>
        </div>
    </div>

    <h3 align="center">AUTORIZATION MENU</h3> 

    <div class="page-content">

	<br><br><br><br><br><br>
	<div style="text-align: center;">
		
			<a href="<?php echo base_url('User/list_user') ?>" type="button" class="btn btn-animate btn-animate-vertical btn-success">
				<span><i class="icon md-filter-list" aria-hidden="true"></i>Create User</span>
			</a> &nbsp;&nbsp;&nbsp;&nbsp;

			<a href="<?php echo base_url('User/list_group') ?>" type="button" class="btn btn-animate btn-animate-vertical btn-success">
				<span><i class="icon md-plus-circle-o-duplicate" aria-hidden="true"></i>User Group</span>
			</a> &nbsp;&nbsp;&nbsp;&nbsp;

			<a href="<?php echo base_url("User/list_menu") ?>" type="button" class="btn btn-animate btn-animate-vertical btn-success">
				<span><i class="icon md-minus-circle-outline" aria-hidden="true"></i>Create Menu</span>
			</a>
		<!-- <a class="float-right">
			<button type="button" class="btn btn-animate btn-animate-side btn-success">
				<span><i class="icon md-accounts" aria-hidden="true"></i>Register</span>
			</button>
		</a> -->
		<br><br><br><br><br>
	</div>
        
    </div>
</div>