<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 5px;
        margin-bottom: 0;
        font-size: 30px;
    }

    #button1 {
        margin-top: 30px;
        left: 48%;
    }

    .container {
        top: 50%;
    }

    .action {
        width: 200;
    }
</style>

<div class="page">
    <div class="page-header">
        <div class="col-md-12">
            <!-- button-->
            <a href="<?php echo base_url("User/list_menu") ?>" type="button" class="btn btn-info btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home" >
                <span class="ladda-label"><i class="icon md-undo mr-10" aria-hidden="true"></i>Back</span>
            </a>
        </div>
    </div>

    <h2 align="center" class="title">Create Menu</h2> 

    <div class="page-content">
      <div class="panel">
        <div class="panel-body container-fluid" style="padding: 0px;">
                    <div class="panel">
              <div class="panel-body container-fluid">
                <div class="row row-lg">
                  <div class="col-md-12 col-lg-6">
                    <!-- Example Horizontal Form -->
                    <div class="example-wrap">
                      <div class="example">
                        <!-- <form class="form-horizontal"> -->
                          <!-- <form action="http://machinemanagement.asiagalaxy.com/backend/user/save_profil_user" id="login_validation" enctype="multipart/form-data" method="post" accept-charset="utf-8"> -->

                          <div class="form-group row">
                            <label class="col-md-3 form-control-label"><b>Employee No </b></label>
                            <div class="col-md-9">
                              <input type="text" required="required"  class="form-control" value=""/>
                            </div>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-3 form-control-label"><b>Name  </b></label>
                            <div class="col-md-9">
                              <input type="text" required="required" class="form-control"  autocomplete="off" />
                            </div>
                          </div>
                          
                          <div class="form-group row">
                            <label class="col-md-3 form-control-label"><b>Email  </b></label>
                            <div class="col-md-9">
                              <input type="email" required="required" class="form-control"  autocomplete="off"/>
                            </div>
                          </div>

                          <div class="form-group row form-material row">
                            <label class="col-md-3 form-control-label">User Autorization  </label>
                            <div class="col-md-9">
                                <select class="form-control" data-plugin="select2"  data-placeholder="Select Departement" >
                                    <?php foreach ($groups as $value) { ?>
                                        <option value="<?php echo $value->id ?>"><?php echo $value->group ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-3 form-control-label"><b>Birthday </b></label>
                            <div class="col-md-9">
                              <input type="date" class="form-control"  value="" autocomplete="off" />
                            </div>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-3 form-control-label"><b>Gender  </b></label>
                            <div class="col-md-9">
                            <select class="form-control" data-plugin="select2" id="gender"  data-placeholder="Select Gender" >
                                  <option  value="M">Male</option>
                                <option  value="F">Female</option>
                            </select>
                            </div>
                          </div>

                          <div class="form-group row form row">
                            <label class="col-md-3 form-control-label"><b>Address  </b></label>
                            <div class="col-md-9">
                              <textarea class="maxlength-textarea form-control" data-plugin="maxlength" data-placement="bottom-right-inside" maxlength="100"  rows="2" placeholder="Address"></textarea>
                            </div>
                          </div>
                      </div>
                    </div>
                    <!-- End Example Horizontal Form -->
                  </div>
                  <div class="col-md-12 col-lg-6">
                    <!-- Example Horizontal Form -->
                    <div class="example-wrap">
                      <div class="example">
                        <!-- <form class="form-horizontal"> -->
                          <div class="form-group row">
                            <label class="col-md-3 form-control-label"><b>Phone No.  </b></label>
                            <div class="col-md-9">
                              <input type="text" class="form-control"  value="0" placeholder="+62 xxx - xxxx - xxxx" onkeypress="return hanyaAngka(event)" autocomplete="off"/>
                            </div>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-3 form-control-label"><b>Password  </b></label>
                            <div class="col-md-9">
                              <input type="password" class="form-control" name="password" autocomplete="off" id="password"/>
                            </div>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-3 form-control-label"><b>Confirm Password  </b></label>
                            <div class="col-md-9">
                              <input type="password" class="form-control" name="confirm_password" autocomplete="off" id="confirm_password"/>
                            </div>
                          </div>

                          <div class="form-group row  row">
                            <label class="col-md-3 form-control-label"><b>Photo Profile  </b></label>
                            <div class="col-md-9">
                              <div class="input-group input-group-file" data-plugin="inputGroupFile">
                                <input type="text" class="form-control" readonly="">
                                <span class="input-group-append">
                                  <span class="btn btn-success btn-file">
                                    <i class="icon md-upload" aria-hidden="true"></i>
                                    <input type="file" name="file_picture" multiple="">
                                  </span>
                                </span>
                              </div>
                            </div>
                          </div>

                          <!-- <style>
                            .profil {
                              border 5px solid #0099ff;
                              padding 5px;
                              border-radius 100px;
                            }
                          </style> -->
                          <div class="form-group row">
                            <label class="col-md-3 form-control-label"><b>Picture</b></label>
                            <div class="col-md-9">
                                <img src="" alt="Paris" width="200" height="200" class="profil">
                            </div>
                          </div>

                      </div>
                    </div>
                    <!-- End Example Horizontal Form -->
                  </div>
                  <!-- Button Action -->
                    <div class="col-lg-5 form-group form-material">
                        <!-- <input type="text" class="form-control" placeholder=".col-lg-4"> -->
                    </div>
                    <div class="col-lg-5 form-group form-material">
                      
                      <!-- <button type="reset" class="btn btn-success btn-sm">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;REFRESH&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp; -->
                      <button type="Submit" class="btn btn-success btn-sm">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SAVE &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>

                    </div>
                    <div class="col-lg-2 form-group form-material">
                      <!-- <input type="text" class="form-control" placeholder=".col-lg-4"> -->
                    </div>
                                      <!-- Button Action -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>