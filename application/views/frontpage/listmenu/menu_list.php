<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 5px;
        margin-bottom: 0;
        font-size: 30px;
    }

    #button1 {
        margin-top: 30px;
        left: 48%;
    }

    .container {
        top: 50%;
    }

    .action {
        width: 200;
    }
</style>

<div class="page">
    <div class="page-header">
        <div class="col-md-12">
            <!-- button-->
            <a href="<?php echo base_url("User") ?>" type="button" class="btn btn-info  btn btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
                <span class="ladda-label"><i class="icon md-home mr-10" aria-hidden="true"></i>Home</span>
            </a>

            <a href="<?php echo base_url("User/create_menu") ?>" type="button" class="btn btn-danger btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home" >
                <span class="ladda-label"><i class="icon md-plus mr-10" aria-hidden="true"></i>Creat User</span>
            </a>
        </div>
    </div>

    <h2 align="center">Menu Master List</h2> 

    <div class="page-content">

        <!-- button-->
        <div class="col-md-12">
            <div class="panel">
                <!-- allert -->
                <?php if ($this->session->flashdata('info')) { ?>
                    <div class="alert dark alert-success alert-dismissible" role="alert" id="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?= $this->session->flashdata('info'); ?>
                    </div>
                <?php } ?>
                <div class="panel-body">

                    <div class="panel">
                        <div class="panel-body">
                            <table class="table table-hover dataTable table-striped w-full no-footer dtr-inline" cellspacing="2" id="exampleTableSearch">
                                <thead>
                                    <tr align="center" class>
                                        <th>No</th>
                                        <th>ID Group</th>
                                        <th>Employee No</th>
                                        <th>Name</th>
                                        <th>Birthday</th>
                                        <th>Gender</th>
                                        <th>Address</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">//delete
    $(".nonactive").click(function(){
        var id = $(this).attr("data-bind");
      console.log(id);
       swal({
        title: "Are you sure want to delete?",
        // text: "you will change this data to Non-Active !",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Delete !",
        cancelButtonText: "Cancel !",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
          $.ajax({
             url: '<?= base_url("master_data/hapus/")?>'+id,
             type: 'DELETE',
             error: function() {
                alert('Something is wrong');
             },
             success: function(data) {
                  $("#"+id).remove();
                  swal("Non-Active !", "Your Data is Turned Non-Active.", "success");
                  window.location.reload();
             }
          });
        } else {
          swal("Cancelled", "You Canceled To Delete :)", "error");
        }
      });
    });
</script>

<script type="text/javascript">//edit
    $(".edit").click(function(){
        var id = $(this).attr("data-bind");
    
       swal({
        title: "Are you sure you want to change the data?",
        text: "",
        type: "info",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Yes",
        cancelButtonText: "Cancel",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
          $.ajax({
             // url: '<?= base_url("master_data/edit/")?>'+id,
             type: 'DELETE',
             error: function() {
                alert('Something is wrong');
             },
             success: function(data) {
                  $("#"+id).remove();
                  // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                  window.location.href = '<?= base_url("master_data/edit/")?>'+id;
             }
          });
        } else {
          swal("Cancelled", "You Canceled To Edit :)");
        }
      });
    });
</script>

<!-- SweetAlert Detail -->
<script type="text/javascript">
    //Change Data
    $(".detail").click(function() {
        var id = $(this).attr("data-bind");

        swal({
                title: "Are you sure you want to see detail?",
                text: "",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: "btn-info",
                confirmButtonText: "Yes !",
                cancelButtonText: "Cancel !",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        // url: '<?= base_url("master_data/detail/") ?>'+id,
                        type: 'DELETE',
                        error: function() {
                            alert('Something is wrong');
                        },
                        success: function(data) {
                            $("#" + id).remove();
                            // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            window.location.href = '<?= base_url("master_data/detail/") ?>' + id;
                        }
                    });
                } else {
                    swal("Cancelled", "You Canceled To See Detail :)", "error");
                }
            });
    });
</script>