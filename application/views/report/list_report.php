<div class="page">
    <div class="page-header" style="padding: 0px;">
        <div class="col-md-12"><br>
            <!-- <a href="<?php echo base_url("Dashboard/indirect") ?>" type="button" class="btn btn-danger  btn btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
                <span class="ladda-label"><i class="icon md-home mr-10" aria-hidden="true"></i>Home</span>
            </a>

            <a href="<?php echo base_url("Master_data/add_item") ?>" type="button" class="btn btn-info btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home" >
                <span class="ladda-label"><i class="icon md-plus-circle-o mr-10" aria-hidden="true"></i>Add New Item</span>
            </a>

            <a href="<?php echo base_url("Master_data/add_category") ?>" type="button" class="btn btn-success btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
                <span class="ladda-label"><i class="icon md-widgets mr-10" aria-hidden="true"></i>Add Category</span>
            </a> -->
        </div>
    </div>

    <h3 align="center">List Report Data</h3> 

    <div class="page-content" style="padding: 0px;">
        <div class="col-md-12">
            <div class="panel">
                <!-- allert -->
                <div class="panel-body">
                  <a href="http://machinemanagement.asiagalaxy.com/backend/troubleticket/download_report" type="button" class="btn btn-warning waves-effect waves-light waves-round"><i class="icon md-print" aria-hidden="true"></i>Export Excel</a>
                    <div class="panel">
                        <div class="panel-body">
                            <table class="table table-hover dataTable table-striped w-full no-footer dtr-inline" cellspacing="2" id="exampleTableSearch">
                                <thead>
                                    <tr align="center" class>
                                        <th>No</th>
                                        <th>Description</th>
                                        <th>Picture</th>
                                        <th>Customer</th>
                                        <th>Supplier</th>
                                        <th>Opening Balance</th>
                                        <th>Balance Stock per</th>
                                        <th>Unit</th>
                                        <th>04-30-2021</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   <tr>
                                      <td>1</td>
                                      <td>PLASTIC FOR SHUTTER SUPPORT</td>
                                      <td>picture</td>
                                      <td>WIK</td>
                                      <td>PT. METRICPACK MANDIRI</td>
                                      <td>19</td>
                                      <td>21</td>
                                      <td>Roll</td>
                                      <td>2</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>