<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 5px;
        margin-bottom: 0;
        font-size: 30px;
    }

    #button1 {
        margin-top: 30px;
        left: 48%;
    }

    .container {
        top: 50%;
    }

    .action {
        width: 200;
    }
</style>

<div class="page">
    <div class="page-header" style="padding: 0px;">
        <div class="col-md-12"><br>
            <a href="<?php echo base_url("Dashboard/indirect") ?>" type="button" class="btn btn-danger  btn btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
                <span class="ladda-label"><i class="icon md-home mr-10" aria-hidden="true"></i>Home</span>
            </a>

            <a href="<?php echo base_url("Master_data/add_item") ?>" type="button" class="btn btn-info btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home" >
                <span class="ladda-label"><i class="icon md-plus-circle-o mr-10" aria-hidden="true"></i>Add New Item</span>
            </a>

            <a href="<?php echo base_url("Master_data/add_category") ?>" type="button" class="btn btn-success btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
                <span class="ladda-label"><i class="icon md-widgets mr-10" aria-hidden="true"></i>Add Category</span>
            </a>

            <a href="<?php echo base_url("Master_data/add_customer") ?>" type="button" class="btn btn-warning btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
                <span class="ladda-label"><i class="icon md-account-box-mail mr-10" aria-hidden="true"></i>Add Customers</span>
            </a>

            <a href="<?php echo base_url("Master_data/add_supplier") ?>" type="button" class="btn btn-secondary btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
                <span class="ladda-label"><i class="icon md-truck mr-10" aria-hidden="true"></i>Add Supplier</span>
            </a>
        </div>
    </div>

    <h3 align="center">Tabel Master Data</h3> 

    <div class="page-content">
        <div class="col-md-12">
            <div class="panel">
                <!-- allert -->
                <?php if ($this->session->flashdata('info')) { ?>
                    <div class="alert dark alert-success alert-dismissible" role="alert" id="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?= $this->session->flashdata('info'); ?>
                    </div>
                <?php } ?>
                <div class="px-4 bg-light "><marquee class="py-3"><b>Lates Updated By : <?=$name_user?>, <?=$date_last_update?></b></marquee></div>
                <div class="panel-body">
                    <div class="panel">
                        <div class="panel-body">
                            <table class="table table-hover dataTable table-striped w-full no-footer dtr-inline" cellspacing="2" id="exampleTableSearch">
                                <thead>
                                    <tr align="center" class>
                                        <th>No</th>
                                        <th>ID Item</th>
                                        <th>Category</th>
                                        <th>Name</th>
                                        <th>Image</th>
                                        <th align="center">Opening</th>
                                        <th>Unit</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $no = 1; foreach ($master_data as $value) { ?>
                                   <tr>
                                       <td><?= $no++ ?></td>
                                       <td><?= $value->id_item ?></td>
                                       <td><?= $value->nama_category ?></td>
                                       <td><?= $value->name ?></td>
                                       <td>
                                           <?php if (!empty($value->image)) { ?>
                                               <span class="badge badge-info" style="font-size: 13px;">&nbsp;&nbsp;Available&nbsp;&nbsp;</span>
                                           <?php }else{ ?>
                                               <span class="badge badge-danger" style="font-size: 13px;">Unavailable</span>
                                           <?php }?>
                                       </td>
                                       <td align="center"><?= $value->opening ?></td>
                                       <td><?= $value->unit ?></td>
                                       <td align="center">
                                            <?php if ($value->status == 1) { ?>
                                               <button type='submit' data-bind='<?=$value->id_item?>' class='btn btn-success activenon btn-xs waves-effect waves-light waves-round' style="font-size: 12px;">&emsp;Active&emsp;</button>
                                            <?php } elseif ($value->status == 2) { ?>
                                               <button type='submit' data-bind='<?=$value->id_item?>' class='btn btn-danger active btn-xs waves-effect waves-light waves-round' style="font-size: 12px;">Non-Active</button>
                                            <?php }?>
                                       </td>
                                       <td class="actions" width='150' align="center">
                                           <button type='submit' data-bind='<?=$value->id_item?>' data-toggle="tooltip" class='btn btn-success detail btn btn-floating btn-info btn-xs' title="Detail"><i class="icon md-assignment-check" aria-hidden="true"></i></button>

                                           <button type='submit' data-bind='<?=$value->id_item?>' class='btn btn-success edit btn btn-floating btn-success btn-xs' data-toggle="tooltip" title="Edit"><i class="icon md-icon md-edit" aria-hidden="true"></i></button>

                                           <button type='button' data-bind='<?=$value->id_item?>' class='btn btn-success nonactive btn btn-floating btn-danger btn-xs' data-toggle="tooltip" title="Delete"><i class="icon md-icon md-delete" aria-hidden="true"></i></button>
                                       </td>
                                   <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">//delete
    $(".nonactive").click(function(){
        var id = $(this).attr("data-bind");
      console.log(id);
       swal({
        title: "Are you sure want to delete?",
        // text: "you will change this data to Non-Active !",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Delete !",
        cancelButtonText: "Cancel !",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
          $.ajax({
             url: '<?= base_url("master_data/hapus/")?>'+id,
             type: 'DELETE',
             error: function() {
                alert('Something is wrong');
             },
             success: function(data) {
                  $("#"+id).remove();
                  swal("Non-Active !", "Your Data is Turned Non-Active.", "success");
                  window.location.reload();
             }
          });
        } else {
          swal("Cancelled", "You Canceled To Delete :)", "error");
        }
      });
    });
</script>

<script type="text/javascript">//edit
    $(".edit").click(function(){
        var id = $(this).attr("data-bind");
    
       swal({
        title: "Are you sure you want to change the data?",
        text: "",
        type: "info",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Yes",
        cancelButtonText: "Cancel",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
          $.ajax({
             // url: '<?= base_url("master_data/edit/")?>'+id,
             type: 'DELETE',
             error: function() {
                alert('Something is wrong');
             },
             success: function(data) {
                  $("#"+id).remove();
                  // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                  window.location.href = '<?= base_url("master_data/edit/")?>'+id;
             }
          });
        } else {
          swal("Cancelled", "You Canceled To Edit :)");
        }
      });
    });
</script>

<script type="text/javascript">//SweetAlert Detail
    //Change Data
    $(".detail").click(function() {
        var id = $(this).attr("data-bind");

        swal({
                title: "Are you sure you want to see detail?",
                text: "",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: "btn-info",
                confirmButtonText: "Yes !",
                cancelButtonText: "Cancel !",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        // url: '<?= base_url("master_data/detail/") ?>'+id,
                        type: 'DELETE',
                        error: function() {
                            alert('Something is wrong');
                        },
                        success: function(data) {
                            $("#" + id).remove();
                            // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            window.location.href = '<?= base_url("master_data/detail/") ?>' + id;
                        }
                    });
                } else {
                    swal("Cancelled", "You Canceled To See Detail :)", "error");
                }
            });
    });
</script>

<script type="text/javascript">//Change Non-Active

    $(".activenon").click(function() {
        var id = $(this).attr("data-bind");

        swal({
                title: "Are you sure?",
                text: "You Will Change This Data to be Non-Active !",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Non-active !",
                cancelButtonText: "Cancel !",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        // url: '<?= base_url("master_data/detail/") ?>'+id,
                        type: 'DELETE',
                        error: function() {
                            alert('Something is wrong');
                        },
                        success: function(data) {
                            $("#" + id).remove();
                            // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            window.location.href = '<?= base_url("master_data/update_non_active/") ?>' + id;
                        }
                    });
                } else {
                    swal("Cancelled", "You Canceled To See Detail :)", "error");
                }
            });
    });
</script>

<script type="text/javascript">//Change Active
    $(".active").click(function() {
        var id = $(this).attr("data-bind");

        swal({
                title: "Are you sure?",
                text: "You Will Change This Data to be Non-Active !",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Active !",
                cancelButtonText: "Cancel !",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        // url: '<?= base_url("master_data/detail/") ?>'+id,
                        type: 'DELETE',
                        error: function() {
                            alert('Something is wrong');
                        },
                        success: function(data) {
                            $("#" + id).remove();
                            // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            window.location.href = '<?= base_url("master_data/update_active/") ?>' + id;
                        }
                    });
                } else {
                    swal("Cancelled", "You Canceled To See Detail :)", "error");
                }
            });
    });
</script>