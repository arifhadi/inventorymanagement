<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 5px;
        margin-bottom: 0;
        font-size: 23px;
    }

    #button1 {
        margin-top: 30px;
        left: 45%;
    }


    .control-label:after {
        content: "*";
        color: red;
    }
</style>

<div class="page">
    <div class="page-header" style="padding: 0px;">
        <div class="col-md-12"><br>
            <!-- button-->
            <a href="<?php echo base_url("Dashboard/indirect") ?>" type="button" class="btn btn-danger btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
                <span class="ladda-label"><i class="icon md-home mr-10" aria-hidden="true"></i>Home</span>
            </a>
            <a href="<?php echo base_url("Master_data/add_customer") ?>" type="button" class="btn btn-info btn-round" data-style="slide-left" data-plugin="ladda" data-type="progress" id="back">
                <span class="ladda-label"><i class="icon md-undo" aria-hidden="true"></i>Back</span>
            </a>
        </div>
    </div>

    <h3 align="center">Display Customer</h3>

    <div class="page-content container-fluid" style="padding: 0px;">
        <div class="row">

            <div class="col-lg-3">
                <!-- <div class="panel">
                    <div class="panel-body">
                    </div>
                </div> -->
            </div>

            <div class="col-lg-6">
                <div class="panel">
                    <div class="panel-body">
                        <!-- allert -->
                        <h4 align="center"><b>Detail Form</b></h4>
                           
                        <h4 align="center">ID Item : <b style="color: red;"><?=$detail_customer->id_customer?></b></h4>
                            <div class="form-group form-material row">
                                <label class="col-md-3 col-form-label control-label"><b>Name</b></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" value="<?=$detail_customer->name_cust?>" placeholder="Name" autocomplete="off" disabled="disabled" />
                                </div>
                            </div>

                            <div class="form-group form-material row">
                                <label class="col-md-3 col-form-label control-label"><b>Short Name</b></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" value="<?=$detail_customer->short_name?>" placeholder="XXX" autocomplete="off"  disabled="disabled"/>
                                </div>
                            </div>

                            <div class="form-group form-material row">
                                <label class="col-md-3 col-form-label"><b>Email</b></label>
                                <div class="col-md-6">
                                    <input type="email" class="form-control" value="<?=$detail_customer->email?>" placeholder="@.com" autocomplete="off" disabled="disabled"/>
                                </div>
                            </div>

                            <div class="form-group form-material row">
                                <label class="col-md-3 col-form-label"><b>Phone Number</b></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" value="<?=$detail_customer->phone?>" placeholder="+62 XX-XX" disabled="disabled"/>
                                </div>
                            </div>
                    </div>
                </div>
                <!-- End Panel Form -->
            </div>
        </div>
    </div>
</div>
</div>
<!-- End Panel Extended -->

<script type="text/javascript">//SweetAlert Detail
    $(".detail").click(function() {
        var id = $(this).attr("data-bind");

        swal({
            title: "Are you sure you want to see detail?",
            text: "",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-info",
            confirmButtonText: "Yes !",
            cancelButtonText: "Cancel !",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    // url: '<?= base_url("master_data/detail/") ?>'+id,
                    type: 'DELETE',
                    error: function() {
                        alert('Something is wrong');
                    },
                    success: function(data) {
                        $("#" + id).remove();
                        // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        window.location.href = '<?= base_url("master_data/detail_customer/") ?>' + id;
                    }
                });
            } else {
                swal("Cancelled", "You Canceled To See Detail :)", "error");
            }
        });
    });
</script>

<script type="text/javascript">//edit
    $(".edit").click(function(){
        var id = $(this).attr("data-bind");
    
        swal({
            title: "Are you sure you want to change the data?",
            text: "",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    // url: '<?= base_url("master_data/edit/")?>'+id,
                    type: 'DELETE',
                    error: function() {
                        alert('Something is wrong');
                    },
                    success: function(data) {
                        $("#"+id).remove();
                        // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        window.location.href = '<?= base_url("master_data/edit/")?>'+id;
                    }
                });
            } else {
              swal("Cancelled", "You Canceled To Edit :)");
            }
        });
    });
</script>

<script type="text/javascript">//delete
    $(".nonactive").click(function(){
        var id = $(this).attr("data-bind");
        console.log(id);
        swal({
            title: "Are you sure want to delete?",detail_customer
            // text: "you will change this data to Non-Active !",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Delete !",
            cancelButtonText: "Cancel !",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '<?= base_url("master_data/hapus/")?>'+id,
                    type: 'DELETE',
                    error: function() {
                        alert('Something is wrong');
                    },
                    success: function(data) {
                        $("#"+id).remove();
                        swal("Non-Active !", "Your Data is Turned Non-Active.", "success");
                        window.location.reload();
                    }
                });
            } else {
              swal("Cancelled", "You Canceled To Delete :)", "error");
            }
        });
    });
</script>