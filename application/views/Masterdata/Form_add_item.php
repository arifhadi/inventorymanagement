<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 5px;
        margin-bottom: 0;
        font-size: 30px;
    }

    #button1 {
        margin-top: 30px;
        left: 48%;
    }

    .container {
        top: 50%;
    }

    .action {
        width: 200;
    }

    .control-label:after {
        content: "*";
        color: red;
    }
</style>

<div class="page">
    <div class="page-header" style="padding: 0px;">
        <div class="col-md-12"><br>
            <!-- button-->
            <a href="<?php echo base_url("Dashboard/indirect") ?>" type="button" class="btn btn-danger btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
                <span class="ladda-label"><i class="icon md-home mr-10" aria-hidden="true"></i>Home</span>
            </a>
            <a href="<?php echo base_url("Master_data") ?>" type="button" class="btn btn-info btn-round" data-style="slide-left" data-plugin="ladda" data-type="progress" id="back">
                <span class="ladda-label"><i class="icon md-undo" aria-hidden="true"></i>Back</span>
            </a>
        </div>
        <h3 align="center">Add Item Master Data</h3>
    </div>

    <div class="page-content">
        <!-- button-->
        <div class="col-md-12">
            <div class="panel">
                <!-- allert -->
                <?php if ($this->session->flashdata('info')) { ?>
                    <div class="alert dark alert-success alert-dismissible" role="alert" id="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?= $this->session->flashdata('info'); ?>
                    </div>
                <?php }elseif($this->session->flashdata('danger')) { ?>
                    <div class="alert dark alert-danger alert-dismissible" role="alert" id="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?= $this->session->flashdata('danger'); ?>
                    </div>
                <?php }?>
                <div class="panel-body" style="padding : 0px;">
                    <div class="panel">
                        <div class="panel-body">
                            <?php echo form_open_multipart('master_data/save_add_item'); ?>
                            <div class="panel-body container-fluid" align="left" style="padding : 0px;">
                                <div class="row row-lg-12">
                                    <div class="col-md-12 col-lg-6">
                                        <div class="example-wrap">
                                            <div class="example">

                                                <div class="form-group form-material row">
                                                    <label class="col-md-3 col-form-label control-label"><b>Name</b></label>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" name="name" placeholder="Product Name" autocomplete="off" required />
                                                    </div>
                                                </div>

                                                <!-- <div class="form-group form-material row">
                                                    <label class="col-md-3 col-form-label control-label">Short Name </label>
                                                    <div class="col-md-6">
                                                    <input type="text" class="form-control" name="short_name" placeholder="( A - Z )" autocomplete="off" required id="input" onkeypress="return event.charCode < 48 || event.charCode  >57" maxlength="3"/>
                                                    </div>
                                                </div> -->

                                                <div class="form-group form-material row">
                                                    <label class="col-md-3 col-form-label control-label"><b>Category</b></label>
                                                    <div class="col-md-6">
                                                        <select name="id_category" class="form-control" data-plugin="select2" required>
                                                            <option value="">select</option>
                                                            <?php foreach ($category as $value) { ?>
                                                                <option value="<?php echo $value->id_category ?>"><?php echo $value->nama_category ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group form-material row">
                                                    <label class="col-md-3 col-form-label control-label"><b>Inventory Category</b></label>
                                                    <div class="col-md-6">
                                                        <select name="inventory_category" class="form-control" data-plugin="select2" required>
                                                             <option></option>
                                                            <option value="1">Packaging</option>
                                                            <option value="2">Stationary</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group form-material row">
                                                    <label class="col-md-3 col-form-label control-label"><b>Customer</b></label>
                                                    <div class="col-md-6">
                                                        <select name="id_customer" class="form-control" data-plugin="select2" required>
                                                            <option value="">select</option>
                                                            <?php foreach ($customers as $value) { ?>
                                                                <option value="<?php echo $value->id_customer ?>"><?php echo $value->name_cust?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group form-material row">
                                                    <label class="col-md-3 col-form-label control-label"><b>Supplier</b></label>
                                                    <div class="col-md-6">
                                                        <select name="id_supplier" class="form-control" data-plugin="select2" required>
                                                            <option value="">select</option>
                                                            <?php foreach ($supplier as $value) { ?>
                                                                <option value="<?php echo $value->id_supplier ?>"><?php echo $value->name_sup?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label"><b>Unit</b></label>
                                                    <div class="col-md-6">
                                                        <select id="test" class="form-control" required="required" data-plugin="select" id="priority" name="unit" data-placeholder="Select Priority" >
                                                            <option value="">select</option>
                                                            <option value="Pack">Pack</option>
                                                            <option value="Pcs">Pcs</option>
                                                            <option value="Kg">Kg</option>
                                                            <option value="Roll">Roll</option>
                                                            <option value="Kaleng">Kaleng</option>
                                                            <option class="editable" value="other">Other</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <textarea class="editOption form-control" data-plugin="maxlength" maxlength="100" rows="1" placeholder="location" style="display:none;"></textarea>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-lg-6">
                                        <div class="example-wrap">
                                            <div class="example">

                                                <div class="form-group form-material row">
                                                    <label class="col-md-3 col-form-label "><b>Opening</b></label>
                                                    <div class="col-md-6">
                                                        <input type="number" min="0" class="form-control" name="opening" placeholder="Opening" autocomplete="off" />
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label "><b>Remarks</b></label>
                                                    <div class="col-md-6">
                                                        <div class="example-wrap">
                                                        <textarea class="form-control" name="remarks" id="textareaDefault" rows="3"></textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label control-label-"><b>Upload</b></label>
                                                    <div class="col-md-6">
                                                        <input type="file" name="image" multiple="" id="input-file-now-custom-1" data-plugin="dropify" data-default-file="" />
                                                    </div>
                                                </div>

                                                <div class="form-group form-material row">
                                                    <div class="col-md-12" style="text-align: left;">
                                                        <button type="submit" class="btn btn-round btn-success">
                                                            <span class=" text">&nbsp;&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                        </button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- button -->
                                    <!-- <div class="card-body" id="body" align="center">
                                        <button type="submit" class="btn btn btn-round btn-success">
                                            <span class=" text">&nbsp;&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        </button>
                                    </div> -->

                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
      var initialText = $('.editable').val();
        $('.editOption').val(initialText);

        $('#test').change(function(){
        var selected = $('option:selected', this).attr('class');
        var optionText = $('.editable').text();

        if(selected == "editable"){
          $('.editOption').show();

          
          $('.editOption').keyup(function(){
              var editText = $('.editOption').val();
              $('.editable').val(editText);
              $('.editable').html(editText);
          });

        }else{
          $('.editOption').hide();
        }
      });

    $(function() {
        $('#input').keyup(function() {
                this.value = this.value.toLocaleUpperCase();
        });
    });
</script>