<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 5px;
        margin-bottom: 0;
        font-size: 23px;
    }

    #button1 {
        margin-top: 30px;
        left: 48%;
    }


    .control-label:after {
        content: "*";
        color: red;
    }
</style>

<div class="page">
    <div class="page-header" style="padding: 0px;">
        <div class="col-md-12"><br>
            <!-- button-->
            <a href="<?php echo base_url("Dashboard/indirect") ?>" type="button" class="btn btn-danger btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
                <span class="ladda-label"><i class="icon md-home mr-10" aria-hidden="true"></i>Home</span>
            </a>
            <a href="<?php echo base_url("Master_data/add_supplier") ?>" type="button" class="btn btn-info btn-round" data-style="slide-left" data-plugin="ladda" data-type="progress" id="back">
                <span class="ladda-label"><i class="icon md-undo" aria-hidden="true"></i>Back</span>
            </a>
        </div>
    </div>

    <h3 align="center">Display Supplier</h3>

    <div class="page-content container-fluid" style="padding: 0px;">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="panel">
                    <div class="panel-body">
                        <!-- allert -->
                        <!-- <h4 align="center"><b>Form Add</b></h4> -->
                        <h4 align="center">ID Item : <b style="color: red;"><?=$detail_supplier->id_supplier?></b></h4>
                            <div class="form-group form-material row">
                                <label class="col-md-3 col-form-label control-label"><b>Name</b></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" value="<?=$detail_supplier->name_sup?>" placeholder="Name" autocomplete="off" disabled />
                                </div>
                            </div>

                            <div class="form-group form-material row">
                                <label class="col-md-3 col-form-label control-label"><b>Short Name</b></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" value="<?=$detail_supplier->short_name?>" placeholder="xxx" autocomplete="off" disabled/>
                                </div>
                            </div>

                            <div class="form-group form-material row">
                                <label class="col-md-3 col-form-label"><b>Email</b></label>
                                <div class="col-md-6">
                                    <input type="email" class="form-control" value="<?=$detail_supplier->email?>" placeholder="@.com" autocomplete="off" disabled/>
                                </div>
                            </div>

                            <div class="form-group form-material row">
                                <label class="col-md-3 col-form-label"><b>Phone Number</b></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="phone" placeholder="+62 XX-XX" disabled/>
                                </div>
                            </div>
                    </div>
                </div>
                <!-- End Panel Form -->
            </div>

        </div>
    </div>
</div>
</div>
<!-- End Panel Extended -->