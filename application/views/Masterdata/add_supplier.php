<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 5px;
        margin-bottom: 0;
        font-size: 23px;
    }

    #button1 {
        margin-top: 30px;
        left: 48%;
    }


    .control-label:after {
        content: "*";
        color: red;
    }
</style>

<div class="page">
    <div class="page-header" style="padding: 0px;">
        <div class="col-md-12"><br>
            <!-- button-->
            <a href="<?php echo base_url("Dashboard/indirect") ?>" type="button" class="btn btn-danger btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
                <span class="ladda-label"><i class="icon md-home mr-10" aria-hidden="true"></i>Home</span>
            </a>
            <a href="<?php echo base_url("Master_data") ?>" type="button" class="btn btn-info btn-round" data-style="slide-left" data-plugin="ladda" data-type="progress" id="back">
                <span class="ladda-label"><i class="icon md-undo" aria-hidden="true"></i>Back</span>
            </a>
        </div>
    </div>

    <h3 align="center">Add Supplier</h3>

    <div class="page-content container-fluid" style="padding: 0px;">
        <div class="row">
            <div class="col-lg-5">
                <div class="panel">
                    <div class="panel-body">
                        <!-- allert -->
                        <?php if ($this->session->flashdata('success')) { ?>
                            <div class="alert dark alert-success alert-dismissible" role="alert" id="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button><?= $this->session->flashdata('success'); ?>
                            </div>
                        <?php }elseif($this->session->flashdata('error')){?>
                            <div class="alert dark alert-danger alert-dismissible" role="alert" id="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button><?= $this->session->flashdata('error'); ?>
                            </div>
                        <?php } ?>
                        <?php echo form_open_multipart('Master_data/add_supplier'); ?>
                        <h4 align="center"><b>Form Add</b></h4>
                            <div class="form-group form-material row">
                                <label class="col-md-3 col-form-label control-label"><b>Name</b></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name_sup" placeholder="Name" autocomplete="off" required />
                                </div>
                            </div>

                            <div class="form-group form-material row">
                                <label class="col-md-3 col-form-label control-label"><b>Short Name</b></label>
                                <div class="col-md-6">
                                    <input type="text" required="required" class="form-control" name="short_name" placeholder="xxx" id="input" autocomplete="off" onkeypress="return event.charCode < 48 || event.charCode  >57" maxlength="3"/>
                                </div>
                            </div>

                            <div class="form-group form-material row">
                                <label class="col-md-3 col-form-label"><b>Email</b></label>
                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" placeholder="@.com" autocomplete="off"/>
                                </div>
                            </div>

                            <div class="form-group form-material row">
                                <label class="col-md-3 col-form-label"><b>Phone Number</b></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="phone" placeholder="+62 XX-XX"/>
                                </div>
                            </div>

                            <div class="form-group form-material row">
                                <button type="submit" class="btn btn-sm btn-round btn-success" id="button1">
                                    <span class=" text">&emsp;SAVE&emsp;</span>
                                </button>
                            </div>
                    </div>
                </div>
                <!-- End Panel Form -->
            </div>

            <div class="col-lg-7">
                <!-- Panel Extended -->
                <div class="panel">
                    <div class="panel-body">
                        <h4 align="center"><b>List Master Data</b></h4>
                        <table class="table table-bordered table-hover table-striped" cellspacing="0" id="exampleTableSearch">
                            <thead>
                                <tr>
                                    <th class="text-center" width="10">No.</th>
                                    <th class="text-center" width="30">ID Supplier</th>
                                    <th class="text-center" width="50">Name</th>
                                    <th class="text-center" width="20">Short Name</th>
                                    <th class="text-center" width="20">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; foreach ($data_supplier as $value) { ?>
                                    <tr>
                                        <td class="text-center"><?= $no++ ?></td>
                                        <td class="text-center"><?= $value->id_supplier ?></td>
                                        <td class="text-center"><?= $value->name_sup ?></td>
                                        <td class="text-center"><?= $value->short_name ?></td>
                                        <td class="actions" width='50' align="center">
                                           <button type='submit' data-bind='<?=$value->id_supplier?>' data-toggle="tooltip" class='btn btn-success detail btn btn-floating btn-info btn-xs' title="Detail"><i class="icon md-assignment-check" aria-hidden="true"></i></button>

                                           <button type='submit' data-bind='<?=$value->id_supplier?>' class='btn btn-success edit btn btn-floating btn-success btn-xs' data-toggle="tooltip" title="Edit"><i class="icon md-icon md-edit" aria-hidden="true"></i></button>

                                           <button type='button' data-bind='<?=$value->id_supplier?>' class='btn btn-success nonactive btn btn-floating btn-danger btn-xs' data-toggle="tooltip" title="Delete" disabled><i class="icon md-icon md-delete" aria-hidden="true"></i></button>
                                       </td>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- End Panel Extended -->

<script>
    $(function() {
        $('#input').keyup(function() {
            this.value = this.value.toLocaleUpperCase();
        });
    });
</script> 

<script type="text/javascript">//SweetAlert Detail
    $(".detail").click(function() {
        var id = $(this).attr("data-bind");

        swal({
            title: "Are you sure you want to see detail?",
            text: "",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-info",
            confirmButtonText: "Yes !",
            cancelButtonText: "Cancel !",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    // url: '<?= base_url("master_data/detail/") ?>'+id,
                    type: 'DELETE',
                    error: function() {
                        alert('Something is wrong');
                    },
                    success: function(data) {
                        $("#" + id).remove();
                        // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        window.location.href = '<?= base_url("master_data/detail_supplier/") ?>' + id;
                    }
                });
            } else {
                swal("Cancelled", "You Canceled To See Detail :)", "error");
            }
        });
    });
</script>

<script type="text/javascript">//edit
    $(".edit").click(function(){
        var id = $(this).attr("data-bind");
    
        swal({
            title: "Are you sure you want to change the data?",
            text: "",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    // url: '<?= base_url("master_data/edit/")?>'+id,
                    type: 'DELETE',
                    error: function() {
                        alert('Something is wrong');
                    },
                    success: function(data) {
                        $("#"+id).remove();
                        // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        window.location.href = '<?= base_url("master_data/edit_supplier/")?>'+id;
                    }
                });
            } else {
              swal("Cancelled", "You Canceled To Edit :)");
            }
        });
    });
</script>

<script type="text/javascript">//delete
    $(".nonactive").click(function(){
        var id = $(this).attr("data-bind");
        console.log(id);
        swal({
            title: "Are you sure want to delete?",
            // text: "you will change this data to Non-Active !",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Delete !",
            cancelButtonText: "Cancel !",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '<?= base_url("master_data/hapus/")?>'+id,
                    type: 'DELETE',
                    error: function() {
                        alert('Something is wrong');
                    },
                    success: function(data) {
                        $("#"+id).remove();
                        swal("Non-Active !", "Your Data is Turned Non-Active.", "success");
                        window.location.reload();
                    }
                });
            } else {
              swal("Cancelled", "You Canceled To Delete :)", "error");
            }
        });
    });
</script>