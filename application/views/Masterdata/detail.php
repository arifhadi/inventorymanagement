<style>
  .card-header {
    background-color: #36459b;
    text-align: center;
  }
  #title {
    padding-top: 5px;
    margin-bottom: 0;
    font-size: 30px;
  }
  #button1 {
    margin-top: 30px;
    left: 48%;
  }
  .container {
    top: 50%;
  }
  .action {
    width: 200;
  }
</style>

<div class="page">
    <div class="page-header" style="padding: 0px;">
      <div class="col-md-12">
          <div class="card-body">
              <a href="<?php echo base_url("Master_data") ?>" type="button" class="btn btn-info btn-round btn-sm" data-style="slide-left" data-plugin="ladda" data-type="progress" id="back">
                <span class="ladda-label"><i class="icon md-undo" aria-hidden="true"></i>&emsp;Back&emsp;</span>
              </a>
          </div>
      </div>
      <h3 align="center" style="padding: 0px;">Detail Master Data</h3> 
    </div>
  
    <div class="page-content">
        <!-- button-->
      <div class="col-md-12">
        <div class="panel">
          <?php echo form_open_multipart('master_data/save_add_item'); ?>
            <div class="panel-body container-fluid" align="left">
              <div class="row row-lg-12">
                <div class="col-md-12 col-lg-6">
                  <div class="example-wrap">
                    <div class="example">

                        <div class="form-group form-material row">
                          <label class="col-md-3 col-form-label control-label"><b>ID Item</b> </label>
                          <div class="col-md-6">
                            <input type="text" class="form-control" name="id_item" placeholder="ID Item" autocomplete="off" disabled value="<?= $data_details->id_item ?>" />
                          </div>
                        </div>
                                    
                        <div class="form-group form-material row">
                          <label class="col-md-3 col-form-label control-label"><b>Name</b> </label>
                          <div class="col-md-6">
                            <input type="text" class="form-control" name="name" placeholder="Full Name" autocomplete="off" disabled value="<?= $data_details->name ?>" />
                          </div>
                        </div>
                                    
                        <div class="form-group form-material row">
                          <label class="col-md-3 col-form-label control-label"><b>Category</b> </label>
                          <div class="col-md-6">
                            <input type="text" class="form-control" name="name" placeholder="Full Name" autocomplete="off" disabled value="<?= $data_details->nama_category ?>" />
                          </div>
                        </div>

                        <div class="form-group form-material row">
                          <label class="col-md-3 col-form-label control-label"><b>Inventory Category</b></label>
                          <div class="col-md-6">
                              <select name="inventory_category" class="form-control" data-plugin="select2" disabled>
                                  <option <?php if(""== $data_details->inventory_category){ echo 'selected="selected"'; } ?> value="">select</option>
                                  <option <?php if($data_details->inventory_category == 1){ echo 'selected="selected"'; }?> value="1">Packaging</option>
                                  <option <?php if($data_details->inventory_category == 2){ echo 'selected="selected"'; }?> value="2">Stationary</option>
                              </select>
                          </div>
                        </div>

                        <div class="form-group form-material row">
                          <label class="col-md-3 col-form-label control-label"><b>Customer</b></label>
                          <div class="col-md-6">
                              <select name="id_customer" class="form-control" data-plugin="select2" disabled>
                                  <option value="">select</option>
                                  <?php foreach ($customers as $value) { ?>
                                      <option <?php if($value->id_customer == $data_details->id_customer){ echo 'selected="selected"'; } ?> value="<?php echo $value->id_customer ?>"><?php echo $value->name_cust?></option>
                                  <?php } ?>
                              </select>
                          </div>
                        </div>

                        <div class="form-group form-material row">
                          <label class="col-md-3 col-form-label control-label"><b>Supplier</b></label>
                          <div class="col-md-6">
                              <select name="id_supplier" class="form-control" data-plugin="select2" disabled>
                                  <option value="">select</option>
                                  <?php foreach ($supplier as $value) { ?>
                                      <option <?php if($value->id_supplier == $data_details->id_supplier){ echo 'selected="selected"'; } ?> value="<?php echo $value->id_supplier ?>"><?php echo $value->name_sup?></option>
                                  <?php } ?>
                              </select>
                          </div>
                        </div>
                        
                    </div>
                  </div>
                </div>

                <div class="col-md-12 col-lg-6">
                  <div class="example-wrap">
                    <div class="example">

                      <div class="form-group form-material row">
                        <label class="col-md-3 col-form-label control-label"><b>Unit</b> </label>
                        <div class="col-md-6">
                          <input type="text" class="form-control" name="name" placeholder="Full Name" autocomplete="off" disabled value="<?= $data_details->unit ?>" />
                        </div>
                      </div>
                                            
                      <div class="form-group form-material row">
                        <label class="col-md-3 col-form-label control-label"><b>Opening</b> </label>
                        <div class="col-md-6">
                          <input type="text" class="form-control" name="opening" placeholder="Opening" autocomplete="off" disabled value="<?= $data_details->opening ?>" />
                        </div>
                      </div>
                                            
                      <div class="form-group form-material row">
                        <label class="col-md-3 col-form-label control-label"><b>Remarks</b> </label>
                        <div class="col-md-6">
                          <input type="text" class="form-control" name="remarks" placeholder="" autocomplete="off" disabled value="<?= $data_details->remarks ?>" />
                        </div>
                      </div>

                      <div class="form-group form-material row">
                        <label class="col-md-3 col-form-label control-label"><b>Image</b></label>
                        <div class="col-md-6">
                          <?php if (!empty($data_details->image)) { ?>
                            <img id="myImg" src="<?php echo base_url('master_data/download/'. $data_details->image) ?>" width='270' height='180'>
                          <?php }else{ ?>
                            <img src="<?php echo base_url('/src/assets/images/profile/images.png '); ?>"  width='270' height='180'>
                          <?php } ?>
                        </div>
                      </div>

                      <div class="form-group form-material row">
                        <label class="col-md-3 col-form-label control-label"></label>
                        <div class="col-md-6">
                          <a href="<?php echo base_url('master_data/download/'. $data_details->image) ?>" type="button" class="btn btn-squared btn-info btn-lg waves-effect waves-light waves-round" data-style="slide-left" data-plugin="ladda" data-type="progress" id="back"><span class="ladda-label">Download &nbsp;<i class="icon md-download " aria-hidden="true"></i></span></a>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php echo form_close(); ?>
          </div>
        </div>
    </div>
</div>

<style>

  #myImg:hover {opacity: 0.7;}

  /* The Modal (background) */
  .modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 99999; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
  }

  /* Modal Content (image) */
  .modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
  }

  /* Caption of Modal Image */
  #caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
  }

  /* Add Animation */
  .modal-content, #caption {  
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
  }

  @-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
  }

  @keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
  }

  /* The Close Button */
  .close {
    position: absolute;
    top: 15px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
  }

  .close:hover,
  .close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
  }

  /* 100% Image Width on Smaller Screens */
  @media only screen and (max-width: 700px){
    .modal-content {
      width: 100%;
    }
  }
</style>
    <!-- The Modal -->
    <div id="myModal" class="modal">
      <span class="close">&times;</span>
      <img class="modal-content" id="img01">
      <!-- <div style="text-align: center;"><h3><?=$data_details->image;?></h3></div> -->
    </div>


    <script>
      // Get the modal
      var modal = document.getElementById("myModal");

      // Get the image and insert it inside the modal - use its "alt" text as a caption
      var img = document.getElementById("myImg");
      var modalImg = document.getElementById("img01");
      var captionText = document.getElementById("caption");
      img.onclick = function(){
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
      }

      // Get the <span> element that closes the modal
      var span = document.getElementsByClassName("close")[0];

      // When the user clicks on <span> (x), close the modal
      span.onclick = function() { 
        modal.style.display = "none";
      }
    </script>