<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 5px;
        margin-bottom: 0;
        font-size: 23px;
    }

    #button1 {
        margin-top: 30px;
        left: 48%;
    }

    .p{
        font-size: 40px;
        letter-spacing: 5px;
    }


    .control-label:after {
        content: "*";
        color: red;
    }
</style>

<div class="page">
    <div class="page-header" style="padding: 0px;">
        <div class="col-md-12"><br>
            <!-- button-->
            <a href="<?php echo base_url("Dashboard/indirect") ?>" type="button" class="btn btn-danger btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
                <span class="ladda-label"><i class="icon md-home mr-10" aria-hidden="true"></i>Home</span>
            </a>
            <a href="<?php echo base_url("master_data") ?>" type="button" class="btn btn-info btn-round" data-style="slide-left" data-plugin="ladda" data-type="progress" id="back">
                <span class="ladda-label"><i class="icon md-undo" aria-hidden="true"></i>Back</span>
            </a>
        </div>
    </div>

    <h3 align='center'><b>Add Category</b></h3>

    <div class="page-content container-fluid" style="padding: 0px;">
        <div class="row">
            <div class="col-lg-6">
                <div class="panel">
                 <!-- allert -->
                 <?php if ($this->session->flashdata('info')) { ?>
                    <div class="alert dark alert-success alert-dismissible" role="alert" id="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button><?= $this->session->flashdata('info'); ?>
                    </div>
                <?php } ?>
                    <div class="panel-body">
                    <h4 align="center"><b>Form Add Category</b></h4>
                        <?php echo form_open_multipart('master_data/save_category'); ?>
                            <div class="row row-lg-12">
                                <div class="col-md-12 col-lg-12">
                                    <div class="example-wrap">
                                        <div class="example">
                                            <div class="form-group row">
                                                <label class="col-md-6 col-form-label control-label" align='center'>ID Category</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="id_category" placeholder="ID Category" autocomplete="off" required />
                                                </div>
                                            </div>
                                            <br>
                                            <div class="form-group row">
                                                <label class="col-md-6 col-form-label control-label" align='center'>Name Category </label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="nama_category" placeholder="Name Category" autocomplete="off" required />
                                                </div>
                                            </div>
                                            <br>
                                            <div class="card-body" id="body" align="center">
                                                <button type="submit" class="btn btn-success btn-round btn-md">
                                                    <span class=" text">&emsp;Save&emsp;</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- End Panel Form -->
            </div>

            <div class="col-lg-6">
                <!-- Panel Extended -->
                <div class="panel">
                    <div class="panel-body">
                        <h4 align="center"><b>List Add Category</b></h4><br>
                        <table class="table table-bordered table-hover table-striped" cellspacing="0" id="exampleTableSearch">
                            <thead>
                                <tr>
                                    <th style="width: 5px;">ID</th>
                                    <th style="width: 10px;">ID Category</th>
                                    <th style="width: 10px;">Name</th>
                                    <th style="width: 10px;">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1;
                                foreach ($category as $value) { ?>
                                    <tr>
                                        <td><?= $no++ ?></td>
                                        <td><?= $value->id_category ?></td>
                                        <td><?= $value->nama_category ?></td>
                                        <td class="actions" width='90' align="center">
                                            <button type='submit' data-bind='<?= $value->id_category ?>' class='btn btn-success edit btn btn-floating btn-xs' data-toggle="tooltip" title="Edit"><i class="icon md-icon md-edit" aria-hidden="true"></i></button>

                                            <button type='submit' data-bind='<?= $value->id_category ?>' class='btn nonactive btn btn-floating btn-danger btn-xs' data-toggle="tooltip" title="Delete"><i class="icon md-icon md-delete" aria-hidden="true"></i></button>
                                        </td>
                                    <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
        //delete
        $(".nonactive").click(function() {
            var id = $(this).attr("data-bind");
            console.log(id);
            swal({
                    title: "Are you sure want to delete?",
                    // text: "you will change this data to Non-Active !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Delete !",
                    cancelButtonText: "Cancel !",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: '<?= base_url("master_data/hapus_category/") ?>' + id,
                            type: 'DELETE',
                            error: function() {
                                alert('Something is wrong');
                            },
                            success: function(data) {
                                $("#" + id).remove();
                                swal("Non-Active !", "Your Data is Turned Non-Active.", "success");
                                window.location.reload();
                            }
                        });
                    } else {
                        swal("Cancelled", "You Canceled To Delete :)", "error");
                    }
                });
        });
    </script>

    <script type="text/javascript">
        //edit
        $(".edit").click(function() {
            var id = $(this).attr("data-bind");

            swal({
                    title: "Are you sure you want to change the data?",
                    text: "",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: "btn-success",
                    confirmButtonText: "Yes",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            // url: '<?= base_url("backend/main_menu/change_masterdata/") ?>'+id,
                            type: 'DELETE',
                            error: function() {
                                alert('Something is wrong');
                            },
                            success: function(data) {
                                $("#" + id).remove();
                                // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                                window.location.href = '<?= base_url("master_data/edit_category/") ?>' + id;
                            }
                        });
                    } else {
                        swal("Cancelled", "You Canceled To Edit :)");
                    }
                });
        });
    </script>                