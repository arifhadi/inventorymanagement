<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 5px;
        margin-bottom: 0;
        font-size: 30px;
    }

    #button1 {
        margin-top: 30px;
        left: 48%;
    }

    .container {
        top: 50%;
    }

    .action {
        width: 200;
    }
    .control-label:after {
                content: "*";
                color: red;
            }
</style>

<div class="page">
    <div class="page-header" style="padding: 0px;">
        <div class="col-md-12">
            <!-- button--><br>
            <a href="<?php echo base_url("Master_data") ?>" type="button" class="btn btn-info btn-round" data-style="slide-left" data-plugin="ladda" data-type="progress" id="back">
                <span class="ladda-label"><i class="icon md-undo" aria-hidden="true"></i>BACK</span>
            </a>
        </div>
    </div>
    <h3 align="center">Edit Master Data</h3> 
    <div class="page-content" style="padding: 0px;">
        <!-- button-->
        <div class="col-md-12">
            <div class="panel">
                <!-- allert -->
                <?php if ($this->session->flashdata('info')) { ?>
                    <div class="alert dark alert-success alert-dismissible" role="alert" id="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?= $this->session->flashdata('info'); ?>
                    </div>
                <?php } ?>
                <div class="panel-body">
                    <div class="panel">
                        <div class="panel-body">
                        <?php echo form_open_multipart('Master_data/update_data'); ?>
                            <div class="panel-body container-fluid" align="left" style="padding: 0px;">
                                <div class="row row-lg-12">
                                    <div class="col-md-12 col-lg-6">
                                        <div class="example-wrap">
                                            <div class="example">
                                                <div class="form-group form-material row">
                                                    <label class="col-md-3 col-form-label control-label"><b>ID Item</b></label>
                                                    <div class="col-md-2">
                                                        <input type="text" class="form-control" name="id_item" autocomplete="off" readonly="readonly" required value="<?= $data_edit->id_item ?>" />
                                                    </div>
                                                </div>
                                                <div class="form-group form-material row">
                                                    <label class="col-md-3 col-form-label control-label"><b>Name</b></label>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" name="name" placeholder="Full Name" autocomplete="off" required value="<?= $data_edit->name ?>" />
                                                    </div>
                                                </div>
                                                <div class="form-group form-material row">
                                                    <label class="col-md-3 col-form-label control-label"><b>Category</b></label>
                                                    <div class="col-md-6">
                                                        <select name="id_category" class="form-control" data-plugin="select2">
                                                            <option>Select</option>
                                                            <?php foreach ($category as $value) { ?>
                                                                <option <?php if($value->id_category == $data_edit->id_category){ echo 'selected="selected"'; } ?>  value="<?php echo $value->id_category ?>"><?php echo $value->nama_category ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group form-material row">
                                                    <label class="col-md-3 col-form-label control-label"><b>Inventory Category</b></label>
                                                    <div class="col-md-6">
                                                        <select name="inventory_category" class="form-control" data-plugin="select2" required>
                                                            <option <?php if(""== $data_edit->inventory_category){ echo 'selected="selected"'; } ?> value="">select</option>
                                                            <option <?php if($data_edit->inventory_category == 1){ echo 'selected="selected"'; }?> value="1">Packaging</option>
                                                            <option <?php if($data_edit->inventory_category == 2){ echo 'selected="selected"'; }?> value="2">Stationary</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group form-material row">
                                                    <label class="col-md-3 col-form-label control-label"><b>Customer</b></label>
                                                    <div class="col-md-6">
                                                        <select name="id_customer" class="form-control" data-plugin="select2" required>
                                                            <option value="">select</option>
                                                            <?php foreach ($customers as $value) { ?>
                                                                <option <?php if($value->id_customer == $data_edit->id_customer){ echo 'selected="selected"'; } ?> value="<?php echo $value->id_customer ?>"><?php echo $value->name_cust?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group form-material row">
                                                    <label class="col-md-3 col-form-label control-label"><b>Supplier</b></label>
                                                    <div class="col-md-6">
                                                        <select name="id_supplier" class="form-control" data-plugin="select2" required>
                                                            <option value="">select</option>
                                                            <?php foreach ($supplier as $value) { ?>
                                                                <option <?php if($value->id_supplier == $data_edit->id_supplier){ echo 'selected="selected"'; } ?> value="<?php echo $value->id_supplier ?>"><?php echo $value->name_sup?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group  row">
                                                    <label class="col-md-3 col-form-label "><b>Unit</b></label>
                                                    <div class="col-md-4"> 
                                                        <select id="test" class="form-control" required="required" data-plugin="select" id="priority" name="unit" data-placeholder="Select Priority" >
                                                            <option <?php if($data_edit->unit == 'Pack'){ echo 'selected="selected"'; } ?> value="Pack">Pack</option>
                                                            <option <?php if($data_edit->unit == 'Pcs'){ echo 'selected="selected"'; } ?>>Pcs</option>
                                                            <option <?php if($data_edit->unit == 'Kg'){ echo 'selected="selected"'; } ?>>Kg</option>
                                                            <option <?php if($data_edit->unit == 'Roll'){ echo 'selected="selected"'; } ?>>Roll</option>
                                                            <option <?php if($data_edit->unit == 'Kaleng'){ echo 'selected="selected"'; } ?>>Kaleng</option>
                                                            <option <?php if($data_edit->unit == $data_edit->unit){ echo 'selected="selected"'; } ?>><?= $data_edit->unit ?></option>
                                                            <option class="editable" value="other">Other</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <textarea class="editOption form-control" data-plugin="maxlength" maxlength="100" rows="1" placeholder="location" style="display:none;"></textarea>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-lg-6">
                                        <div class="example-wrap">
                                            <div class="example">

                                                <div class="form-group form-material row">
                                                    <label class="col-md-3 col-form-label "><b>Opening</b></label>
                                                    <div class="col-md-5">
                                                        <input type="number" min="0" class="form-control" id="form2" name="opening" placeholder="" autocomplete="off" value="<?= $data_edit->opening ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group -material row">
                                                    <label class="col-md-3 col-form-label "><b>Remarks</b></label>
                                                    <div class="col-md-5">
                                                        <div class="example-wrap">
                                                        <textarea class="form-control" name="remarks" id="textareaDefault" rows="3"><?= $data_edit->remarks?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label"><b>image</b></label>
                                                    <div class="col-md-6">
                                                        <?php if(!$data_edit->image){?>
                                                            <input type="file" name="image" multiple="" id="input-file-now-custom-1" data-plugin="dropify" data-default-file="" />
                                                        <?php }  else{?>
                                                            <input type="file" name="image" multiple="" id="input-file-now-custom-1" data-plugin="dropify" data-default-file="<?php echo base_url("/src/assets/images/foto/" . $data_edit->image) ?>" width='270' height='180' />
                                                        <?php }?>
                                                    </div>
                                                </div>

                                                <div class="form-group -material row" hidden>
                                                    <label class="col-md-3 col-form-label "><b>status</b></label>
                                                    <div class="col-md-6">
                                                        <div class="example-wrap">
                                                        <?php 
                                                        if(!$value->status == 0) { ?>
                                                            <input type="text" class="form-control" name="status" placeholder="" autocomplete="off" value="2">
                                                        <?php }elseif($value->status == 1 or 2) { ?>
                                                            <input type="text" class="form-control" name="status" placeholder="" autocomplete="off" value="<?= $data_edit->status ?>">
                                                        <?php }?>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>

                                    <!-- button -->
                                    <div class="card-body" id="body" align="center">
                                        <button type="submit" class="btn btn-round btn-success">
                                            <span class=" text">&emsp;Save&emsp;</span>
                                        </button>
                                    </div>

                                </div>
                            </div>
                        <?php echo form_close(); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
      var initialText = $('.editable').val();
        $('.editOption').val(initialText);

        $('#test').change(function(){
        var selected = $('option:selected', this).attr('class');
        var optionText = $('.editable').text();

        if(selected == "editable"){
          $('.editOption').show();

          
          $('.editOption').keyup(function(){
              var editText = $('.editOption').val();
              $('.editable').val(editText);
              $('.editable').html(editText);
          });

        }else{
          $('.editOption').hide();
        }
      });
</script>