<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 5px;
        margin-bottom: 0;
        font-size: 23px;
    }

    #button1 {
        margin-top: 30px;
        left: 48%;
    }



    .control-label:after {
        content:"*";
        color:red;
    }
</style>

<br>


<div class="page">
    <div class="page-header">
        <div class="col-md-12">
            <!-- button-->
            <a href="<?php echo base_url("Master_data/add_category") ?>" type="button" class="btn btn-info btn-round" data-style="slide-left" data-plugin="ladda" data-type="progress" id="back">
                    <span class="ladda-label"><i class="icon md-undo" aria-hidden="true"></i>BACK</span>
            </a>
        </div>
    </div>

    <h2 align="center">Form Edit Category</h2>

    <div class="page-content">

        <!-- button-->
        <div class="col-md-12">
            <div class="panel">
                <!-- allert -->
                <?php if ($this->session->flashdata('info')) { ?>
                    <div class="alert dark alert-success alert-dismissible" role="alert" id="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?= $this->session->flashdata('info'); ?>
                    </div>
                <?php } ?>
                <div class="panel-body">

                    <div class="panel">
                        <div class="panel-body">
                            <?php echo form_open_multipart('master_data/update_category/'); ?>
                                <div class="row row-lg-12">
                                    <div class="col-md-12 col-lg-4">
                                        <div class="example-wrap">
                                            <div class="example"></div>
                                        </div>
                                    </div> 

                                    <div class="col-md-12 col-lg-4">
                                        <div class="example-wrap">
                                            <div class="example">
                                                <div class="form-group form-material row">
                                                    <label class="col-md-5 col-form-label control-label">ID Category</label>
                                                    <div class="col-md-7">
                                                        <input type="text" class="form-control" name="id_category" placeholder="ID Category" autocomplete="off" readonly  value=<?= $edit_category->id_category ?>>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="form-group form-material row">
                                                    <label class="col-md-5 col-form-label control-label">Name Category </label>
                                                    <div class="col-md-7">
                                                        <!-- <input type="text" class="form-control" name="nama_category" placeholder="Name Category" autocomplete="off"  value=<?= $edit_category->nama_category?>> -->
                                                        <input type="text" class="form-control" name="nama_category" value="<?=$edit_category->nama_category;?>">
                                                    </div>
                                                </div>
                                            <br>
                                            <br>
                                            <div class="card-body" id="body" align="center">
                                                <button type="submit" class="btn btn-success btn-round">
                                                    <span class=" text">&nbsp;&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                </button>
                                            </div>

                                            </div>
                                        </div>
                                    </div>

                                    </div>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>