<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 5px;
        margin-bottom: 0;
        font-size: 30px;
    }

    #button1 {
        margin-top: 30px;
        left: 48%;
    }

    .container {
        top: 50%;
    }

    #jan {
        color: white;
        background-color: blueviolet;
    }
</style>

<!-- button-->
<div class="page">
    <div class="page-header" style="padding: 0px;"><br>
        <div class="col-md-12">
            <!-- button-->
            <a href="<?php echo base_url("Dashboard/indirect") ?>" type="button" class="btn btn-danger btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
                <span class="ladda-label"><i class="icon md-home mr-10" aria-hidden="true"></i>Home</span>
            </a>
        </div>
    </div>
    <div class="page-content">
        <h3 align="center">Form Request List</h3>
        <!-- button-->
        <div class=" col-md-">
            <div class="panel" style="padding: 0px;">
                <div class="panel-body">
                    <div class="panel">
                        <div class="panel-body">
                            <table class="table table-hover dataTable table-striped w-full no-footer dtr-inline" cellspacing="0" id="exampleTableSearch">
                                <thead>
                                    <tr align="center" class>
                                        <th class="text-center">No</th>
                                        <th class="text-center">ID Item</th>
                                        <th class="text-center">Name</th>
                                        <th class="text-center">Unit</th>
                                        <th class="text-center">Balance Stock</th>
                                        <th class="text-center">Total Request</th>
                                        <th class="text-center">Add Request</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1;
                                    foreach ($request as $value) { ?>
                                        <tr>
                                            <td class="text-center"><?= $no++ ?></td>
                                            <td class="text-center"><?= $value->id_item ?></td>
                                            <td class="text-center"><?= $value->name ?></td>
                                            <td class="text-center"><?= $value->unit ?></td>
                                            <td class="text-center"><?= $value->balance_stock ?></td>
                                            <td class="text-center"><?= $value->tot_request ?></td>
                                            <td class="text-center">
                                                <?php
                                                if ($value->balance_stock > 1) { ?>
                                                    <button type='submit' data-bind='<?= $value->id_item ?>' data-toggle="tooltip" title="Add Request" class='btn btn-success add_stock btn btn-floating btn-warning btn-xs'><i class="icon md-plus-circle" aria-hidden="true"></i></button>
                                                <?php } elseif ($value->balance_stock < 1) { ?>
                                                    <button disabled class='btn btn-success add_stock btn btn-floating btn-warning btn-xs'><i class="icon md-plus-circle" aria-hidden="true"></i></button>
                                                <?php } ?>
                                            <td align="center">
                                                <button type='submit' data-bind='<?= $value->id_item ?>' data-toggle="tooltip" title="Detail" class='btn btn-success detail btn btn-floating btn-info btn-xs'><i class="icon md-format-list-bulleted" aria-hidden="true"></i></button>
                                                <button type='submit' data-bind='<?= $value->id_item ?>' data-toggle="tooltip" title="Delete" class='btn btn-success edit btn btn-floating btn-success btn-xs'><i class="icon md-icon md-edit" aria-hidden="true"></i></button>
                                            </td>
                                        <?php } ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- SweetAlert Detail -->
<script type="text/javascript">
    //Change Data
    $(".detail").click(function() {
        var id = $(this).attr("data-bind");

        swal({
                title: "Are you sure you want to see detail?",
                text: "",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Yes !",
                cancelButtonText: "Cancel !",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        // url: '<?= base_url("requestor/detail/") ?>'+id,
                        type: 'DELETE',
                        error: function() {
                            alert('Something is wrong');
                        },
                        success: function(data) {
                            $("#" + id).remove();
                            // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            window.location.href = '<?= base_url("requestor/detail/") ?>' + id;
                        }
                    });
                } else {
                    swal("Cancelled", "You Canceled To See Detail", "error");
                }
            });
    });
</script>

<!-- SweetAlert Edit -->
<script type="text/javascript">
    //Change Data
    $(".edit").click(function() {
        var id = $(this).attr("data-bind");

        swal({
                title: "Are you sure you want to change the data?",
                text: "",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Yes !",
                cancelButtonText: "Cancel !",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        // url: '<?= base_url("requestor/tb_edit/") ?>'+id,
                        type: 'DELETE',
                        error: function() {
                            alert('Something is wrong');
                        },
                        success: function(data) {
                            $("#" + id).remove();
                            // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            window.location.href = '<?= base_url("requestor/tb_edit/") ?>' + id;
                        }
                    });
                } else {
                    swal("Cancelled", "You Canceled To Edit", "error");
                }
            });
    });
</script>

<!-- SweetAlert Add Stock -->
<script type="text/javascript">
    //Change Data
    $(".add_stock").click(function() {
        var id = $(this).attr("data-bind");

        swal({
                title: "Are you sure you want to Add Request?",
                text: "",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Yes !",
                cancelButtonText: "Cancel !",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        // url: '<?= base_url("requestor/form/") ?>'+id,
                        type: 'DELETE',
                        error: function() {
                            alert('Something is wrong');
                        },
                        success: function(data) {
                            $("#" + id).remove();
                            // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            window.location.href = '<?= base_url("requestor/form/") ?>' + id;
                        }
                    });
                } else {
                    swal("Cancelled", "You Canceled To Add Request", "error");
                }
            });
    });
</script>