<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 5px;
        margin-bottom: 0;
        font-size: 30px;
    }

    .button {
        display: flex;
        color: white;
        justify-content: space-evenly;
    }

    .container {
        top: 50%;
    }

    #jan {
        color: white;
        background-color: blueviolet;
    }

    .expand {
        height: 6em;
        width: 100%;
        padding: 5px;
        resize: none;
        overflow-y: scroll;
    }
</style>

<!-- button-->
<div class="page">
    <div class="page-header" style="padding: 0px;"><br>
        <div class="col-md-12">
            <a href="<?php echo base_url("Dashboard/indirect") ?>" type="button" class="btn btn-danger btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
                <span class="ladda-label"><i class="icon md-home mr-10" aria-hidden="true"></i>Home</span>
            </a>
            <a href="<?php echo base_url("requestor") ?>" type="button" class="btn btn-info btn-round" data-style="slide-left" data-plugin="ladda" data-type="progress" id="back">
                <span class="ladda-label"><i class="icon md-undo" aria-hidden="true"></i>Back</span>
            </a>
        </div>
    </div>

    <h3 align="center">Detail List</h3>

     <div class="container" style="padding: 0px;">
        <div class="container">
            <!-- button-->
            <div class=" col-md-">
                <div class="panel">
                        <!-- allert
                <?php if ($this->session->flashdata('info')) { ?>
                    <div class="alert dark alert-success alert-dismissible" role="alert" id="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?= $this->session->flashdata('info'); ?>
                    </div>
                <?php } ?> -->
                        <!-- Tabel List -->
                    <div class="panel-body">
                        <div class="panel">
                            <div class="panel-body">
                                <table class="table table-hover dataTable table-striped w-full no-footer dtr-inline" cellspacing="0" id="exampleTableSearch">
                                    <thead>
                                        <tr>
                                            <th class="text-center">No.</th>
                                            <th class="text-center">Requestor</th>
                                            <th class="text-center">ID Item</th>
                                            <th class="text-center">Date Request</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Total Request</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1; foreach ($data_details as $value) { ?>
                                            <tr>
                                                <td class="text-center"><?= $no++ ?></td>
                                                <td class="text-center"><?= $value->requestor ?></td>
                                                <td class="text-center"><?= $value->id_item ?></td>
                                                <td class="text-center"><?= $value->date_request ?></td>
                                                <td class="text-center">
                                                    <?php if ($value->status == 1) { ?>
                                                        <span class="badge badge-secondary" style="font-size: 12px;">
                                                                Waiting Acceptance
                                                        </span>
                                                    <?php } elseif ($value->status == 2) { ?>
                                                        <span class="badge badge-success" style="font-size: 12px;">
                                                                Acceptance
                                                        </span>
                                                    <?php } elseif ($value->status == 3) { ?>
                                                        <span class="badge badge-danger" style="font-size: 12px;">
                                                                Rejected
                                                        </span>
                                                    <?php } ?>
                                                </td>
                                                <td class="text-center"><?= $value->total_request ?></td>
                                                <td class="text-center">
                                                    <button type='submit' data-bind='<?= $value->id ?>' data-toggle="tooltip" title="More Detail" class='btn detail btn-floating btn-info btn-xs'><i class="icon md-format-list-bulleted" aria-hidden="true"></i></button>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                        <tr>
                                            <td class="text-center" colspan="5"><b>Total</b></td>
                                            <td class="text-center" style="color:red"><?= $total_list->total_request ?></td>
                                            <td></td> 
                                        </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Alert Button Detail -->
<script type="text/javascript">
    //Change Data
    $(".detail").click(function() {
        var id = $(this).attr("data-bind");

        swal({
            title: "Are you sure you want to see detail?",
            text: "",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-info",
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    // url: '<?= base_url("requestor/more_dtl/") ?>'+id,
                    type: 'DELETE',
                    error: function() {
                        alert('Something is wrong');
                    },
                    success: function(data) {
                        $("#" + id).remove();
                        // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        window.location.href = '<?= base_url("requestor/more_dtl/") ?>' + id;
                    }
                });
            } else {
                swal("Cancelled", "You Canceled To See Detail)", "error");
            }
        });
    });
</script>
<!-- End Alert Button Detail -->