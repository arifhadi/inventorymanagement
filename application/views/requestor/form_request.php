<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 5px;
        margin-bottom: 0;
        font-size: 23px;
    }

    #button1 {
        margin-top: 30px;
        left: 48%;
    }


    .control-label:after {
        content: "*";
        color: red;
    }
</style>

<div class="page">
    <div class="page-header" style="padding: 0px;"><br>
        <div class="col-md-12">
            <!-- button-->
            <a href="<?php echo base_url("Dashboard/indirect") ?>" type="button" class="btn btn-danger btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
                <span class="ladda-label"><i class="icon md-home mr-10" aria-hidden="true"></i>Home</span>
            </a>
            <a href="<?php echo base_url("requestor") ?>" type="button" class="btn btn-info btn-round" data-style="slide-left" data-plugin="ladda" data-type="progress" id="back">
                <span class="ladda-label"><i class="icon md-undo" aria-hidden="true"></i>Back</span>
            </a>
        </div>
    </div>

    <h3 align="center">Form Request</h3>
    <!-- <div class="page-content"> -->

    <div class="page-content container-fluid" style="padding: 0px;">
        <div class="row">
            <div class="col-lg-5">
                <!-- Panel Basic -->
                <div class="panel">
                    <div class="panel-body">
                        <!-- allert -->
                        <?php if ($this->session->flashdata('info')) { ?>
                            <div class="alert dark alert-success alert-dismissible" role="alert" id="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <?= $this->session->flashdata('info'); ?>
                            </div>
                        <?php } ?>
                        <form method="POST" action="<?php echo base_url('requestor/save_request'); ?>">
                            <h3 align="center">Form</h3><br>
                                <div class="form-group form-material row">
                                    <label class="col-md-3 col-form-label control-label"><b>Requestor </b></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="requestor" placeholder="Nama" autocomplete="off" value="<?= USER_NAME ?>" required />
                                    </div>
                                </div>

                                <div class="form-group form-material row">
                                    <label class="col-md-3 col-form-label control-label"><b>ID Item </b></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="id_item" placeholder="ID Item" readonly autocomplete="off" required value="<?= $id_item ?>" />
                                    </div>
                                </div>

                                <div class="form-group form-material row">
                                    <label class="col-md-3 col-form-label control-label"><b>Stock </b></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="balance_stock" placeholder="Stock" readonly autocomplete="off" required value="<?= $balance_stock ?>" />
                                    </div>
                                </div>

                                <div class="form-group form-material row">
                                    <label class="col-md-3 col-form-label control-label"><b>Total Request </b></label>
                                    <div class="col-md-6">
                                        <input type="number" min="0" max="<?= $balance_stock ?>" step="1" class="form-control" name="total_request" placeholder="Total Request" autocomplete="off" required />
                                    </div>
                                </div>

                                <div class="form-group form-material row">
                                    <label class="col-md-3 col-form-label control-label"><b>Date Request </b></label>
                                    <div class="col-md-6">
                                        <input class="form-control" type="date_request" value="<?php echo date('Y-m-d') ?>" readonly />
                                    </div>
                                </div>

                                <div class="form-group form-material row">
                                    <button type="submit" class="btn btn-md btn-round btn-success" id="button1">
                                        <span class=" text">SAVE</span>
                                        </a>
                                    </button>
                                </div>
                        </form>
                    </div>
                </div>
                <!-- End Panel Form -->
            </div>

            <div class="col-lg-7">
                <!-- Panel Extended -->
                <div class="panel">
                    <div class="panel-body">
                        <h3 align="center">List Request</h3>
                        <table class="table table-hover dataTable table-striped w-full no-footer dtr-inline" cellspacing="0" id="exampleTableSearch">
                            <thead>
                                <tr>
                                    <th class="text-center">No. </th>
                                    <th class="text-center">Requestor </th>
                                    <th class="text-center">ID Item </th>
                                    <th class="text-center">Date Request </th>
                                    <th class="text-center">Status </th>
                                    <th class="text-center">Total Request</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; foreach ($request_list as $value) { ?>
                                    <tr>
                                        <td class="text-center"><?= $no++?></td>
                                        <td class="text-center"><?= $value->requestor?></td>
                                        <td class="text-center"><?= $value->id_item?></td>
                                        <td class="text-center"><?= $value->date_request?></td>
                                        <td class="text-center">
                                            <?php if ($value->status == 1) { ?>
                                                <span class="badge badge-secondary" style="font-size: 12px;">
                                                    Waiting Acceptance
                                                </span>
                                            <?php } elseif ($value->status == 2) { ?>
                                                <span class="badge badge-success" style="font-size: 12px;">
                                                    Acceptance
                                                </span>
                                            <?php } elseif ($value->status == 3) { ?>
                                                <span class="badge badge-danger" style="font-size: 12px;">
                                                    Rejected
                                                </span>
                                            <?php } ?>
                                        </td>
                                        <td class="text-center"><?= $value->total_request?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                                <tr>
                                    <td class="text-center" colspan="5"><b>Total</b></td>
                                    <td class="text-center" style="color:red"><?=$total_request->total_request?></td>
                                </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- End Panel Extended -->