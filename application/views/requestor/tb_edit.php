<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 5px;
        margin-bottom: 0;
        font-size: 30px;
    }

    #button1 {
        margin-top: 30px;
        left: 48%;
    }

    .container {
        top: 50%;
    }

    #jan {
        color: white;
        background-color: blueviolet;
    }
</style>

<!-- button-->
<div class="page">
    <div class="page-header" style="padding: 0px;"><br>
        <div class="col-md-12">
            <!-- button-->
            <a href="<?php echo base_url("requestor") ?>" type="button" class="btn btn-info btn-round" data-style="slide-left" data-plugin="ladda" data-type="progress" id="back">
                <span class="ladda-label"><i class="icon md-undo" aria-hidden="true"></i>BACK</span>
            </a>
        </div>
    </div>
    <div class="page-content">
        <h3 align="center">Request List</h3>
        <!-- button-->
        <div class=" col-md-">
            <div class="panel">
                <div class="panel-body">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                </div>
                            </div>
                            <table class="table table-hover dataTable table-striped w-full no-footer dtr-inline" cellspacing="0" id="exampleTableSearch">
                                <thead>
                                    <tr align="center" class>
                                        <th class="text-center">No</th>
                                        <th class="text-center">Requestor</th>
                                        <th class="text-center">Id Item</th>
                                        <th class="text-center">Date Request</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Total Request</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; foreach ($tb_list as $value) { ?>
                                        <tr>
                                            <td class="text-center"><?= $no++ ?></td>
                                            <td class="text-center"><?= $value->requestor ?></td>
                                            <td class="text-center"><?= $value->id_item ?></td>
                                            <td class="text-center"><?= $value->date_request ?></td>
                                            <td class="text-center">
                                                <?php
                                                if ($value->status == 1) { ?>
                                                    <span class="badge badge-secondary" style="font-size: 12px;">
                                                        Waiting Acceptance
                                                    </span>
                                                <?php } elseif ($value->status == 2) { ?>
                                                    <span class="badge badge-success" style="font-size: 12px;">
                                                        Acceptance
                                                    </span>
                                                <?php } elseif ($value->status == 3) { ?>
                                                    <span class="badge badge-danger" style="font-size: 12px;">
                                                        Rejected
                                                    </span>
                                                <?php } ?>
                                            </td>
                                            <td class="text-center"><?= $value->total_request ?></td>
                                            <td align="center">
                                                <?php if ($value->status == 1) { ?>
                                                    <button type='submit' data-bind='<?= $value->id ?>' data-toggle="tooltip" title="Edit" class='btn btn-success edit btn btn-floating btn-success btn-xs'><i class="icon md-icon md-edit" aria-hidden="true"></i></button>
                                                <?php } elseif ($value->status == 2 or $value->status == 3) { ?>
                                                    <button type='submit' disabled data-bind='<?= $value->id ?>' class='btn btn-success edit btn btn-floating btn-success btn-xs'><i class="icon md-icon md-edit" aria-hidden="true"></i></button>
                                                <?php } ?>
                                            </td>
                                        <?php } ?>
                                        <!-- <tr>
                                            <td class="text-center" colspan="5"><b>Total<b></td>
                                            <td class="text-center" style="color:red"><?= $total_list->total_request ?></td>
                                            <td hidden></td>
                                        </tr> -->
                                </tbody>
                                <tbody>
                                    <tr>
                                        <td class="text-center" colspan="5"><b>Total<b></td>
                                        <td class="text-center" style="color:red"><?= $total_list->total_request ?></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- SweetAlert Edit -->
<script type="text/javascript">
    //Change Data
    $(".edit").click(function() {
        var id = $(this).attr("data-bind");

        swal({
                title: "Are you sure you want to change the data?",
                text: "",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Yes !",
                cancelButtonText: "Cancel !",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        // url: '<?= base_url("requestor/edit/") ?>'+id,
                        type: 'DELETE',
                        error: function() {
                            alert('Something is wrong');
                        },
                        success: function(data) {
                            $("#" + id).remove();
                            // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            window.location.href = '<?= base_url("requestor/edit/") ?>' + id;
                        }
                    });
                } else {
                    swal("Cancelled", "You Canceled To Edit", "error");
                }
            });
    });
</script>