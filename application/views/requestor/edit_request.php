<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 5px;
        margin-bottom: 0;
        font-size: 23px;
    }

    #button1 {
        margin-top: 30px;
        left: 48%;
    }



    .control-label:after {
        content: "*";
        color: red;
    }
</style>

<div class="page">
    <div class="page-header" style="padding: 0px;"><br>
        <div class="col-md-12">
            <a href="<?php echo base_url("requestor/tb_edit/" . $tb_req->id_item) ?>" type="button" class="btn btn-info btn-round" data-style="slide-left" data-plugin="ladda" data-type="progress" id="back">
                <span class="ladda-label"><i class="icon md-undo" aria-hidden="true"></i>BACK</span>
            </a>
        </div>
    </div>

    <h3 align="center">Form Request</h3>

    <div class="page-content" style="padding: 0px;">
        <!-- button-->
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body" style="padding: 0px 0px;">
                    <!-- allert -->
                    <?php if ($this->session->flashdata('info')) { ?>
                        <div class="alert dark alert-success alert-dismissible" role="alert" id="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?= $this->session->flashdata('info'); ?>
                        </div>
                    <?php } ?>
                    <div class="panel">
                        <div class="panel-body">
                            <!-- <form method="POST" action="<?php echo base_url('incoming_item/update_edit/'); ?>"> -->
                            <?php echo form_open_multipart('requestor/save_edit/'); ?>
                            <div class="row row-lg-12">
                                <div class="col-md-12 col-lg-3">
                                    <div class="example-wrap">
                                        <div class="example"></div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-lg-6">
                                    <div class="example-wrap">
                                        <div class="form-group form-material row" hidden>
                                            <label class="col-md-3 col-form-label control-label"><b>Id </b></label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="id" placeholder="id" autocomplete="off" required value="<?= $tb_req->id ?>" />
                                            </div>
                                        </div>

                                        <div class="form-group form-material row">
                                            <label class="col-md-3 col-form-label control-label"><b>Requestor </b></label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="requestor" placeholder="PIC" autocomplete="off" required value="<?= $tb_req->requestor ?>" />
                                            </div>
                                        </div>

                                        <div class="form-group form-material row">
                                            <label class="col-md-3 col-form-label control-label"><b>ID Item </b></label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="id_item" placeholder="ID Item" autocomplete="off" required readonly value="<?= $tb_req->id_item ?>" />
                                            </div>
                                        </div>

                                        <div class="form-group form-material row">
                                            <label class="col-md-3 col-form-label control-label"><b>Stock</b></label>
                                            <div class="col-md-6">
                                                <input type="text" readonly class="form-control" name="balance_Stock" placeholder="total_stock_in" autocomplete="off" required value="<?= $balance_stock ?>">
                                            </div>
                                        </div>

                                        <div class="form-group form-material row">
                                            <label class="col-md-3 col-form-label control-label"><b>Total Request </b></label>
                                            <div class="col-md-6">
                                                <input type="number" min="0" max="" step="1" class="form-control" name="total_request" placeholder="Stock IN" autocomplete="off" required value="<?= $tb_req->total_request ?>" />
                                            </div>
                                        </div>

                                        <div class="form-group form-material row">
                                            <label class="col-md-3 col-form-label control-label"><b>Date Request </b></label>
                                            <div class="col-md-6">
                                                <input class="form-control" type="date" value="<?php echo date('Y-m-d') ?>" readonly />
                                            </div>
                                        </div>

                                        <button type="submit" class="btn btn-sm btn-round btn-success" id="button1"><span class=" text">&nbsp;&nbsp;&nbsp;&nbsp;SAVE&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                        <!-- </form> -->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>