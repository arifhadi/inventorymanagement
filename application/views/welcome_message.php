<div class="container">
  <div class="row">
    <div class="col-12">
      <h2 style="text-align: center; font-style: sans-serif; font-weight: 900; color: white;">MENU INVENTORY MANAGEMENT</h2>
    </div>
    <?php if(check_permission_view(ID_GROUP,'read','Indirect')) { ?>
    <div class="col-sm">
      <div class="hexagon-wrapper">
        <div class="hexagon">
          <a href="<?php echo base_url('dashboard/indirect'); ?>" role="button" aria-pressed="true"><br><br>
            <i class="icon md-layers-off" aria-hidden="true" style="text-align: center;"><br><b><p style="font-size: 22px;">Indirect Material</p></b></i>
          </a>
        </div>
      </div>
    </div>
    <?php } ?>

    <?php if(check_permission_view(ID_GROUP,'read','autorization')) { ?>
    <div class="col-sm">
      <div class="hexagon-wrapper">
        <div class="hexagon">
          <a href="<?php echo base_url('user'); ?>" role="button" aria-pressed="true"><br><br>
            <i class="icon md-settings" aria-hidden="true" style="text-align: center;"><br><b><p style="font-size: 22px;">User Autorization</p></b></i>
          </a>
        </div>
      </div>
    </div>
    <?php } ?>

    <?php if(check_permission_view(ID_GROUP,'read','direct')) { ?>
    <div class="col-sm">
      <div class="hexagon-wrapper">
        <div class="hexagon">
          <a href="#" role="button" aria-pressed="true"><br><br>
            <i class="icon md-layers" aria-hidden="true" style="text-align: center;"><br><b><p style="font-size: 22px;">Direct Material</p></b></i>
          </a>
        </div>
      </div>
    </div>
    <?php } ?>
    
  </div>
</div>
<style type="text/css">
.container {
  width: 100%;
  height: 100vh;
  overflow: hidden;
  background: linear-gradient(18deg, #e37682, #a58fe9);
  display: flex;
  flex-wrap: wrap;
  justify-content: space-evenly;
  align-items: center;
}

.hexagon-wrapper {
  margin: auto;
  display: flex;
  text-align: initial;
  width: 200px;
  height: 200px;
  cursor: pointer;
}

.hexagon {
  position: relative;
  width: 46%;
  height: 80%;
  margin: auto;
  color: white;
  background: linear-gradient(-180deg, white, #fda3b2);
  display: flex;
  align-content: center;
  justify-content: center;
  transition: 0.5s;
}

.hexagon i {
  z-index: 1;
  margin: auto;
  font-size: 50px;
  color: transparent;
  background: linear-gradient(45deg, #a58fe9, #e37682);
  background-clip: text;
  -webkit-background-clip: text;
}

.hexagon:before,
.hexagon:after {
  position: absolute;
  content: "";
  background: inherit;
  height: 100%;
  width: 100%;
  border-radius: 0;
  transition: 0.5s;
  transform-origin: center;
}
.hexagon:before {
  transform: rotateZ(60deg);
}
.hexagon:after {
  transform: rotateZ(-60deg);
}
.hexagon:hover {
  border-radius: 50px;
  transition: 0.5s;
}
.hexagon:hover:before {
  border-radius: 50px;
  transition: 0.5s;
}
.hexagon:hover:after {
  border-radius: 50px;
  transition: 0.5s;
}
</style>