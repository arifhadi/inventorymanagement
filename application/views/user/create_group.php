<!-- Page -->
  <div class="page">
    <ol class="breadcrumb">
      <a href="<?php echo base_url('user/list_group'); ?>" type="button" class="btn btn-round btn-warning"><i class="icon md-format-indent-increase" aria-hidden="true"></i>Groups Master List</a>
    </ol>
    <div class="page-header" style="text-align: center; padding: 0px;">
      <h1 class="page-title">Create Groups</h1>
    </div>
    <div class="page-content">
      <div class="panel">
        <div class="panel-body container-fluid" style="padding: 0px;">
          <?php if ($this->session->flashdata('error')) { ?>
            <div class="alert alert-danger alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button><p><?php echo $this->session->flashdata('error'); ?></p>
            </div>
          <?php } ?>
          <div class="panel">
              <div class="panel-body container-fluid">
                <div class="row row-lg">
                  <div class="col-md-12 col-lg-6">
                    <!-- Example Horizontal Form -->
                    <div class="example-wrap">
                      <div class="example">
                        <!-- <form class="form-horizontal"> -->
                          <?= form_open(base_url('user/create_group'),  'id="login_validation" enctype="multipart/form-data"') ?>
                          
                          <div class="form-group row form-material row">
                            <label class="col-md-2 form-control-label">Departement<b style="color: red;">*</b> : </label>
                            <div class="col-md-6">
                              <select class="form-control" required="required" data-plugin="select2" id="departement" name="departement" data-placeholder="Select Departement" >
                                <option></option>
                                <?php foreach ($get_dept as $val) { ?>
                                  <option value="<?=$val->id;?>"><?=$val->name;?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-2 form-control-label"><b>Group Name</b><b style="color: red;">*</b> <b>:</b> </label>
                            <div class="col-md-6">
                              <input type="text"  class="form-control" name="name" placeholder="Group Name" autocomplete="off"/>
                            </div>
                          </div>

                      </div>
                    </div>
                    <!-- End Example Horizontal Form -->
                  </div>
                  <div class="col-md-12 col-lg-6">
                    <!-- Example Horizontal Form -->
                    <div class="example-wrap">
                      <div class="example">
                          
                          <div class="form-group row">
                            <label class="col-md-2 form-control-label"><b>Description : </b></label>
                            <div class="col-md-6">
                              <input type="text" class="form-control" name="description" placeholder="Description" autocomplete="off"/>
                            </div>
                          </div>

                      </div>
                    </div>
                    <!-- End Example Horizontal Form -->
                  </div>
                  <!-- Button Action -->
                    <div class="col-lg-5 form-group form-material">
                        <!-- <input type="text" class="form-control" placeholder=".col-lg-4"> -->
                    </div>
                    <div class="col-lg-5 form-group form-material">
                      <button type="Submit" class="btn btn-success btn-sm">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SAVE &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
                    </div>
                    <div class="col-lg-2 form-group form-material">
                      <!-- <input type="text" class="form-control" placeholder=".col-lg-4"> -->
                    </div>
                    <?php form_close() ?>
                  <!-- Button Action -->
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    <!-- End Page -->