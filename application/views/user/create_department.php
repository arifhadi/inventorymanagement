<style>
  .card-header {
    background-color: #36459b;
    text-align: center;
  }
  #title {
    padding-top: 5px;
    margin-bottom: 0;
    font-size: 23px;
  }
  #button1 {
    margin-top: 30px;
    left: 48%;
  }
  .control-label:after {
    content: "*";
    color: red;
  }
</style>
<div class="page">
  <div class="page-header">
    <div class="col-md-12">
      <!-- <a href="<?php echo base_url("user/list_group") ?>" type="button" class="btn btn-danger btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
        <span class="ladda-label"><i class="icon md-home mr-10" aria-hidden="true"></i>Home</span>
      </a> -->
      <a href="<?php echo base_url("user/list_group")?>" type="button" class="btn btn-info btn-round" data-style="slide-left" data-plugin="ladda" data-type="progress">
        <span class="ladda-label"><i class="icon md-undo" aria-hidden="true"></i>Back</span>
      </a>
    </div>
  </div>
  <div class="page-content container-fluid" style="padding: 0px;">
    <div class="row">
      <div class="col-lg-6">
        <div class="panel">
          <div class="panel-body">
            <?php if($this->session->flashdata('info')){?>
              <div class="alert dark alert-success alert-dismissible" role="alert" id="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button><?= $this->session->flashdata('info');?>
              </div>
            <?php }  elseif ($this->session->flashdata('danger')){?>
              <div class="alert dark alert-danger alert-dismissible" role="alert" id="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button><?= $this->session->flashdata('danger');?>
              </div>
            <?php }?>
              <?php echo form_open_multipart('user/create_department'); ?>
              <h3 Align="center">Form Create</h3><br>
              <form class="form-horizontal">
              <div class="form-group row">
                <label class="col-md-3 col-form-label control-label"><b>Name</b></label>
                <div class="col-md-9">
                  <input type="text" required="required" class="form-control" name="name" placeholder="Name" id="input" autocomplete="off" onkeypress="return event.charCode < 48 || event.charCode  >57"/>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label control-label"><b>Short Name</b></label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="short_name" id="short_name" placeholder="Short Name" autocomplete="off" required onkeypress="return event.charCode < 48 || event.charCode  >57"/>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label control-label"><b>Description</b></label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="description" placeholder="Description" required/>
                </div>
              </div>
              <div class="form-group form-material row">
                <button type="submit" class=" btn btn-success mt--10" id="button1">
                  <span class="text">&nbsp;&nbsp; SAVE &nbsp;&nbsp;</span>
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="col-lg-6">
        <div class="panel">
          <div class="panel-body">
            <h3 align="center">List Department</h3><br>
              <div class="row">
                <div class="col-md-12">
                </div>
              </div>
            <table class="table table-bordered table-hover table-striped" cellspacing="0" id="exampleTableSearch" data-mobile-responsive="true">
              <thead>
                <tr>
                  <th class="text-center">No.</th>
                  <th class="text-center">Name</th>
                  <th class="text-center">Short Name</th>
                  <th class="text-center">Description</th>
                </tr>
              </thead>
              <tbody> 
                <?php $no = 1; foreach($get_dept as $value){ ?>
                <tr>
                  <td class="text-center"><?= $no++ ?></td>
                  <td class="text-center"><?= $value->name ?></td>
                  <td class="text-center"><?= $value->short_name ?></td>
                  <td class="text-center"><?= $value->description ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<script>
  $(function() {
    $('#input').keyup(function() {
      this.value = this.value.toLocaleUpperCase();
    });
  });

  $(function() {
    $('#short_name').keyup(function() {
      this.value = this.value.toLocaleUpperCase();
    });
  });
</script> 