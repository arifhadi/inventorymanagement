<?php defined('BASEPATH') or exit('No direct script access allowed');?>

<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 5px;
        margin-bottom: 0;
        font-size: 30px;
    }

    .button {
        display: flex;
        color: white;
        justify-content: space-evenly;
    }

    .container {
        top: 50%;
    }

    #jan {
        color: white;
        background-color: blueviolet;
    }
</style>

    <div class="page">
        <div class="page-header" style="padding: 0px;"><br>
            <div class="col-md-12">
                <a href="<?php echo base_url("Dashboard/indirect") ?>" type="button" class="btn btn-danger btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
                    <span class="ladda-label"><i class="icon md-home mr-10" aria-hidden="true"></i>Home</span>
                </a>
            </div>
        </div>

        <h3 align="center">Request List</h3>

        <div class="container">
            <div class="container">
                <div class=" col-md-">
                    <div class="panel" style="padding: 0px;">
                        <div class="panel">
                            <div class="panel-body">
                                <table class="table table-hover dataTable table-striped w-full no-footer dtr-inline" cellspacing="0" id="exampleTableSearch">
                                    <thead>
                                        <tr>
                                            <th class="text-center">No.</th>
                                            <th class="text-center">Requestor</th>
                                            <th class="text-center">ID Item</th>
                                            <th class="text-center">Date Request</th>
                                            <th class="text-center">Total Request</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Request</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1; foreach ($request_list as $value) { ?>
                                            <tr>
                                                <td class="text-center"><?= $no++ ?></td>
                                                <td class="text-center"><?= $value->requestor ?></td>
                                                <td class="text-center"><?= $value->id_item ?></td>
                                                <td class="text-center"><?= $value->date_request ?></td>
                                                <td class="text-center"><?= $value->total_request ?></td>
                                                <td class="text-center">
                                                    <?php if ($value->status == 1) { ?>
                                                        <span class="badge badge-secondary" style="font-size:12px;" id="1">
                                                            Waiting Acceptance
                                                        </span>
                                                    <?php } elseif ($value->status == 2) { ?>
                                                        <span class="badge badge-success" style="font-size:12px;">Acceptance
                                                        </span>
                                                    <?php } elseif ($value->status == 3) { ?>
                                                        <span class="badge badge-danger" style="font-size:12px;">
                                                            Rejected
                                                        </span>
                                                    <?php } ?>
                                                </td>
                                                <td class="text-center">
                                                    <?php if ($value->status == 1) { ?>
                                                        <button type='submit' data-bind='<?= $value->id ?>' data-toggle="tooltip" title="Response" class='btn out_stock btn-floating btn-xs btn-warning'><i class="icon md-assignment-alert" aria-hidden="true" disable="disable"></i></button>
                                                    <?php }elseif($value->status == 2 or $value->status == 3) { ?>
                                                        <button type='submit' disabled data-toggle="tooltip" title="Responded" class='btn out_stock btn-floating btn-xs btn-warning'><i class="icon md-assignment-check" aria-hidden="true" disable="disable"></i></button>
                                                    <?php } ?>
                                                </td>
                                                <td class="button">
                                                    <?php if(check_permission_view(ID_GROUP,'read','approval')) { ?>
                                                        <button type='submit' data-bind='<?= $value->id ?>' data-toggle="tooltip" title="Detail" class='btn detail btn-floating btn-info btn-xs'><i class="icon md-collection-text" aria-hidden="true"></i></button>
                                                    <?php } ?>

                                                    <?php if(check_permission_view(ID_GROUP,'delete','approval')) { ?>
                                                        <button type='button' data-bind='<?= $value->id ?>' data-toggle="tooltip" title="Delete" class='btn nonactive btn-floating btn-danger btn-xs'><i class="icon md-icon md-delete" aria-hidden="true"></i></button>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Alert Btn Delete -->
    <script type="text/javascript">
        $(".nonactive").click(function() {
            var id = $(this).attr("data-bind");
            console.log(id);
            swal({
                    title: "Are you sure want to delete?",
                    // text: "you will change this data to Non-Active !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Delete !",
                    cancelButtonText: "Cancel !",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: '<?= base_url("request/hapus/") ?>' + id,
                            type: 'DELETE',
                            error: function() {
                                alert('Something is wrong');
                            },
                            success: function(data) {
                                $("#" + id).remove();
                                swal("Non-Active !", "Your Data is Delete", "success");
                                window.location.reload();
                            }
                        });
                    } else {
                        swal("Cancelled", "You Canceled To Delete", "error");
                    }
                });
        });
    </script>
    <!-- End Alert Button Delete -->

    <!-- Alert Button Detail -->
    <script type="text/javascript">
        //Change Data
        $(".detail").click(function() {
            var id = $(this).attr("data-bind");

            swal({
                    title: "Are you sure you want to see detail?",
                    text: "",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: "btn-info",
                    confirmButtonText: "Yes",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            // url: '<?= base_url("request/detail/") ?>'+id,
                            type: 'DELETE',
                            error: function() {
                                alert('Something is wrong');
                            },
                            success: function(data) {
                                $("#" + id).remove();
                                // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                                window.location.href = '<?= base_url("request/detail/") ?>' + id;
                            }
                        });
                    } else {
                        swal("Cancelled", "You Canceled To See Detail", "error");
                    }
                });
        });
    </script>
    <!-- End Alert Button Detail -->

    <!-- Alert Button Outstock -->
    <script type="text/javascript">
        //Change Data
        $(".out_stock").click(function() {
            var id = $(this).attr("data-bind");

            swal({
                    title: "Are you sure you want to response the request?",
                    text: "",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: "btn-warning",
                    confirmButtonText: "Yes",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            // url: '<?= base_url("request/form/") ?>'+id,
                            type: 'DELETE',
                            error: function() {
                                alert('Something is wrong');
                            },
                            success: function(data) {
                                $("#" + id).remove();
                                // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                                window.location.href = '<?= base_url("request/form/") ?>' + id;
                            }
                        });
                    } else {
                        swal("Cancelled", "You canceled to response the request", "error");
                    }
                });
        });
    </script>
    <!-- End Alert Button Outstock-->