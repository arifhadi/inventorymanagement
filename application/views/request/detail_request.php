<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 5px;
        margin-bottom: 0;
        font-size: 30px;
    }

    #button1 {
        margin-top: 30px;
        left: 48%;
    }

    .container {
        top: 50%;
    }

    .action {
        width: 200;
    }

    .control-label:after {
        content: "*";
        color: red;
    }

    .expand {
        height: 2em;
        width: 100%;
        padding: 5px;
        resize: none;
        overflow: hidden
    }

    textarea {
        height: 1em;
        width: 50%;
        padding: 3px;
        transition: all 0.5s ease;
    }

    textarea:focus {
        height: 6em;
        overflow-y: scroll;
    }

    #cs1 {
        margin-top: -70px;
    }

    #cs2 {
        margin-top: -85px;
    }
</style>

<div class="page">
    <div class="page-header" style="padding: 0px;"><br>
        <div class="col-md-12">
            <a href="<?php echo base_url("Dashboard/indirect") ?>" type="button" class="btn btn-danger btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
                <span class="ladda-label"><i class="icon md-home mr-10" aria-hidden="true"></i>Home</span>
            </a>
            <a href="<?php echo base_url("request") ?>" type="button" class="btn btn-info btn-round" data-style="slide-left" data-plugin="ladda" data-type="progress" id="back">
                <span class="ladda-label"><i class="icon md-undo" aria-hidden="true"></i>Back</span>
            </a>
        </div>
    </div>

    <h3 align="center">Form Detail</h3>

    <div class="page-content" style="padding: 0px;">
        <!-- button-->
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="panel-body container-fluid" align="left">
                                    <div class="row row-lg-12">
                                        <div class="col-md-12 col-lg-6" id="cs1">
                                            <div class="example-wrap">
                                                <div class="example">

                                                    <div class="form-group form-material row" hidden>
                                                        <label class="col-md-3 col-form-label control-label">ID </label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" name="id" placeholder="ID Item" autocomplete="off" required value="<?= $req_in->id ?>" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group form-material row">
                                                        <label class="col-md-3 col-form-label control-label"><b>Requestor</b></label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control" name="requestor" placeholder="ID Item" autocomplete="off" readonly required value="<?= $req_in->requestor ?>" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group form-material row">
                                                        <label class="col-md-3 col-form-label control-label"><b>Id Item</b></label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control" name="id_item" placeholder="Id Item" autocomplete="off" readonly required value="<?= $req_in->id_item ?>" />
                                                        </div>
                                                    </div>
                                                   
                                                    <div class="form-group form-material row">
                                                        <label class="col-md-3 col-form-label control-label"><b>Stock</b></label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control" name="id_item" placeholder="Id Item" autocomplete="off" readonly required value="<?= $balance_stock ?>" />
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group form-material row">
                                                        <label class="col-md-3 col-form-label control-label"><b>PIC</b></label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control" name="pic" placeholder="PIC" autocomplete="off" readonly required value="<?= $req_in->pic ?>" />
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-lg-6" id="cs1">
                                            <div class="example-wrap">
                                                <div class="example">

                                                    <div class="form-group form-material row">
                                                        <label class="col-md-3 col-form-label control-label"><b>Total Request</b></label>
                                                        <div class="col-md-6">
                                                            <input type="number" min="<?= $req_in->total_request ?>" max="100" step="1" class="form-control" name="total_request" placeholder="Request Stock" readonly autocomplete="off" required value="<?= $req_in->total_request ?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group form-material row">
                                                        <label class="col-md-3 col-form-label control-label"><b>Date Request</b></label>
                                                        <div class="col-md-6">
                                                            <input type="date" class="form-control" name="date_request" placeholder="Date Out" autocomplete="off" required readonly value="<?= $req_in->date_request ?>" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group form-material row">
                                                        <label class="col-md-3 col-form-label control-label"><b>Date Responded</b></label>
                                                        <div class="col-md-6">
                                                            <input type="date" class="form-control" name="date_request" placeholder="Date Out" autocomplete="off" required readonly value="<?= $req_in->date_responded ?>" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group form-material row">
                                                        <label class="col-md-4 col-form-label control-label"><b>Request Response</b></label>
                                                        <div class="col-md-7">
                                                            <?php
                                                            if ($req_in->status == 1) { ?>
                                                                <span class="badge badge-secondary" style="font-size: 12px;">
                                                                    Waiting Acceptance
                                                                </span>
                                                            <?php } elseif ($req_in->status == 2) { ?>
                                                                <input type="radio" value="2" name="status" checked readonly required dis>&ensp;<label class="badge badge-success" style="font-size: 12px;" for="flexRadioDefault1">Acceptance</label>
                                                                &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                                                                <input type="radio" value="3" name="status" required disabled>&ensp;<label class="badge badge-danger" style="font-size: 12px;" for="flexRadioDefault2">Rejected</label>
                                                            <?php } elseif ($req_in->status == 3) { ?>
                                                                <input type="radio" value="2" name="status" required disabled>&ensp;<label class="badge badge-success" style="font-size: 12px;" for="flexRadioDefault1">Acceptance</label>
                                                                &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                                                                <input type="radio" value="3" name="status" checked readonly required>&ensp;<label class="badge badge-danger" style="font-size: 15px;" for="flexRadioDefault2">Rejected</label>
                                                        </div>
                                                    </div>

                                                    <div>
                                                        &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;
                                                        <textarea class="expand" rows="1" cols="10" name="reason" readonly><?= $req_in->reason ?></textarea>
                                                    </div>
                                                <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>                           
<script>
    function showTextArea() {
        document.getElementById('textAreaDiv').style.visibility = "visible";
    }

    function hideTextArea() {
        document.getElementById('textAreaDiv').style.visibility = "hidden";
    }
</script>