<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 5px;
        margin-bottom: 0;
        font-size: 30px;
    }

    #button1 {
        margin-top: 30px;
        left: 48%;
    }

    .container {
        top: 50%;
    }

    .action {
        width: 200;
    }

    .control-label:after {
        content: "*";
        color: red;
    }

    .expand {
        height: 2em;
        width: 100%;
        padding: 5px;
        resize: none;
        overflow: hidden
    }

    textarea {
        height: 1em;
        width: 50%;
        padding: 3px;
        transition: all 0.5s ease;
    }

    textarea:focus {
        height: 6em;
        overflow-y: scroll;
    }

    #cs1 {
        margin-top: -70px;
    }

    #cs2 {
        margin-top: -85px;
    }
</style>

<script>
    $(document).ready(function() {
        $("#flexRadioDefault1").click(function() {
            $("textarea").hide();
            $("textarea").attr('required', false);
        });
        $("#flexRadioDefault2").click(function() {
            $("textarea").show();
            $("textarea").attr('required', true);
        });
    });
</script>

<div class="page">
    <div class="page-header" style="padding: 0px;"><br>
        <div class="col-md-12">
            <a href="<?php echo base_url("Dashboard/indirect") ?>" type="button" class="btn btn-danger btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
                <span class="ladda-label"><i class="icon md-home mr-10" aria-hidden="true"></i>Home</span>
            </a>
            <a href="<?php echo base_url("request") ?>" type="button" class="btn btn-info btn-round" data-style="slide-left" data-plugin="ladda" data-type="progress" id="back">
                <span class="ladda-label"><i class="icon md-undo" aria-hidden="true"></i>Back</span>
            </a>
        </div>
    </div>

    <h3 align="center">Form Response</h3>

    <div class="page-content" style="padding: 0px;">
        <div class="col-md-12">
            <div class="panel">
                <!-- allert -->
                <?php if ($this->session->flashdata('info')) { ?>
                    <div class="alert dark alert-success alert-dismissible" role="alert" id="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?= $this->session->flashdata('info'); ?>
                    </div>
                <?php } ?>
                <div class="panel-body">
                    <div class="panel">
                        <div class="panel-body">
                            <form method="POST" action="<?php echo base_url('request/save_request'); ?>">
                                <div class="panel-body container-fluid" align="left">
                                    <div class="row row-lg-12">
                                        <div class="col-md-12 col-lg-6" id="cs1">
                                            <div class="example-wrap">
                                                <div class="example">

                                                    <div class="form-group form-material row" hidden>
                                                        <label class="col-md-3 col-form-label control-label">ID </label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" name="id" placeholder="ID Item" autocomplete="off" required value="<?= $req_in->id ?>" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group form-material row">
                                                        <label class="col-md-3 col-form-label control-label"><b>Requestor</b></label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control" name="requestor" placeholder="ID Item" autocomplete="off" readonly required value="<?= $req_in->requestor ?>" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group form-material row">
                                                        <label class="col-md-3 col-form-label control-label"><b>Id Item</b></label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control" name="id_item" placeholder="Id Item" autocomplete="off" readonly required value="<?= $req_in->id_item ?>" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group form-material row">
                                                        <label class="col-md-3 col-form-label control-label"><b>Stock</b></label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control" name="" placeholder="Id Item" autocomplete="off" readonly required value="<?= $balance_stock ?>" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group form-material row">
                                                        <label class="col-md-3 col-form-label control-label"><b>PIC</b></label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control" name="pic" placeholder="PIC" autocomplete="off" required value="<?= USER_NAME ?>" />
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-lg-6" id="cs1">
                                            <div class="example-wrap">
                                                <div class="example">

                                                    <div class="form-group form-material row">
                                                        <label class="col-md-3 col-form-label control-label"><b>Total Request</b></label>
                                                        <div class="col-md-6">
                                                            <input type="number" min="<?= $req_in->total_request ?>" max="100" step="1" class="form-control" name="total_request" readonly placeholder="Request Stock" autocomplete="off" required value="<?= $req_in->total_request ?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group form-material row">
                                                        <label class="col-md-3 col-form-label control-label"><b>Date Request</b></label>
                                                        <div class="col-md-6">
                                                            <input type="date" class="form-control" name="date_request" placeholder="Date Out" autocomplete="off" required readonly value="<?= $req_in->date_request ?>" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group form-material row" hidden>
                                                        <label class="col-md-3 col-form-label control-label"><b>Date Responded</b></label>
                                                        <div class="col-md-6">
                                                            <input class="form-control" type="date_responded" value="<?php echo date('Y-m-d'); ?>" readonly />
                                                        </div>
                                                    </div>

                                                    <div class="form-group form-material row">
                                                        <label class="col-md-4 col-form-label control-label"><b>Request Response</b></label>
                                                        <div class="col-md-7">
                                                            <?php if ($balance_stock < $req_in->total_request or $balance_stock == 0) { ?>
                                                                <input type="radio" name="status" disabled>&ensp;<label class="badge badge-success" style="font-size: 12px;" for="flexRadioDefault1">Acceptance</label>
                                                                &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                                                                <input type="radio" value="3" class="tesb" name="status" id="flexRadioDefault2" required>&ensp;<label class="badge badge-danger" style="font-size: 12px;" for="flexRadioDefault2">Rejected</label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label"></label>
                                                        <div class="col-md-6">
                                                            <div id="textAreaDiv">
                                                                <textarea class="expand" rows="1" cols="10" placeholder="Give Your Reasons!" name="reason"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   <!--  <div id="textAreaDiv">
                                                        <textarea hidden class=" expand" required rows="1" cols="10" placeholder="Give Your Reasons!" name="reason"></textarea>
                                                    </div> -->

                                                <?php } elseif ($balance_stock > $req_in->total_request or $balance_stock == $req_in->total_request) { ?>
                                                    <input type="radio" value="2" name="status" id="flexRadioDefault1" required>&ensp;<label class="badge badge-success" style="font-size: 12px;" for="flexRadioDefault1">Acceptance</label>
                                                    &emsp;&emsp;
                                                    <input type="radio" value="3" name="status" id="flexRadioDefault2" required>&ensp;<label class="badge badge-danger" style="font-size: 12px;" for="flexRadioDefault2">Rejected</label>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label"></label>
                                                <div class="col-md-6">
                                                    <div id="textAreaDiv">
                                                        <textarea class="expand" rows="1" cols="10" placeholder="Give Your Reasons!" name="reason"></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>

                                <!-- button -->
                                <div class="card-body" id="cs2" align="center">
                                    <button type="submit" class="btn btn btn-round btn-success">
                                        <span class=" text">&emsp;Save&emsp;</span>
                                    </button>
                                </div>
                        </div>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>