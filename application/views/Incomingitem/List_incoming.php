<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 0px;
        margin-bottom: 0;
        font-size: 30px;
    }

    #button1 {
        margin-top: 30px;
        left: 48%;
    }

    .container {
        top: 50%;
    }

    .action {
        width: 200;
    }
</style>

<div class="page">
    <div class="page-header" style="padding: 0px;"><br>
        <div class="col-md-12">
            <!-- button-->
            <a href="<?php echo base_url("Dashboard/indirect") ?>" type="button" class="btn btn-danger btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
                <span class="ladda-label"><i class="icon md-home mr-10" aria-hidden="true"></i>Home</span>
            </a>
        </div>
    </div>

    <h3 align="center">Tabel Incoming Item</h3>

    <div class="page-content" style="padding: 0px;">
        <!-- button-->
        <div class="col-md-12">
            <div class="px-4 bg-light "><marquee class="py-3"><b>Lates Updated By : <?=$name_user?>, <?=$date_last_update?></b></marquee></div>
            <div class="panel">
                <!-- allert -->
                <?php if ($this->session->flashdata('info')) { ?>
                    <div class="alert dark alert-success alert-dismissible" role="alert" id="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?= $this->session->flashdata('info'); ?>
                    </div>
                <?php } ?>
                <div class="panel-body">
                    <div class="panel">
                        <div class="panel-body">
                            <table class="table table-hover dataTable table-striped w-full no-footer dtr-inline" cellspacing="0" id="exampleTableSearch">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="20">No.</th>
                                        <th class="text-center">Id Item</th>
                                        <th class="text-center">Name</th>
                                        <th class="text-center">Unit</th>
                                        <th class="text-center">Opening</th>
                                        <th class="text-center">Total Stock In</th>
                                        <th class="text-center">Balance Stock</th>
                                        <th class="text-center">Add Stock</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; foreach ($status as $value) { ?>
                                        <tr align="center">
                                            <td><?= $no++ ?></td>
                                            <td><?= $value->id_item ?></td>
                                            <td><?= $value->name ?></td>
                                            <td><?= $value->unit ?></td>
                                            <td><?= $value->opening ?></td>
                                            <td><?= $value->total_stock_in ?></td>
                                            <td><?= $value->balance_stock ?></td>
                                            <td>
                                                <?php if(check_permission_view(ID_GROUP,'update','incoming')) { ?>
                                                    <button type='submit' data-bind='<?= $value->id_item ?>' data-toggle="tooltip" title="Add Stock" class='btn btn-success add_stock btn btn-floating btn-warning btn-xs' title="Add Stock"><i class="icon md-plus-circle" aria-hidden="true"></i></button>
                                                <?php } ?>
                                            </td>
                                            <td class="actions" width='150' align="center">
                                                <?php if(check_permission_view(ID_GROUP,'read','incoming')) { ?>
                                                    <button type='submit' data-bind='<?= $value->id_item ?>' data-toggle="tooltip" title="Detail" class='btn btn-success detail btn btn-floating btn-info btn-xs'><i class="icon md-assignment-check" aria-hidden="true"></i></button>
                                                <?php } ?>
                                               
                                                <?php if(check_permission_view(ID_GROUP,'update','incoming')) { ?>
                                                    <button type='submit' data-bind='<?= $value->id_item ?>' data-toggle="tooltip" title="Edit" class='btn btn-success edit btn btn-floating btn-success btn-xs'><i class="icon md-icon md-edit" aria-hidden="true"></i></button>
                                                <?php } ?>
                                             
                                                <?php if(check_permission_view(ID_GROUP,'delete','incoming')) { ?>
                                                    <button type='button' data-bind='<?= $value->id_item ?>' data-toggle="tooltip" title="Delete" class='btn btn-success nonactive btn btn-floating btn-danger btn-xs'><i class="icon md-icon md-delete" aria-hidden="true"></i></button>
                                                <?php } ?>
                                            </td>
                                        <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- SweetAlert Delete -->
<script type="text/javascript">
    //delete
    $(".nonactive").click(function() {
        var id = $(this).attr("data-bind");
        console.log(id);
        swal({
                title: "Are you sure want to delete?",
                // text: "you will change this data to Non-Active !",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Delete !",
                cancelButtonText: "Cancel !",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: '<?= base_url("incoming_item/hapus/") ?>' + id,
                        type: 'DELETE',
                        error: function() {
                            alert('Something is wrong');
                        },
                        success: function(data) {
                            $("#" + id).remove();
                            swal("Non-Active !", "Your Data is Delete", "success");
                            window.location.reload();
                        }
                    });
                } else {
                    swal("Cancelled", "You Canceled To Delete :)", "error");
                }
            });
    });
</script>

<!-- SweetAlert Detail -->
<script type="text/javascript">
    //Change Data
    $(".detail").click(function() {
        var id = $(this).attr("data-bind");

        swal({
                title: "Are you sure you want to see detail?",
                text: "",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: "btn-info",
                confirmButtonText: "Yes !",
                cancelButtonText: "Cancel !",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        // url: '<?= base_url("incoming_item/detail/") ?>'+id,
                        type: 'DELETE',
                        error: function() {
                            alert('Something is wrong');
                        },
                        success: function(data) {
                            $("#" + id).remove();
                            // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            window.location.href = '<?= base_url("incoming_item/detail/") ?>' + id;
                        }
                    });
                } else {
                    swal("Cancelled", "You Canceled To See Detail :)", "error");
                }
            });
    });
</script>

<!-- SweetAlert Edit -->
<script type="text/javascript">
    //Change Data
    $(".edit").click(function() {
        var id = $(this).attr("data-bind");

        swal({
                title: "Are you sure you want to change the data?",
                text: "",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Yes !",
                cancelButtonText: "Cancel !",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        // url: '<?= base_url("incoming_item/edit/") ?>'+id,
                        type: 'DELETE',
                        error: function() {
                            alert('Something is wrong');
                        },
                        success: function(data) {
                            $("#" + id).remove();
                            // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            window.location.href = '<?= base_url("incoming_item/edit/") ?>' + id;
                        }
                    });
                } else {
                    swal("Cancelled", "You Canceled To Edit :)", "error");
                }
            });
    });
</script>

<!-- SweetAlert Add Stock -->
<script type="text/javascript">
    //Change Data
    $(".add_stock").click(function() {
        var id = $(this).attr("data-bind");

        swal({
                title: "Are you sure you want to add stock?",
                text: "",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: "btn-warning",
                confirmButtonText: "Yes !",
                cancelButtonText: "Cancel !",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        // url: '<?= base_url("incoming_item/form/") ?>'+id,
                        type: 'DELETE',
                        error: function() {
                            alert('Something is wrong');
                        },
                        success: function(data) {
                            $("#" + id).remove();
                            // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            window.location.href = '<?= base_url("incoming_item/form/") ?>' + id;
                        }
                    });
                } else {
                    swal("Cancelled", "You Canceled To Add Stock :)", "error");
                }
            });
    });
</script>