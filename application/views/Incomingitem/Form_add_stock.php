<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 5px;
        margin-bottom: 0;
        font-size: 23px;
    }

    #button1 {
        margin-top: 30px;
        left: 48%;
    }

    .control-label:after {
        content: "*";
        color: red;
    }
</style>

<div class="page">
    <div class="page-header" style="padding: 0px;"><br>
        <div class="col-md-12">
            <!-- button-->
            <a href="<?php echo base_url("Dashboard/indirect") ?>" type="button" class="btn btn-danger btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
                <span class="ladda-label"><i class="icon md-home mr-10" aria-hidden="true"></i>Home</span>
            </a>
            <a href="<?php echo base_url("Incoming_item") ?>" type="button" class="btn btn-info btn-round" data-style="slide-left" data-plugin="ladda" data-type="progress" id="back">
                <span class="ladda-label"><i class="icon md-undo" aria-hidden="true"></i>Back</span>
            </a>
        </div>
    </div>

    <h3 align="center">Add Stock</h3>
    <!-- <div class="page-content"> -->
    <div class="page-content container-fluid" style="padding: 0px;">
        <div class="row">
            <div class="col-lg-5">
                <!-- <h1 align="center">Form Add Stock</h1> -->
                <!-- Panel Basic -->
                <div class="panel">
                    <div class="panel-body">
                        <!-- allert -->
                        <?php if ($this->session->flashdata('info')) { ?>
                            <div class="alert dark alert-success alert-dismissible" role="alert" id="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <?= $this->session->flashdata('info'); ?>
                            </div>
                        <?php } ?>
                        <?php echo form_open_multipart('incoming_item/save_add_stock'); ?>
                        <h4 align="center">Form Add Stock</h4><br>

                            <div class="form-group form-material row">
                                <label class="col-md-3 col-form-label control-label"><b>PIC </b></label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="pic" placeholder="PIC" autocomplete="off" required value="<?= USER_NAME ?>" readonly />
                                </div>
                            </div>

                            <div class="form-group form-material row">
                                <label class="col-md-3 col-form-label control-label"><b>ID Item </b></label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="id_item" placeholder="ID Item" autocomplete="off" required value="<?= $id_item ?>" readonly />
                                </div>
                            </div>

                            <div class="form-group form-material row">
                                <label class="col-md-3 col-form-label control-label"><b>Opening </b></label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="opening" placeholder="opening" autocomplete="off" required value="<?= $opening  ?>" readonly />
                                </div>
                            </div>

                            <div class="form-group form-material row">
                                <label class="col-md-3 col-form-label control-label"><b>Stock IN </b></label>
                                <div class="col-md-5">
                                    <input type="number" min="0" max="" step="1" class="form-control" name="stock_in" placeholder="Stock IN" autocomplete="off" required />
                                </div>
                            </div>

                            <div class="form-group form-material row">
                                <label class="col-md-3 col-form-label control-label"><b>Date IN </b></label>
                                <div class="col-md-5">
                                    <input class="form-control" type="date" value="<?php echo date('Y-m-d') ?>" readonly />
                                </div>
                            </div>

                            <div class="form-group form-material row">
                                <button type="submit" class="btn btn-md btn-round btn-success" id="button1">
                                    <span class=" text">&emsp;SAVE&emsp;</span>
                                </button>
                            </div>
                            <?php echo form_close(); ?>
                    </div>
                </div>
            </div>

            <div class="col-lg-7">
                <!-- Panel Extended -->
                <div class="panel">
                    <div class="panel-body">
                        <h4 align="center">List Stock In</h4><br>
                        <div class="row">
                            <div class="col-md-12">
                            </div>
                        </div>
                        <table class="table table-bordered table-hover table-striped" cellspacing="0" id="exampleTableSearch">
                            <thead>
                                <tr>
                                    <th class="text-center" width="20">No.</th>
                                    <th class="text-center" width="50">PIC</th>
                                    <th class="text-center" width="50">ID Item</th>
                                    <th class="text-center" width="50">Date IN</th>
                                    <th class="text-center" width="50">Stock IN</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1;
                                foreach ($stock_list as $value) { ?>
                                    <tr>
                                        <td class="text-center"><?= $no++ ?></td>
                                        <td class="text-center"><?= $value->pic ?></td>
                                        <td class="text-center"><?= $value->id_item ?></td>
                                        <td class="text-center"><?= $value->date_in ?></td>
                                        <td class="text-center"><?= $value->stock_in ?></td>
                                    <?php } ?>
                            </tbody>
                            </tbody>
                            <tr>
                                <td class="text-center" colspan="4"><b>Total<b></td>
                                <td class="text-center" style="color:red"><?= $total_list->stock_in ?></td>
                                <td hidden></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- End Panel Extended -->