<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 5px;
        margin-bottom: 0;
        font-size: 23px;
    }

    #button1 {
        margin-top: 30px;
        left: 48%;
    }

    .control-label:after {
        content: "*";
        color: red;
    }
</style>

<div class="page">
    <div class="page-header" style="padding: 0px;"><br>
        <div class="col-md-12">
            <!-- button-->
            <a href="<?php echo base_url("Incoming_Item/edit/" . $p_edit->id_item) ?>" type="button" class="btn btn-info btn-round" data-style="slide-left" data-plugin="ladda" data-type="progress" id="back">
                <span class="ladda-label"><i class="icon md-undo" aria-hidden="true"></i>Back</span>
            </a>
        </div>
    </div>

    <h3 align="center">Form Edit Stock</h3>

    <div class="page-content">
        <div class="col-md-12" style="padding: 0px;">
            <div class="panel">
                <div class="panel-body">
                    <!-- allert -->
                    <?php if ($this->session->flashdata('info')) { ?>
                        <div class="alert dark alert-success alert-dismissible" role="alert" id="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?= $this->session->flashdata('info'); ?>
                        </div>
                    <?php } ?>
                    <div class="panel">
                        <div class="panel-body">
                            <?php echo form_open_multipart('incoming_item/update_edit/'); ?>
                            <div class="row row-lg-12">
                                <div class="col-md-12 col-lg-3">
                                    <div class="example-wrap">
                                        <div class="example"></div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-lg-6">
                                    <div class="example-wrap">
                                        <div class="example">
                                            <div class="form-group form-material row" hidden>
                                                <label class="col-md-3 col-form-label control-label"><b> Id </b></label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="id" placeholder="id" autocomplete="off" required value="<?= $p_edit->id ?>" />
                                                </div>
                                            </div>

                                            <div class="form-group form-material row">
                                                <label class="col-md-3 col-form-label control-label"><b> PIC </b></label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="pic" placeholder="PIC" autocomplete="off" required readonly value="<?= $p_edit->pic ?>" />
                                                </div>
                                            </div>

                                            <div class="form-group form-material row">
                                                <label class="col-md-3 col-form-label control-label"><b>ID Item </b></label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" name="id_item" placeholder="ID Item" autocomplete="off" required readonly value="<?= $p_edit->id_item ?>" />
                                                </div>
                                            </div>

                                            <div class="form-group form-material row">
                                                <label class="col-md-3 col-form-label control-label"><b>Total Stock In</b></label>
                                                <div class="col-md-9">
                                                    <input type="text" readonly class="form-control" name="total_stock_in" placeholder="total_stock_in" autocomplete="off" required readonly value="<?= $total_stock_in ?>" />
                                                </div>
                                            </div>

                                            <div class="form-group form-material row">
                                                <label class="col-md-3 col-form-label control-label"><b>Stock IN </b></label>
                                                <div class="col-md-9">
                                                    <input type="number" min="<?= $p_edit->stock_in ?>" max="" step="1" class="form-control" name="stock_in" placeholder="Stock IN" autocomplete="off" required value="<?= $p_edit->stock_in ?>" />
                                                </div>
                                            </div>

                                            <div class="form-group form-material row">
                                                <label class="col-md-3 col-form-label control-label"><b> Date IN </b></label>
                                                <div class="col-md-9">
                                                    <input class="form-control" type="date" value="<?php echo date('Y-m-d') ?>" readonly />
                                                </div>
                                            </div>

                                            <button type="submit" class="btn btn-md btn-round btn-success" id="button1">
                                                <span class="text">&emsp;SAVE&emsp;</span>
                                            </button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                            <!-- </form> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>