<style>
    .card-header {
        background-color: #36459b;
        text-align: center;
    }

    #title {
        padding-top: 5px;
        margin-bottom: 0;
        font-size: 30px;
    }

    #button1 {
        margin-top: 30px;
        left: 48%;
    }

    .container {
        top: 50%;
    }

    #jan {
        color: white;
        background-color: blueviolet;
    }
</style>

<!-- button-->
<div class="page">
    <div class="page-header" style="padding: 0px;"><br>
        <a href="<?php echo base_url("Dashboard/indirect") ?>" type="button" class="btn btn-danger btn-round" data-style="expand-left" data-plugin="ladda" data-type="progress" id="home">
            <span class="ladda-label"><i class="icon md-home mr-10" aria-hidden="true"></i>Home</span>
        </a>
        <a href="<?php echo base_url("Incoming_item") ?>" type="button" class="btn btn-info btn-round" data-style="slide-left" data-plugin="ladda" data-type="progress" id="back">
            <span class="ladda-label"><i class="icon md-undo" aria-hidden="true"></i>Back</span>
        </a>
    </div>
    <h3 align="center">List Stock</h3>

    <div class="page-content" style="padding: 0px;">
        <!-- button-->
        <div class=" col-md-12">
            <div class="panel" >
                <div class="panel-body">
                    <!-- allert -->
                    <?php if ($this->session->flashdata('info')) { ?>
                        <div class="alert dark alert-success alert-dismissible" role="alert" id="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?= $this->session->flashdata('info'); ?>
                        </div>
                    <?php } ?>
                    <!-- Tabel List -->
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                </div>
                            </div>
                            <table class="table table-bordered table-hover table-striped" cellspacing="0" id="exampleTableSearch">
                                <thead>
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th class="text-center">PIC</th>
                                        <th class="text-center">ID Item</th>
                                        <th class="text-center">Date IN</th>
                                        <th class="text-center">Stock IN</th>
                                        <th class="text-center" width="120px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; foreach ($stock_list as $value) { ?> 
                                        <tr>
                                            <td class="text-center"><?= $no++ ?></td>
                                            <td class="text-center"><?= $value->pic ?></td>
                                            <td class="text-center"><?= $value->id_item ?></td>
                                            <td class="text-center"><?= $value->date_in ?></td>
                                            <td class="text-center"><?= $value->stock_in ?></td>
                                            <td class="text-center" width='100'>
                                                <?php if(check_permission_view(ID_GROUP,'update','incoming')) { ?>
                                                    <button type='submit' data-bind='<?= $value->id ?>' data-toggle="tooltip" title="Edit" class='btn btn-success edit_stock btn btn-floating btn-success btn-xs'><i class="icon md-icon md-edit" aria-hidden="true"></i></button>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                                <tr>
                                    <td class="text-center" colspan="4"><b>Total<b></td>
                                    <td class="text-center" style="color:red"><?= $total_list->stock_in ?></td>
                                    <td hidden></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- SweetAlert Edit -->
<script type="text/javascript">
    //Change Data
    $(".edit_stock").click(function() {
        var id = $(this).attr("data-bind");

        swal({
            title: "Are you sure you want to edit stock?",
            text: "",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Yes !",
            cancelButtonText: "Cancel !",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    // url: '<?= base_url("incoming_item/edit_form/") ?>'+id,
                    type: 'DELETE',
                    error: function() {
                        alert('Something is wrong');
                    },
                    success: function(data) {
                        $("#" + id).remove();
                            // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            window.location.href = '<?= base_url("incoming_item/edit_form/") ?>' + id;
                    }
                });
            } else {
                swal("Cancelled", "You Canceled To Edit :)", "error");
            }
        });
    });
</script>