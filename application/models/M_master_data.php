<?php

class M_master_data extends CI_Model
{

	// function tampil_data(){
	// 	return $this->db->get('user');
	// }

	// insert category 
	function insert_category($data)
	{
		$this->db->insert('tb_category', $data);
	}

	// insert item
	function insert_add_item($data)
	{
		$this->db->insert('tb_item', $data);
	}

	// tb stock in
	public function insert_tb_stock($data_stock)
	{
		$this->db->insert('tb_stock', $data_stock);
	}

	// category
	function get_category()
	{
		$this->db->select('*');
		$this->db->from('tb_category');
		return $this->db->get()->result();
	}

	function get_customers()
	{
		$this->db->select('*');
		$this->db->from('tb_customers');
		return $this->db->get()->result();
	}

	function get_supplier()
	{
		$this->db->select('*');
		$this->db->from('tb_supplier');
		return $this->db->get()->result();
	}

	// master data & category
	public function get_master_data()
	{
		$this->db->select('tb_item.*,tb_category.*');
		$this->db->from('tb_category');
		$this->db->join('tb_item' , 'tb_category.id_category = tb_item.id_category');
		return $this->db->get()->result();
	}

	public function get_lates_update()
	{
		$this->db->select('*');
		$this->db->from('tb_item');
		$this->db->order_by('last_update', 'DESC');
		return $this->db->get()->result();
	}

	public function get_user($user_id)
	{
		$this->db->select('*');
		$this->db->from('users');
		// $this->db->join('employees', 'employees.employee_no = users.employee_no', 'left');
		$this->db->where('id', $user_id);
		return $this->db->get()->row();
	}

	// detail
	public function detail($id_item)
	{
		// log_r($id_item);
		$this->db->select('tb_item.*,tb_category.*');
		$this->db->from('tb_category');
		$this->db->join('tb_item' , 'tb_category.id_category = tb_item.id_category');
		$this->db->where('id_item', $id_item);

		return $this->db->get()->row();
	}


	// delete
	public function hapus_data($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

	// edit item
	public function edit($id_item)
	{
		$this->db->select('*');
		$this->db->from('tb_item');
		$this->db->where('id_item', $id_item);

		return $this->db->get()->row();
	}

	// update_add_item
	function update_add_item($data,$id_item )
	{
		$this->db->where('id_item', $id_item);
		$this->db->update('tb_item', $data);
	}

	//edit category
	public function edit_category($id_category)
	{
		$this->db->select('*');
		$this->db->from('tb_category');
		$this->db->where('id_category', $id_category);

		return $this->db->get()->row();
	}

	// update_category
	function update_category($data, $id_category)
	{
		$this->db->where('id_category', $id_category);
		$this->db->update('tb_category', $data);
	}

	public function data_customer()
	{
		$this->db->select('*');
		$this->db->from('tb_customers');
		return $this->db->get()->result();
	}

	public function data_customer_row($id='')
	{
		$this->db->select('*');
		$this->db->from('tb_customers');
		$this->db->where('id_customer', $id);
		return $this->db->get()->row();
	}

	public function cek_id_customer($id_cust)
	{
		return $this->db->get_where('tb_customers', array('id_customer' => $id_cust))->num_rows();
	}

	function insert_customer($data)
	{
		return $this->db->insert('tb_customers', $data);
	}

	function update_customer($data, $id)
	{
		$this->db->where('id_customer', $id);
		$this->db->update('tb_customers', $data);
	}

	public function data_supplier()
	{
		$this->db->select('*');
		$this->db->from('tb_supplier');
		return $this->db->get()->result();
	}

	function cek_id_supplier($id_supp)
	{
		return $this->db->get_where('tb_supplier', array('id_supplier' => $id_supp))->num_rows();
	}

	function insert_supplier($data)
	{
		return $this->db->insert('tb_supplier', $data);
	}

	function detail_supplier_row($id='')
	{
		$this->db->select('*');
		$this->db->from('tb_supplier');
		$this->db->where('id_supplier', $id);
		return $this->db->get()->row();
	}

	function update_supplier($data, $id_supp)
	{
		$this->db->where('id_supplier', $id_supp);
		$this->db->update('tb_supplier', $data);
	}

	// Memanggil stock
	public  function call_stock($id_item)
    {
        $this->db->select('*');
        $this->db->from('tb_stock');
        $this->db->where('id_item', $id_item);
        return $this->db->get()->row();
    }

	// update stock 
	public function update_stock($id_item, $stock)
	{
		// log_r($id_item, $stock);
		$this->db->set($stock);
		$this->db->where('id_item', $id_item);
        $this->db->update('tb_stock');
	}

	public function count_opening($id_item)
    {
        $this->db->select_sum('opening');
        $this->db->from('tb_item');
        $this->db->where('id_item', $id_item);
        return $this->db->get()->row();
    }

	public function cek_id_item($angka_item)
	{
		return $this->db->get_where('tb_item', array('id_item' => $angka_item))->num_rows();
	}

	public function get_id_item($id_category)
	{
		$this->db->select('id_item');
        $this->db->from('tb_item');
        $this->db->where('id_category', $id_category);
        return $this->db->get()->result();
	}

	//update non active
	function non_active($id_item)
	{
		$this->db->set('status',2);
		$this->db->where('id_item', $id_item);
		$this->db->update('tb_item');
	}

	//update active
	function active($id_item)
	{
		$this->db->set('status',1);
		$this->db->where('id_item', $id_item);
		$this->db->update('tb_item');
	}
}
