<?php 

class M_auto_rezition extends CI_Model 
{
    public function employee()
    {
        $this->db->select('*');
        $this->db->from('employees');
        return $this->db->get()->result();
    }
    public function get_data_groups()
    {
        $this->db->select('*');
        $this->db->from('groups');
		return $this->db->get()->result();
    }
}