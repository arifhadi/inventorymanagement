<?php

class M_requestor extends CI_Model
{

    //Query Index
    //Get Request List
    public function get_request()
    {
        $this->db->select('tb_stock.balance_stock,tb_item.id_item,tb_item.name,tb_item.unit,tb_item.status, SUM(tb_request.total_request) as tot_request');
        $this->db->from('tb_stock');
        $this->db->where('tb_item.status = 1');
        $this->db->join('tb_item', 'tb_item.id_item = tb_stock.id_item', 'left');
        $this->db->join('tb_request', 'tb_request.id_item = tb_stock.id_item', 'left');
        $this->db->group_by('tb_item.id_item');
        return $this->db->get()->result();
    }

    //Query Form
    #Get Req In
    function get_req_in($id_item)
    {
        $this->db->select('balance_stock');
        $this->db->from('tb_stock');
        $this->db->where('id_item', $id_item);
        return $this->db->get()->row();
    }

    #Get request List
    public function get_request_list($id_item)
    {
        $this->db->select('*');
        $this->db->from('tb_request');
        $this->db->where('id_item', $id_item);
        return $this->db->get()->result();
    }

    #Total Request
    public function total_request($id_item)
    {
        $this->db->select_sum('total_request');
        $this->db->from('tb_request');
        $this->db->where('id_item', $id_item);
        return $this->db->get()->row();
    }

    #manggil balance
    public  function call_balance($id_item)
    {
        $this->db->select('balance_stock');
        $this->db->from('tb_stock');
        $this->db->where('id_item', $id_item);
        return $this->db->get()->row();
    }

    //Query Insert
    function insert_request($data)
    {
        $this->db->insert('tb_request', $data);
    }

    //Query Detail
    public function detail($id_item)
    {
        $this->db->select('*');
        $this->db->from('tb_request');
        $this->db->where('id_item', $id_item);

        return $this->db->get()->result();
    }

    function more_dtl($id)
    {
        $this->db->select('tb_request.*,tb_stock.id_item,tb_stock.balance_stock');
        $this->db->from('tb_stock');
        $this->db->where('tb_request.id', $id);
        $this->db->join('tb_request', 'tb_stock.id_item = tb_request.id_item');
        return $this->db->get()->row();
    }

    public function get_tb_req($id)
    {
        $this->db->select('*');
        $this->db->from('tb_request');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function get_stock_edit($id)
    {
        $this->db->select('tb_request.*,tb_stock.id_item,tb_stock.balance_stock');
        $this->db->from('tb_stock');
        $this->db->where('tb_request.id', $id);
        $this->db->join('tb_request', 'tb_stock.id_item = tb_request.id_item');

        return $this->db->get()->row();
    }

    public function update_request($id, $data)
    {
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('tb_request');
    }
}
