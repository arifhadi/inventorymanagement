<?php

class M_itemout extends CI_Model
{

	//index
	public function get_total_stock_out()
	{
		$this->db->select('tb_stock.id,tb_stock.id_item,tb_item.id_category,tb_item.name,tb_item.unit,tb_stock.total_stock_in,tb_stock.total_stock_out,tb_stock.balance_stock');
		$this->db->from('tb_stock');
		$this->db->join('tb_item', 'tb_stock.id_item = tb_item.id_item');
		return $this->db->get()->result();
	}

	//form
		//get stock out
	function get_stock_out($id_item)
	{
		$this->db->select('*');
		$this->db->from('tb_stock');
		$this->db->where('id_item',$id_item);
		return $this->db->get()->row();
	}

		//get stock list
	public function get_stock_list($id_item)
    {
        $this->db->select('*');
        $this->db->from('tb_item_stock_out');
		$this->db->where('id_item',$id_item);
        return $this->db->get()->result();
    }

	//save form
		//insert 
	function insert_form($data)
	{
		$this->db->insert('tb_item_stock_out', $data);
	}

	//form Edit
		//get stock outs
		public function get_stock_form($id)
		{
			$this->db->select('tb_item_stock_out.*,tb_stock.id_item,tb_stock.balance_stock');
			$this->db->from('tb_stock');
			$this->db->where('tb_item_stock_out.id',$id);
			$this->db->join('tb_item_stock_out', 'tb_stock.id_item = tb_item_stock_out.id_item');
			return $this->db->get()->row();
		}

		public function get_stock_outs($id)
		{
			$this->db->select('*');
			$this->db->from('tb_stock');
			$this->db->where('id',$id);
			return $this->db->get()->row();
		}
	
			//get stock lists
		public function get_stock_lists($id)
		{
			$this->db->select('*');
			$this->db->from('tb_item_stock_out');
			$this->db->where('id', $id);
			return $this->db->get()->row();
		}

	// Update
		// Update data
		public function update_edit($id_item,$data_out)
		{
			$this->db->set($data_out);
			$this->db->where('id_item', $id_item);
			$this->db->update('tb_stock');
		}

		// Update total
		public function update_total($id_item,$total)
		{
			$this->db->set($total);
			$this->db->where('id_item', $id_item);
			$this->db->update('tb_stock', $total);
		}

		//total stock out
	public function count_stock($id_item)
    {
        $this->db->select_sum('stock_out');
        $this->db->from('tb_item_stock_out');
        $this->db->where('id_item', $id_item);
        return $this->db->get()->row();
    }

	#manggil balance
	    public  function call_balance($id_item)
    {
        $this->db->select('*');
        $this->db->from('tb_stock');
        $this->db->where('id_item', $id_item);
        return $this->db->get()->row();
    }

	public  function cek_stock_out($id)
    {
        $this->db->select('total_stock_out');
        $this->db->from('tb_stock');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

	public function update_itemout($id, $data)
	{
		$this->db->set($data);
		$this->db->where('id', $id);
		return $this->db->update('tb_item_stock_out');
	}

	public function insert_out($data)
	{
		return $this->db->insert('tb_item_stock_out', $data);
	}

		//total stock out
		public function count_item_stock($id_item, $total)
		{
			$this->db->set($total);
			$this->db->where('id_item', $id_item);
			$this->db->update('tb_stock');
		}

		public function total($id_item, $total1)
    {
        $this->db->set($total1);
        $this->db->where('id_item', $id_item);
        $this->db->update('tb_stock');
    }
	 public function update_balance_out($id_item, $total)
	 {
		$this->db->set($total);
        $this->db->where('id_item', $id_item);
        return $this->db->update('tb_stock');
	 }
	//Query Detail
    public function detail($id_item)
    {
        $this->db->select('*');
        $this->db->from('tb_item_stock_out');
        $this->db->where('id_item', $id_item);

        return $this->db->get()->result();
    }

    public function per_mount($id_item)
    {
    	// $date = array("2021-04-05", "2021-04-06");
    	$this->db->select('id_item, SUM(IF(date_out = "2021-04-05", stock_out,0)) AS "tes", SUM(IF(date_out = "2021-04-06", stock_out,0)) AS "tes"');
        $this->db->from('tb_item_stock_out');
        $this->db->group_by('id_item', 'WITH ROLLUP');
        // $this->db->where('id_item', $id_item);
        return $this->db->get()->result();
    }

	//Query Hapus
    public function hapus_data($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    } 
}