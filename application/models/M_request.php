<?php

class M_request extends CI_Model
{

    //Query Index
    #Get Request List
    public function get_request_list()
    {
        $this->db->select('*');
        $this->db->from('tb_request');
        return $this->db->get()->result();
    }

    //Query Form
    function get_req_in($id)
    {
        $this->db->select('tb_request.*,tb_stock.id_item,tb_stock.balance_stock');
        $this->db->from('tb_stock');
        $this->db->where('tb_request.id', $id);
        $this->db->join('tb_request', 'tb_stock.id_item = tb_request.id_item');
        return $this->db->get()->row();
    }

    //Query Save Form
    #manggil balance
    public  function call_balance($id_item)
    {
        $this->db->select('balance_stock, total_stock_out');
        $this->db->from('tb_stock');
        $this->db->where('id_item', $id_item);
        return $this->db->get()->row();
    }
    #manggil total stock out
    public  function cek_stock_out($id_item)
    {
        $this->db->select('total_stock_out');
        $this->db->from('tb_stock');
        $this->db->where('id_item', $id_item);
        return $this->db->get()->row();
    }

    //Update Request
    public function update_request($id, $data)
    {
        $this->db->set($data);
        $this->db->where('id', $id);
        $this->db->update('tb_request');
    }

    //Update Balance
    public function update_balance($id_item, $total)
    {
        $this->db->set($total);
        $this->db->where('id_item', $id_item);
        $this->db->update('tb_stock');
    }

    //Insert Tb Stock Out
    function insert_stock_out($tb_stock_out)
    {
        $this->db->insert('tb_item_stock_out', $tb_stock_out);
    }

    //Query Detail
    public function detail($id_item)
    {
        $this->db->select('*');
        $this->db->from('tb_request');
        $this->db->where('id_item', $id_item);

        return $this->db->get()->result();
    }


    //Query Hapus
    public function hapus_data($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
}
