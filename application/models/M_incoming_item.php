

<?php

class M_incoming_item extends CI_Model
{
    //Query Index
    public function get_total_stock_in()
    {
        $this->db->select('tb_stock.id_item,tb_stock.total_stock_in,tb_stock.opening,tb_stock.balance_stock,tb_item.id_category,tb_item.name,tb_item.unit,tb_item.status');
        $this->db->from('tb_stock');
        $this->db->where('tb_item.status = 1');
        $this->db->join('tb_item', 'tb_stock.opening,tb_stock.balance_stock,tb_stock.id_item = tb_item.id_item');
        return $this->db->get()->result();
    }

    public function get_lates_update()
    {
        $this->db->select('*');
        $this->db->from('tb_stock');
        $this->db->order_by('last_update', 'DESC');
        return $this->db->get()->result();
    }

    public function get_user($user_id)
    {
        $this->db->select('*');
        $this->db->from('users');
        // $this->db->join('employees', 'employees.employee_no = users.employee_no', 'left');
        $this->db->where('id', $user_id);
        return $this->db->get()->row();
    }

    //Query Form
    //Get Stock In
    function get_stock_in($id_item)
    {
        $this->db->select('*');
        $this->db->from('tb_stock');
        $this->db->where('id_item', $id_item);
        return $this->db->get()->row();
    }

    //Get Stock list
    public function get_stock_list($id_item)
    {
        $this->db->select('*');
        $this->db->from('tb_item_stock_in');
        $this->db->where('id_item', $id_item);
        return $this->db->get()->result();
    }

    //Query Save Add Stock
    //Memanggil Total Stock In
    public  function cek_stock_in($id_item)
    {
        $this->db->select('total_stock_in');
        $this->db->from('tb_stock');
        $this->db->where('id_item', $id_item);
        return $this->db->get()->row();
    }

    //Memanggil Balance Stock
    public  function call_balance($id_item)
    {
        $this->db->select('balance_stock');
        $this->db->from('tb_stock');
        $this->db->where('id_item', $id_item);
        return $this->db->get()->row();
    }

    //Insert Add Stock
    function insert_add_stock($data)
    {
        $this->db->insert('tb_item_stock_in', $data);
    }

    //Pernjumlahan Semua Stock In
    public function count_stock($id_item)
    {
        $this->db->select_sum('stock_in');
        $this->db->from('tb_item_stock_in');
        $this->db->where('id_item', $id_item);
        return $this->db->get()->row();
    }

    //Total Pehitungan [Stock In + Opening] & [Stock In + Balance]
    public function total($id_item, $total_balance)
    {
        $this->db->set($total_balance);
        $this->db->where('id_item', $id_item);
        $this->db->update('tb_stock');
    }

    //Query Detail
    public function detail($id_item)
    {
        $this->db->select('*');
        $this->db->from('tb_item_stock_in');
        $this->db->where('id_item', $id_item);

        return $this->db->get()->result();
    }

    //Query Edit
    public function edit($id_item)
    {
        $this->db->select('tb_stock.id_item,tb_stock.opening,tb_item_stock_in.*');
        $this->db->from('tb_stock');
        $this->db->where('tb_item_stock_in.id_item', $id_item);
        $this->db->join('tb_item_stock_in', 'tb_stock.id_item = tb_item_stock_in.id_item');

        return $this->db->get()->row();
    }


    //Query Form edit
    public function get_stock_edit($id)
    {
        $this->db->select('tb_item_stock_in.*,tb_stock.id_item,tb_stock.total_stock_in');
        $this->db->from('tb_stock');
        $this->db->where('tb_item_stock_in.id', $id);
        $this->db->join('tb_item_stock_in', 'tb_stock.id_item = tb_item_stock_in.id_item');

        return $this->db->get()->row();
    }

    public function get_pic($id)
    {
        $this->db->select('*');
        $this->db->from('tb_item_stock_in');
        $this->db->where('id', $id);
        // log_r($id);

        return $this->db->get()->row();
    }

    public function call_id($id_item)
    {
        $this->db->select('id');
        $this->db->from('tb_item_stock_in');
        $this->db->where('id', $id_item);
        // log_r($id);

        return $this->db->get()->row();
    }

    public function data_stock($id_item)
    {
        $this->db->select('*');
        $this->db->from('tb_item_stock_in');
        $this->db->where('id_item', $id_item);

        return $this->db->get()->row();
    }

    //Memanggil Opening
    public  function call_opening($id_item)
    {
        $this->db->select('*');
        $this->db->from('tb_stock');
        $this->db->where('id_item', $id_item);
        return $this->db->get()->row();
    }

    public function update_edit($id, $data)
    {
        // log_r($data);
        $this->db->set($data);
        $this->db->where('id', $id);
        $this->db->update('tb_item_stock_in', $data);
    }

    //Query Update [stock in]
    function update_add_item($data, $id_item)
    {
        $this->db->where('id_item', $id_item);
        $this->db->update('tb_item_stock_in', $data);
    }

    //Query sum update
    public function count_sum($id_item)
    {
        $this->db->select_sum('stock_in');
        $this->db->from('tb_item_stock_in');
        $this->db->where('id_item', $id_item);
        return $this->db->get()->row();
    }
    //Query Update [total stock in]
    public function total_sum($id_item, $total_sum)
    {
        $this->db->set($total_sum);
        $this->db->where('id_item', $id_item);
        $this->db->update('tb_stock');
    }

    //Query Hapus
    public function hapus_data($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
}
